<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
/* @var $APPLICATION */

$APPLICATION->IncludeComponent(
	"proomer:design.detail", 
	".default", 
	array(
		"IBLOCK_ID" => "11",
		"ELEMENT_CODE" => $_REQUEST["elementCode"],
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"STYLE_HIGHLOAD_ID" => "3",
		"ROOMS_IBLOCK_ID" => "12"
	),
	false
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");

?>
