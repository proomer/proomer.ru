<?

$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/testpersonal/order/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/testpersonal/order/index.php",
	),
	array(
		"CONDITION" => "#^/testcatalog/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/testcatalog/index.php",
	),
	array(
		"CONDITION" => "#^/design/(.*)/#",
		"RULE" => "elementCode=$1",
		"PATH" => "/pages/design/detail.php",
	),
	array(
		"CONDITION" => "#^/teststore/#",
		"RULE" => "",
		"ID" => "bitrix:catalog.store",
		"PATH" => "/teststore/index.php",
	),
	array(
		"CONDITION" => "#^/testnews/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/testnews/index.php",
	),
	 /** VR страница дизайна */
    array(
        "CONDITION" => "#^/panorama/(.*)#",
        "RULE" => "elementCode=$1",
        "PATH" => "/pages/vr/design.php",
    )
);

?>
