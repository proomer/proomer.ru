
<?
/**
 * Class Sibirix_Model_Room
 */
class Sibirix_Model_Room extends Sibirix_Model_Bitrix {

    /**
     * @var CIBlockElement
     */
    protected $_bxElement;
    protected $_iblockId = IB_ROOM;
    protected $_selectFields = array(
        'ID',
        'NAME',
        'PROPERTY_IMAGES',
        'PROPERTY_AREA',
        'PROPERTY_DESIGN',
		'PROPERTY_ROOM_PLAN',
		'PROPERTY_PRICE_SQUARE'
    );

    public function init($initParams = NULL) {
        $this->_bxElement = new CIBlockElement();
    }

    /**
     * Добавляет комнату
     * @param $designId
     * @param $name
     * @param $area
     * @return bool
     */
    public function addRoom($designId, $name, $area) {
        if (!((int)$designId > 0)) return false;

        $fields = array(
            "IBLOCK_ID"       => $this->_iblockId,
            "NAME"            => $name,
            "PROPERTY_VALUES" => array(
                "DESIGN" => $designId,
                "AREA"   => $area
            )
        );
        $addResult = $this->_bxElement->Add($fields);

        return $addResult;
    }
	

    /**
     * Добавление/изменение комнаты
     * @param $fields
     * @return bool|int
     */
    public function editRoom($fields) {
        $bxElement           = new CIBlockElement();
        $fields["IBLOCK_ID"] = IB_ROOM;

        if ($fields["ID"] > 0) {
            $roomId    = $fields["ID"];
            $designProps = $fields["PROPERTY_VALUES"];
            unset($fields["ID"], $fields["PROPERTY_VALUES"]);

            if (!empty($fields["NAME"])) {
                $fields["CODE"] = CUtil::translit($fields["NAME"].uniqid(), "ru");
            }

            $updateResult = $bxElement->Update($roomId, $fields);

            if ($updateResult && !empty($designProps)) {
                $bxElement->SetPropertyValuesEx($roomId, IB_ROOM, $designProps);
            }

            $result = $roomId;
        } else {
            $patternEnumId = EnumUtils::getListIdByXmlId($this->_iblockId, "STATUS", "draft");
            if (empty($fields["NAME"])) {
                $fields["NAME"] = PATTERN_ROOM_NAME .uniqid();
                $fields["CODE"] = CUtil::translit($fields["NAME"], "ru");
            }
            $newId = $bxElement->Add($fields);

            $result = $newId;
        }

       /* if (empty($fields["PRICE_VALUE"])) {
            return $result;
        }*/

        /*$bxPrice = new CPrice();
        $res = $bxPrice->GetList(array(), array(
                "PRODUCT_ID"       => $fields["PRICE_VALUE"]["PRODUCT_ID"],
                "CATALOG_GROUP_ID" => $fields["PRICE_VALUE"]["CATALOG_GROUP_ID"],
                "CURRENCY"         => $fields["PRICE_VALUE"]["CURRENCY"]
            ));

        if ($arr = $res->Fetch()) {
            $bxPrice->Update($arr["ID"], $fields["PRICE_VALUE"]);
        } else {
            $bxCatalogProduct = new CCatalogProduct();
            $bxCatalogProduct->Add(["ID" => $fields["PRICE_VALUE"]["PRODUCT_ID"]]);
            $bxPrice->Add($fields["PRICE_VALUE"]);
        }*/

        return $result;
    }
	
 
	public function getRoomList($filter, $sort, $page, $profile=false, $limit = 11) {
		
        $roomList = $this->select($this->_selectFields, true)->where($filter)->orderBy($sort, true)->getPageItem($page, $limit);
        return $roomList;
    }	
	
	/**
	 * УСТАРЕЛО, теперь цены хранятся в дизайне команты
	 *
	 * Получить сумму цен по мебели, свету, сантехнике, электронике и материалам
	 * всех комнат дизайна 
	 *
	 * @param $design_id
	 * @return array
	 *
	 * @author Роман Камлюк <roman@kamlyuk.com>
	 */
	public function getSumPricesByDesignId($design_id) {
		
		// Цены каждой комнаты
        $rooms_prices_list = $this
		->select(['PROPERTY_PRICE_FURNITURE', 'PROPERTY_PRICE_LIGHT', 'PROPERTY_PRICE_PLUMBING', 'PROPERTY_PRICE_ELECTRONICS', 'PROPERTY_PRICE_MATERIALS'], true)
		->where(['PROPERTY_DESIGN' => $design_id])->getElements();
		
		// Сумма цен всех комнат
		$prices_list = array(	'PRICE_FURNITURE' => 0,
								'PRICE_LIGHT' => 0,
								'PRICE_PLUMBING' => 0,
								'PRICE_ELECTRONICS' => 0,
								'PRICE_MATERIALS' => 0);
		foreach ($rooms_prices_list as $room_prices_list)
		{
			$prices_list['PRICE_FURNITURE'] += $room_prices_list->PROPERTY_PRICE_FURNITURE_VALUE;
			$prices_list['PRICE_LIGHT'] += $room_prices_list->PROPERTY_PRICE_LIGHT_VALUE;
			$prices_list['PRICE_PLUMBING'] += $room_prices_list->PROPERTY_PRICE_PLUMBING_VALUE;
			$prices_list['PRICE_ELECTRONICS'] += $room_prices_list->PROPERTY_PRICE_ELECTRONICS_VALUE;
			$prices_list['PRICE_MATERIALS'] += $room_prices_list->PROPERTY_PRICE_MATERIALS_VALUE;
		}
		
		// Общая сумма
		$sum_price = 0;
		foreach($prices_list as $price)
		{
			$sum_price += $price;
		}
		$prices_list['PRICE_SUM'] = $sum_price;
	
		
		return $prices_list;
    }
	
    /**
     * Удаляет комнату
     * @param $roomId
     * @return bool
     */
    public function deleteRoom($roomId) {
        if (!((int)$roomId > 0)) return false;
        $delResult = $this->_bxElement->Delete($roomId);

        return $delResult;
    }

    /**
     * Проверяет есть ли у пользователя доступ к комнате
     * @param $roomId
     * @param int $createdBy
     * @return bool
     */
    public function checkRoomAccess($roomId, $createdBy = 0) {
        $roomData = $this->getElement($roomId);
        $designId = $roomData->PROPERTY_DESIGN_VALUE;

        if (!($designId > 0)) {
            return false;
        }

        if (!Sibirix_Model_User::isAuthorized()) {
            return false;
        }

        if ($createdBy == 0) {
            $designModel = new Sibirix_Model_Design();
            $designData = $designModel->getElement($designId);
            $createdBy  = $designData->CREATED_BY;
        }

        if ($createdBy != Sibirix_Model_User::getId()) {
            return false;
        }

        return true;
    }

    /**
     * Добавляет файл к множественному свойству
     * возвращает список новых файлов
     * @param $id
     * @param $imageFile
     * @return array
     * @throws Exception
     */
    public function addImage($id, $imageFile) {

        $imageExist = $this->select(["PROPERTY_IMAGES"], true)->getElement($id);
        $alreadyImages = array();

        foreach ($imageExist->PROPERTY_IMAGES_VALUE as $key => $image) {
            $alreadyImages[$imageExist->PROPERTY_IMAGES_PROPERTY_VALUE_ID[$key]] = CIBlock::makeFilePropArray($image);
        }
        $fields["ID"] = $id;
        $fields["PROPERTY_VALUES"] = array(
            "IMAGES" => $alreadyImages + array("n0" => array("VALUE" => $imageFile))
        );

        $this->_bxElement->SetPropertyValuesEx($id, IB_ROOM, $fields["PROPERTY_VALUES"]);

        $newImage = $this->select(["PROPERTY_IMAGES"], true)->getElement($id);
        $resultImages = array();
        foreach ($newImage->PROPERTY_IMAGES_VALUE as $key => $imageId) {
            $resultImages[] = array(
                "valueId" => $newImage->PROPERTY_IMAGES_PROPERTY_VALUE_ID[$key],
                "imgSrc" => Resizer::resizeImage($imageId, "DROPZONE_ROOMS_PHOTO")
            );
        }

        return $resultImages;
    }

    /**
     * Удаляет файл из множетсвенного свойства
     * возвращает список новых файлов
     * @param $roomId
     * @param $imageItemId
     * @return array
     * @throws Exception
     */
    public function deleteImageItem($roomId, $imageItemId) {
        $arFile["MODULE_ID"] = "iblock";
        $arFile["del"] = "Y";

        $this->_bxElement->SetPropertyValueCode($roomId, "IMAGES", Array($imageItemId => Array("VALUE" => $arFile)));

        $newImage = $this->select(["PROPERTY_IMAGES"], true)->getElement($roomId);
        $resultImages = array();
        foreach ($newImage->PROPERTY_IMAGES_VALUE as $key => $imageId) {
            $resultImages[] = array(
                "valueId" => $newImage->PROPERTY_IMAGES_PROPERTY_VALUE_ID[$key],
                "imgSrc" => Resizer::resizeImage($imageId, "DROPZONE_ROOMS_PHOTO")
            );
        }

        return $resultImages;
    }
	
	/**
     * Получает прайс-лист комнат
     * @param $design
     * @return array
     */
    public function getPrice($room) {
        if (empty($room)) return array();
        $priceList = array();

        if (!is_array($room)) {
            $room = [$room];
        }

        $roomIds = array();
        foreach ($room as $roomItem) {
            if (!is_object($roomItem)) continue;
            $designIds[] = $roomItem->ID;
        }

        if (empty($designIds)) {
            $designIds = $room;
        }

        $bxPrice = new CPrice();
        $getList = $bxPrice->GetList(array(),array("PRODUCT_ID" => $roomIds));

        while ($price = $getList->Fetch()) {
            $priceList[$price["PRODUCT_ID"]] = $price;
        }

        return $priceList;
    }
}
