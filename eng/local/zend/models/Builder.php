<?

class Sibirix_Model_Builder extends Sibirix_Model_Bitrix {

    protected $_iblockId = IB_BUILDER;
	
	 protected $_selectFields = array(
        'ID',
        'CODE',
        'NAME',
        'DETAIL_PICTURE',
        'PREVIEW_TEXT',
        'DETAIL_TEXT',
        'PROPERTY_DESIGN_CNT',
        'PROPERTY_CONSTRUCTOR',
        'PROPERTY_SIMILAR_COMPLEX',
		'PROPERTY_PHOTO_COMPLEX'
    );

    protected $_selectListFields = [
        'ID',
        'CODE',
        'NAME',
        'DETAIL_PICTURE',
        'PREVIEW_TEXT',
        'PROPERTY_CONSTRUCTOR',
        'PROPERTY_AVERAGE_DESIGN_PRICE',
        'PROPERTY_DESIGN_CNT',
    ];

	public function getBuilderList($selected, $filter, $sort, $page, $limit) {
		
		if(!$selected){
			$selected = $this->_selectListFields;
		}
		
        $planList = $this->select($selected, true)->where($filter)->orderBy($sort, true)->getPageItem($page, $limit);
        return $planList;
    }
	
	 /**
     * Email застройщика
     * 
     * @author Роман Камлюк <roman@kamlyuk.com>
     */
	public function getEmail($id) {
		
        $email = $this->select(['PROPERTY_E_MAIL_CENTER'], true)->getElement($id);
        return $email;
    }
	
}
