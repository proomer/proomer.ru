<?

/**
 * Class Sibirix_Model_Complex
 *
 */
class Sibirix_Model_Categories extends Sibirix_Model_Bitrix {

	protected $hh;
    protected $_iblockId = IB_CATEGORY;
    protected $_selectFields = array(
        'ID',
        'NAME'
    );
	

/*=================================================================================*/
//	Получает список категории товаров
/*=================================================================================*/
	public function getCategoryList($filter, $sort, $page) {
		$categories['ITEMS'] = $this->select($this->_selectFields, true)->orderBy([], true)->getElements();
		return $categories;
    }
/*=================================================================================*/
/*=================================================================================*/	

}
