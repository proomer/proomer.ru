<?

/**
 * Class Zend_View_Helper_ProfileSidebar
 */
class Zend_View_Helper_ProfileSidebar extends Zend_View_Helper_Abstract {

    public function profileSidebar() {
        $userModel    = new Sibirix_Model_User();
        $user = $userModel->getCurrent();

        if ($user) {
            $userModel->getImageData($user, 'PERSONAL_PHOTO');
            $type = $user->getTextType()['class'];

            $modelMenu = new Sibirix_Model_MainMenu();
            $menu = $modelMenu->where(['SECTION_CODE' => $type])->getElements();
			
        }

		if($user->UF_IMAGE_BACKGROUND){
			$user->UF_IMAGE_BACKGROUND = Resizer::resizeImage($user->UF_IMAGE_BACKGROUND, 'PROFILE_BACKGROUND_USER');
		}
		else{
			$user->UF_IMAGE_BACKGROUND = false;
		}
		
		CModule::IncludeModule("sale");
		// Выберем все счета
		$dbAccountCurrency = CSaleUserAccount::GetList(
			array(),
			array('USER_ID' => $user->ID),
			false,
			array("CURRENT_BUDGET", "CURRENCY")
		);
		
		$arrCurrency = array();
		
		while ($arAccountCurrency = $dbAccountCurrency->Fetch())
		{
			array_push($arrCurrency, $arAccountCurrency);
		}
		
		if($arrCurrency){
			$user->BUDGET = array_shift($arrCurrency);
		}
		else{
			$user->BUDGET = 0;
		}

        return $this->view->partial('_partials/profile/sidebar.phtml', array("user" => $user, "menu" => $menu));
    }
}