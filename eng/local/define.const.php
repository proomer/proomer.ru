<?
define("IB_SETTINGS",      				 3); // Настройки
define("IB_CITY",          				 4); // Город
define("IB_COMPLEX",       				 5); // Жилой комплекс
define("IB_HOUSE",         				 6); // Дом
define("IB_ENTRANCE",      				 7); // Подъезд
define("IB_FLOOR",         				 8); // Этаж
define("IB_FLAT",          				 9); // Квартира
define("IB_PLAN",         				10); // Планировка
define("IB_PLAN_OPTION",         		29); // Вариант
define("IB_DESIGN",       				11); // Дизайн
define("IB_ROOM",         				12); // Комната
define("IB_MAIN_SLIDER",  				13); // Верхний слайдер на главной
define("IB_PROMO",        				14); // Промо-блок на главной
define("IB_ARTICLE",      				15); // Полезные статьи
define("IB_FEEDBACK",     				16); // Форма обратной связи
define("IB_FEEDBACK_DESIGNER", 			37); // Дизайн(комнаты)
define("IB_SUPER_MAN",    				30); // Форма вызов замерщика
define("IB_COMPLEX_PINS", 				17); // Метки на плане жилого комплекса
define("IB_MAIN_MENU",    				18); // Меню
define("IB_GOODS",   	 				20); // Товары
define("IB_PIN",   	  	  				21); // Пины
define("IB_ORDERS",       				24); //
define("IB_FILES",       				27); //
define("IB_ORDER_PROJECT", 				28); // Заказ проектов
define("IB_ORDER_PROJECT_APARTMENT", 	33); // Заказ проектов(квартиры)
define("IB_ORDER_PROJECT_ROOM", 		31); // Заказ проектов(комнаты)
define("IB_ORDER_PLAN_OPTION", 			32); // 
define("IB_ORDER_PROJECT_FAMILY", 		34); // Заказ проектов(семья)
define("IB_BUILDER", 					35); // Застройщик
define("IB_DESIGN_ROOM", 				36); // Дизайн(комнаты)
define("IB_ACCOUNTS", 					40); // Счета
define("IB_BANK", 						39); // Банк


