
(function($, APP) {
    'use strict';
    
    /**
     * Контроллер маркетплейса
     **/
    APP.Controls.Page.Shop = can.Control.extend({

        init: function() {
            APP.Controls.SelectMulti.initList(this.element.find('.js-select-multi'));
            // new APP.Controls.MovingSlider(this.element.find('.js-title-slider'));
            new APP.Controls.FilterForm(this.element.find(".js-filter-form"));
            new APP.Controls.Pagination(this.element.find(".js-pagination"));
            new APP.Controls.Sorting(this.element.find(".js-sort"));
            new APP.Controls.ViewCounter(this.element.find(".js-view-counter"));
			new APP.Controls.OrderForm(this.element.find(".js-form-order"));
           //new APP.Controls.Likes.initList(this.element.find('.js-like'));
           // APP.Controls.AddBasket.initList(this.element.find(".js-add-basket"));
			APP.Controls.AddGoodsToBasket.initList(this.element.find(".js-add-basket"));
			APP.Controls.AddGoodsToFavorites.initList(this.element.find(".js-add-favourite"));
			
			this.element.find("#tabs").tabs();
			this.element.find('.preview360-gallery').preview360({});
			this.element.find('a.gallery').fancybox({});
        },
		
        'list.contentUpdate': function (el, e, param) {
            var $ajaxContent = this.element.find('.js-ajax-list-content');
            var data = [];

            if (param.page > 0) {
                data.push({
                    name: "page",
                    value: param.page
                })
            } else {
                data.push({
                    name: "page",
                    value: 1
                })
            }

            if (param.viewCounter > 0) {
                data.push({
                    name: "viewCounter",
                    value: param.viewCounter
                })
            }
	
            this.element.find(".js-sort a").each(function(){
                data.push({
                    name: "sort[" + $(this).data("type") + "]",
                    value: $(this).data("method")
                })
            });

            $.merge(data, this.element.find('.js-filter-form').serializeArray());

            var self = this;
            $ajaxContent.ajaxl({
                topPreloader: true,
                url: '/shop/catalog/',
                data: data,
                dataType: 'HTML',
                type: 'POST',
                success: this.proxy(function (data) {
					$ajaxContent.html(data);
                    if ($ajaxContent.find('.not-found').length > 0) {
                        self.element.find('.filter').css({'visibility': 'hidden'});
                    } else {
                        self.element.find('.filter').css({'visibility': 'visible'});
                    }
				
                    new APP.Controls.Pagination(this.element.find(".js-pagination"));
                    //$.scrollTo(this.element.find('.catalog-block'), 500);
                   // new APP.Controls.Likes.initList(this.element.find('.js-like'));
                    APP.Controls.AddGoodsToBasket.initList(this.element.find(".js-add-basket"));
                })
            });
        },
		'.js-buy click': function() {
            this.element.trigger("openOrder");
        },

			
		////////////Калькулятор
		//получаем значения параметров
		'.js-filter-form click': function(e) {
			var json_trade_offer = JSON.parse(this.element.find('#json_trade_offer').html());
			var Price = Number($('.price > span.price').html()); //получаем стоимость товара.
			var base_price = Number($('#price_item').html());
			var colorPrice = 0;
			var sizePrice = 0;
			var materialPrice = 0;
			var colorValue = $('input.color:checked').val();
			var sizeValue = $('input.size:checked').val();
			var materialValue = $('input.material:checked').val();
			
			var colorName = $('input.color:checked').attr('data-name');
			var sizeName = $('input.size:checked').attr('data-name');
			var materialName = $('input.material:checked').attr('data-name');
			
			$('#materialName').val(materialName);
			$('#sizeName').val(sizeName);
			$('#colorName').val(colorName);
			
			$('#materialId').val(materialValue);
			$('#sizeId').val(sizeValue);
			$('#colorId').val(colorValue);
			$('#count').val($('.vote').html());
			
			for(var key in json_trade_offer){													
				if(colorValue == key){		
					colorPrice = json_trade_offer[key].PROPERTY_PRICE_VALUE.PRICE;						
				}
				else if(sizeValue == key){
					sizePrice = json_trade_offer[key].PROPERTY_PRICE_VALUE.PRICE;	
				}
				else if(materialValue == key){
					materialPrice = json_trade_offer[key].PROPERTY_PRICE_VALUE.PRICE;	
				}
			}
			
			
				var count = Number($('#voter .vote').html())
				var total = base_price + (Number(colorPrice) + Number(sizePrice) + Number(materialPrice)) * count;//формула расчета общей стоимости
				var discount = (total/100) * Number($('#discount').html());
				var newTotal = Math.round(total);//округляем
				var newTotalDics = Math.round(total) - discount;//округляем
				
				Number($('.price > span.price').html(newTotal));
				Number($('.price > span.new_price').html(newTotalDics));
		
		
		},
		
		'.js-search-form click': function(e){
			var $ajaxContent = this.element.find('.js-ajax-list-content');

            var q = this.element.find('#form_search_q').val();
			
			window.location.href = '/shop/catalog?q='+q;

		},
		
		'.js-show-cat click': function(el, e){
			var atribute = el.attr('data-type')
			
			if(atribute == 'hide'){
				el.attr('data-type', 'show')
				el.prevAll("a").removeClass('disabled');
				el.removeClass('bottom');
				el.addClass('top');
				el.html('Свернуть');
				//el.prev(".catalog-menu__list_type_more").toggle('fast');
			}
			else{
				el.attr('data-type', 'hide');
				el.prevAll("a").addClass('disabled');
				el.prevAll("a:gt(5)").removeClass('disabled');
				el.removeClass('top');
				el.addClass('bottom');
				el.html('Ещё');
				//el.prev(".catalog-menu__list_type_more").toggle('fast');
			}
			return false;
		},
		
		'.js-show-all-characteristics click': function(el, e){
			this.element.find(".characters .character").each(function(){
				if($(this).hasClass("hide")){
					$(this).removeClass("hide");
				};
            });
			el.hide();
		}
    });

})(jQuery, window.APP);




