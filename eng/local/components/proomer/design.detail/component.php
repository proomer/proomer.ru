<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// что дергаем
$arSelect = array(
  'ID',
  'NAME',
  'CODE',
  'DETAIL_PICTURE',
  'PREVIEW_TEXT',
  'DETAIL_TEXT',
  'CREATED_BY',
  'PROPERTY_CREATED_BY',
  'PROPERTY_STYLE',
  'PROPERTY_PRIMARY_COLOR',
  'PROPERTY_BUDGET',
  'PROPERTY_AREA',
  'PROPERTY_PLAN_FLAT',
  'PROPERTY_ESTIMATE',
  'PROPERTY_PLAN',
  'PROPERTY_PLAN.NAME',
  'PROPERTY_STATUS',
  'PROPERTY_DOCUMENTS',
  'PROPERTY_LIKE_CNT',
  'PROPERTY_TIME',
  'PROPERTY_DESIGN_ROOM',
  'PROPERTY_IMG_3', // свойство множественное с изображениями 3d 360
  'PROPERTY_VIEW_COUNT', // просмотры страницы дизайна
  'PROPERTY_DESIGN_ROOM'
);
// фильтр по текущему коду, приходит с $_REQUEST
$arFilter = array(
  "IBLOCK_ID"=>IntVal($arParams['IBLOCK_ID']),
  'CODE' => $arParams['ELEMENT_CODE'],
  "ACTIVE"=>"Y"
);
// получем
$res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
$ob = $res->GetNextElement();

$design_room_ids = array_unique($ob->fields['PROPERTY_DESIGN_ROOM_VALUE']);

if ($ob)
{
  // просмотры
  CIBlockElement::CounterInc('PROPERTY_VIEW_COUNT');
  // поля - свойства
  $arFields = $ob->GetFields();
  // устанавливаем титл
  $APPLICATION->SetTitle($arFields['NAME']);

  // основная информация дизайн-проекта
  $arResult['ID'] = $arFields['ID'];
  $arResult['DETAIL_PICTURE_ID'] = $arFields["DETAIL_PICTURE"];
  $arResult['NAME'] = $arFields['NAME'];
  $arResult['DETAIL_PAGE_URL'] = "/design/".$arFields['CODE']."/";  
  $arResult['PREVIEW_TEXT'] = $arFields['PREVIEW_TEXT'];
  $arResult['DETAIL_TEXT'] = $arFields['DETAIL_TEXT'];
  $arResult['PROPERTY_DESIGN_ROOM'] = $arFields['PROPERTY_DESIGN_ROOM_VALUE'];
  // стоимость дизайн-проекта
  $price = CPrice::GetBasePrice($arFields['ID']);
  $arResult['COST_2'] = $price['PRICE'];
  $price = round($price['PRICE']);
  $price = number_format($price, 0, '.', ' ');
  $arResult['COST'] = $price;
 
  // стили в хайлоад
  $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById(IntVal($arParams['STYLE_HIGHLOAD_ID']))->fetch();
  if ($hldata)
  {
    $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
    $hl_query = new Bitrix\Main\Entity\Query($hlentity);
    $hl_query->setSelect(array('*'));
    $hl_query->setFilter(array('=UF_XML_ID' => $arFields['PROPERTY_STYLE_VALUE'] ));
    $hl_result = $hl_query->exec();
    $hl_result = new CDBResult($hl_result);
    // передаем в шаблон
    while ($hl_row = $hl_result->Fetch())
    {
      $arResult['STYLES'][] = $hl_row;
    }
  }

  // главное изображение

  $arResult['DETAIL_PICTURE'] = CFile::ResizeImageGet(
    $arFields["DETAIL_PICTURE"],
    array(
      //'width'=> 150, // оставим ширину оригинала для максимальной ширины мониторов
      'height' => 550
    ),
    BX_RESIZE_IMAGE_EXACT
  );
  // 3d крутилко
  $preview_360 = array();
  foreach ($arFields['PROPERTY_IMG_3_VALUE'] as $preview_360_img)
  {
    $preview_360_img = CFile::ResizeImageGet (
      $preview_360_img,
      array(
        'width' => 800,
        'height' => 800
      ),
      BX_RESIZE_IMAGE_PROPORTIONAL
    );
    $preview_360[] = $preview_360_img['src'];
  }
  $arResult['PREVIEW_360'] = $preview_360;

  // автор проекта
  $rsUser = CUser::GetByID($arFields['PROPERTY_CREATED_BY_VALUE']);
  $arUser = $rsUser->Fetch();
  $arResult['PROPERTY_CREATED_BY']['NAME'] = $arUser['NAME'] . " " . $arUser['LAST_NAME'];
  // аватарка автора
  $arResult['PROPERTY_CREATED_BY']['PERSONAL_PHOTO'] = CFile::ResizeImageGet(
    $arUser['PERSONAL_PHOTO'],
    array(
      'width' => 200,
      'height' => 200
    ),
    BX_RESIZE_IMAGE_EXACT
  );

  /** комнаты */
  $arFilterDesRooms = array(
    'IBLOCK_ID' => IB_DESIGN_ROOM, // id блока с комнатами
    'ID' => $arResult['PROPERTY_DESIGN_ROOM'], // id дизайна
    "ACTIVE"=>"Y"
  );

  $resDesRooms = CIBlockElement::GetList(array(), $arFilterDesRooms, false, array(), array(
    'IBLOC_ID',
    'ID',
    'NAME',
    'DESCRIPTION',
    'PREVIEW_TEXT',
    'DETAIL_TEXT',
    'PROPERTY_PRICE_SQUARE',
    'PROPERTY_AREA',
    'PROPERTY_IMAGES',
    'PROPERTY_ROOM_PLAN',
    'PROPERTY_PRICE_FURNITURE',
    'PROPERTY_PRICE_LIGHT',
    'PROPERTY_PRICE_PLUMBING',
    'PROPERTY_PRICE_ELECTRONICS',
    'PROPERTY_PRICE_MATERIALS',
    'PROPERTY_CATALOG',
  ));
  
  // готовим для шаблона
  $des_rooms = array();
  $all_area = 0;
  while ($obRooms = $resDesRooms->GetNextElement())
  {

    /* комната */
    $arFieldsRooms = $obRooms->GetFields();
    // изображения комнаты
    $roomsImages = array();
    foreach ($arFieldsRooms['PROPERTY_IMAGES_VALUE'] as $tempRoomImages)
    {
      $roomsImages[] = CFile::ResizeImageGet(
        $tempRoomImages,
        array(
          'width' => 1200,
          'height' => 520
        ),
        BX_RESIZE_IMAGE_EXACT
      );
    }
    // готовим для шаблона
    // общая квадратура
    $all_area = $all_area + $arFieldsRooms['PROPERTY_AREA_VALUE'];
    $roomCost = floatval($arFieldsRooms['PROPERTY_PRICE_FURNITURE_VALUE']) +
                floatval($arFieldsRooms['PROPERTY_PRICE_LIGHT_VALUE']) +
                floatval($arFieldsRooms['PROPERTY_PRICE_PLUMBING_VALUE']) +
                floatval($arFieldsRooms['PROPERTY_PRICE_ELECTRONICS_VALUE']) +
                floatval($arFieldsRooms['PROPERTY_PRICE_MATERIALS_VALUE']);

    $des_rooms[] = array(
      'ID'                  => $arFieldsRooms['ID'],
      'NAME'                => $arFieldsRooms['NAME'],
	  'SECOND_NAME'         => $arFieldsRooms['NAME'],
      'DESCRIPTION'         => $arFieldsRooms['DESCRIPTION'],
      'PREVIEW_TEXT'        => $arFieldsRooms['PREVIEW_TEXT'],
      'DETAIL_TEXT'         => $arFieldsRooms['DETAIL_TEXT'],
      'IMAGES'              => $roomsImages,
      'PRICE_SQUARE'        => (float) $arFieldsRooms['PROPERTY_PRICE_SQUARE_VALUE'],
      'PRICE_FURNITURE'     => (float) $arFieldsRooms['PROPERTY_PRICE_FURNITURE_VALUE'],
      'PRICE_LIGHT'         => (float) $arFieldsRooms['PROPERTY_PRICE_LIGHT_VALUE'],
      'PRICE_PLUMBING'      => (float) $arFieldsRooms['PROPERTY_PRICE_PLUMBING_VALUE'],
      'PRICE_ELECTRONICS'   => (float) $arFieldsRooms['PROPERTY_PRICE_ELECTRONICS_VALUE'],
      'PRICE_MATERIALS'     => (float) $arFieldsRooms['PROPERTY_PRICE_MATERIALS_VALUE'],
      'COST_ALL'            => $roomCost,
      'AREA'                => $arFieldsRooms['PROPERTY_AREA_VALUE'],
      'ROOM_PLAN'           => $arFieldsRooms['PROPERTY_ROOM_PLAN_VALUE'],
      'CATALOG'             => $arFieldsRooms['PROPERTY_CATALOG_VALUE'],
    );

  } 
	$desIds = array_map(function ($obj) {
	return $obj['ID'];
	}, $des_rooms);

  /** комнаты */
  $arFilterRooms = array(
    'IBLOCK_ID' => IB_ROOM, // id блока с комнатами
    'PROPERTY_DESIGN' => $desIds, // id дизайна
    "ACTIVE"=>"Y"
  );
  
  $resRooms = CIBlockElement::GetList(array(), $arFilterRooms, false, array(), array(
    'ID',
    'NAME',
	'PROPERTY_DESIGN'
  ));
  
  
  $rooms = array();
  while ($obRooms = $resRooms->GetNextElement())
  {
	$arFieldsRooms = $obRooms->GetFields();

	$rooms[] = array(
		'ID'                  => $arFieldsRooms['ID'],
		'NAME'                => $arFieldsRooms['NAME'],
		'PROPERTY_DESIGN_VALUE'              => $arFieldsRooms['PROPERTY_DESIGN_VALUE']
	);
  }
  
	

	
  foreach($des_rooms as $a_des_room){
	  foreach($rooms as $a_room){		
		  if($a_room['PROPERTY_DESIGN_VALUE'] == $a_des_room['ID']){
			$a_des_room['SECOND_NAME'] = $a_room['NAME'];
			break;
		  }
	  }
	  $a_des_room['SECOND_NAME'] = $str_name;
  }
  
  for($i = 0; $i < count($des_rooms); $i++){
	  for($n = 0; $n < count($rooms); $n++){
		  if($rooms[$n]['PROPERTY_DESIGN_VALUE'] == $des_rooms[$i]['ID']){
			  $des_rooms[$i]['SECOND_NAME'] = $rooms[$n]['NAME'];			  
			  break;
		  }
	  }
  }

  // отдаем комнаты в шаблон
  $arResult['ROOMS'] = $des_rooms;
  $arResult['SUM_AREA'] = $all_area;

  /*echo '<pre>';
  print_r($arResult);
  echo '</pre>';*/
}
else // нету элемента с такий блок кодом
{
  LocalRedirect('/404.php');
}

// уходим в шаблон
$this->IncludeComponentTemplate();

?>
