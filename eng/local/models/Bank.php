<?

class Sibirix_Model_Bank extends Sibirix_Model_Bitrix {

    protected $_iblockId = IB_BANK;
	
	 protected $_selectFields = array(
        'ID',
        'CODE',
        'NAME',
        'PROPERTY_EMAIL'
    );

    protected $_selectListFields = [
        'ID',
        'CODE',
        'NAME',
        'PROPERTY_EMAIL'
    ];

	public function getBuilderList($selected, $filter, $sort, $page, $limit) {
		
		if(!$selected){
			$selected = $this->_selectListFields;
		}
		
        $planList = $this->select($selected, true)->where($filter)->orderBy($sort, true)->getPageItem($page, $limit);
        return $planList;
    }
	
	 /**
     * Email банка
     * 
     * @author Роман Камлюк <roman@kamlyuk.com>
     */
	public function getEmail($id) {
		
        $email = $this->select(['PROPERTY_EMAIL'], true)->getElement($id);
        return $email;
    }
	
}