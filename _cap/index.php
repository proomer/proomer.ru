<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Сервис интерьерных решений нового поколения</title>
        <meta name="description" content="Онлайн-сервис готовых дизайнерских интерьерных решений." />
        <meta name="keywords" content="дизайн-проекты, онлайн, заказать дизайн-проект" />

        <!-- Open Graph data -->
        <meta property="og:title" content="Сервис интерьерных решений нового поколения" />
        <meta property="og:image" content="//proomer.ru/_cap/videos/rooms.jpg" />
        <meta property="og:description" content="Онлайн-сервис готовых дизайнерских интерьерных решений." /> 

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="_cap/style.css?v=1">
    </head>
    <body>
        <video autoplay poster="_cap/videos/rooms.jpg" class="background-video" loop>
            <source src="_cap/videos/rooms.webm" type="video/webm">
            <source src="_cap/videos/rooms.mp4" type="video/mp4">
        </video>
        <div class="backgroud-color"></div>
        <div class="header">
            <img class="logo" src="_cap/images/logo.png" alt="Proomer">
            <span class="slogan">Сервис интерьерных решений
                <br>нового поколения
            </span>
            <ul class="social">
                <li>
                    <a href="https://vk.com/proomer_ru" target="_blank">
                        <img src="_cap/images/social/vk.png" alt="VK">
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com/proomerru/" target="_blank">
                        <img src="_cap/images/social/fb.png" alt="FB">
                    </a>
                </li>
                <!--li>
                    <a href="http://instagram.com/" target="_blank">
                        <img src="_cap/images/social/inst.png" alt="INS">
                    </a>
                </li -->
            </ul>
        </div>

        <div class="blocks">
            <div class="column column-default">
                <div class="content vr">
                    <div class="img"></div>
                    <div class="text">
                        <h1>Виртуальная реальность<br/>для продажи квартир</h1>
                        <p>
                            3D визуализация квартиры в очках виртуальной реальности — новая веха в сфере продаж недвижимости и интерьерных решений.
                        </p>
                        <p>
                            С помощью очков Oculus Rift погружение в потрясающе четкий, детализированный 3D мир интерьеров помогает сформировать представление о недвижимости до ее покупки.
                        </p>
                    </div>
                </div>
                <span class="title">Виртуальная реальность<br/> для продажи квартир</span>
            </div>
            <div class="column column-default ">
                <div class="content base">
                    <div class="img"></div>
                    <div class="text">
                        <h1>Онлайн-база<br/> дизайн-проектов</h1>
                        <p>
                            В каталоге собраны эскиз-проекты для разных планировок
                            от лучших профессиональных дизайнеров интерьера. Из сотен решений обязательно можно выбрать подходящее именно вам.
                        </p>
                        <p>
                            Не нашли то, что искали? Дизайнеры к вашим услугам, чтобы разработать индивидуальный проект.
                        </p>
                    </div>
                </div>

                <span class="title">Онлайн-база<br/> дизайн-проектов</span>
            </div>
            <div class="column column-default">
                <div class="content widget">
                    <div class="img"></div>
                    <div class="text">
                        <h1>Продающий виджет</h1>
                        <p>
                            Удобный интерактивный инструмент подойдет застройщикам. Виджет легко встраивается в сайт и, предлагая покупателям квартир качественный дизайн интерьера, повышает лояльность клиентов и продажи недвижимости.
                        </p>
                        <p>
                            Наш виджет — тихий онлайн-помощник роста ваших доходов.
                        </p>

                    </div>
                </div>
                <span class="title">Продающий виджет<br/><br/></span>
            </div>
        </div>
        <script src="_cap/js/jquery-3.0.0.min.js"></script>
        <script src="_cap/js/jquery-ui.min.js"></script>
        <script src="_cap/js/main.js"></script>
    </body>
</html>
