<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "а тут, кейворды, рвзные");
$APPLICATION->SetPageProperty("description", "Тут дескрипшн страницы");
/* @var $APPLICATION */
//$APPLICATION->SetTitle("Список дизайнеров");
?>
  <style type="text/css">
.text-page .static-content { padding: 10px 0 0 !important; background-color: #F2F7F8 !important; }
  </style>
<div class="designer_content">
<?
 
 
 //получить массив всех пользователей и зафильтровать по группе
 //цикл всех пользователей
 
$DesignParams['GROUPS_ID'] = 5;
$DesignParams['IBLOCK_ID'] = 11;
$DesignParams['PROFILE_IMAGE'] = '/local/images/design.jpg';
$DesignersArr = array();
$key=0;
 
 
 
$rsUsers = CUser::GetList(($by="ID"),($order="desc"),Array("GROUPS_ID" => $DesignParams['GROUPS_ID']));
   while ($rsUser = $rsUsers->Fetch()):?>
<?
//echo '<pre>'; print_r($rsUser); echo '</pre>';
//$rsUser['PERSONAL_PHOTO']
$DesignParams['FILTER'] = array('IBLOCK_ID' => $DesignParams['IBLOCK_ID'], 'CREATED_BY' => $rsUser['ID']);

/* Подсчет количества проектов */
$rsUser['PROJECTS'] = CIBlockElement::GetList(
    array(),
    $DesignParams['FILTER'],
    array(),
    false,
    array('ID')
); 

//Картинки нет, картинка большого размера
$rsUser['PROFILE_IMAGE'] = CFile::GetPath($rsUser['PERSONAL_PHOTO']);

//Присваеваем картинку по умолчанию
if(!$rsUser['PROFILE_IMAGE']){
$rsUser['PROFILE_IMAGE'] = $DesignParams['PROFILE_IMAGE'];
}

/* Массив для хака сортировки */
$DesignersArr[] = array(
'PROFILE_IMAGE' => $rsUser['PROFILE_IMAGE'],
'ID' => $rsUser['ID'],
'NAME' => $rsUser['NAME'],
'LAST_NAME' => $rsUser['LAST_NAME'],
'PROJECTS' => $rsUser['PROJECTS']
);
$key++;
$projects_filter[$key]=$rsUser['PROJECTS'];


endwhile;

/* Хак для сортировки */
for($i=0; $i<100000; $i++){	
    array_multisort($projects_filter, SORT_DESC, SORT_NUMERIC, $DesignersArr, SORT_DESC);
}

?>
<div class="content-container">
	<h1 class="mainTitle">Поиск дизайнера</h1>
	<div class="form-q">
		<div class="content-container">
			<form enctype="application/x-www-form-urlencoded" method="post" class="form formTabber" action="/builder/" novalidate="novalidate">
				<div class="input-row">
					<input type="text" name="q" id="q" value="" class="required js-q" autocomplete="off" placeholder="Укажите любые данные об исполнителе">
					<a class="ico-search" href="javascript:void(0);" title=""></a>
					<span class="js-error error-message"></span>
				</div>
			</form>
		</div>
	</div>
	<div class="filter">
		<div class="js-sort sort">
			<span>Сортировать по:</span>
			<!--<a class="under-link" data-type="popular" data-method="">популярности</a>-->
			<a class="under-link" data-type="price" data-method="">цене</a>
			<a class="under-link" data-type="name" data-method="">отзывам</a>
			<!--<a class="under-link" data-type="size" data-method="">размеру</a>-->
		</div>					    
	</div>
	<?foreach ($DesignersArr as $rsUser):?>
	<div class="designer_item">
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2">
				<div class="designer_photo">
					<img class="designer_photo_inner" src="<?=$rsUser['PROFILE_IMAGE']?>">
					<div class="rating">
						<span class="y">&#9733;</span>
						<span class="y">&#9733;</span>
						<span class="y">&#9733;</span>
						<span class="y">&#9733;</span>
						<span class="n">&#9733;</span>
					</div>
					<div class="reviews">
						58 отзывов
					</div>
					<div class="status offline">
						<span>Не в сети</span>
					</div>
					<div class="status busy">
						<span>Занят</span>
					</div>
					<div class="status online">
						<span>Онлайн</span>
					</div>
					<div class="status our">
						<span>Свой статус</span>
					</div>
				</div>
			</div>
			<div class="col-xs-10 col-sm-10 col-md-10">
				<div class="designer_data">
					<div class="designer_top_data">
						<div class="designer_name">
							<a href="/designer/<?=$rsUser['ID']?>/"><?=$rsUser['NAME']?> <?=$rsUser['LAST_NAME']?></a>
						</div>
						<div class="designer_price">
						 500Р/час
						</div>
					</div>
					<div class="designer_location">
						Россия, Кемерово
					</div>
					<div class="designer_on_site_time">
						20 лет в сервисе
					</div>
					<div class="designer_discription">
						Bdlowcost - это проект организации студии дизайна, 
						специализирующейся на разработке и реализации интерьерных решений, 			
					</div>
					
					
					<div class="designer_projects">
						
						<div class="feature-out">
					<div class="feature-list row">
						  <div class="out-feature-item col-xs-4 col-sm-4 col-md-4">
				  <div class="" style="border: 1px solid rgba(1, 1, 1, 0.05); box-sizing: border-box; margin-bottom: 18px; overflow: hidden; padding: 20px 20px; position: relative;">
					<div style="padding-bottom: 10px;">
										  <img src="/upload/iblock/917/9178c1e889dfff0365adc81992d6a21f.jpg">
					  
					</div>
					<div style="position: relative; min-height: 60px">
					  <hr style="position: relative; top: 25px">
					  <img src="/upload/main/4f9/4f984a4fec40d1619eac470e477321ef.png" style="border: 2px solid #fafafa; border-radius: 25px; height: 47px; width: 47px; z-index:9px; position: inherit; box-shadow: 0 0 0 1px rgba(51, 67, 75, 0.22);">

					</div>
					<div>
					  <span style="color: #7c7b7b; font: 300 13px/20px 'Fira Sans',sans-serif;">Proomer </span>
					</div>
					<div style="height: 70px;">
					  <a href="/design/demo-proekt-v-stile-loft-3/" style="font-size: 15px; font-weight: bold; font-family: 'RobotoSlabRegular'; padding-top: 10px; min-height: 74px;">Демо-проект в стиле лофт #3 (43,3кв.м)</a>
					</div>
					<div style="padding: 15px 0">
					  <hr style="position: relative; top: 2px">
					  <div style="background-color: #3fb1da; width: 47px; height: 3px; margin: auto; position: relative"></div>
					</div>
					<div class="row">
					  <div class="col-md-6">
						<p style="color: #7c7b7b; font: 300 13px/20px 'Fira Sans',sans-serif; text-align: left; padding-top: 6px">
						  <span style="background: rgba(0, 0, 0, 0) url('/local/images/sprite_r.png') repeat scroll -55px -349px; display: inline-block; height: 18px; margin-right: 0; vertical-align: middle; width: 20px;"></span> 0                    </p>
					  </div>
					  <div class="col-md-6">
						<p class="price" style="bottom: 15px; color: #3fb1da; font: 400 2.1em 'RobotoRegular',sans-serif; text-align: right;">0 <span style="background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -30px -398px; display: inline-block; height: 16px; width: 10px;"></span></p>
					  </div>
					</div>
				  </div>
				</div>
						  <div class="out-feature-item col-xs-4 col-sm-4 col-md-4">
				  <div class="" style="border: 1px solid rgba(1, 1, 1, 0.05); box-sizing: border-box; margin-bottom: 18px; overflow: hidden; padding: 20px 20px; position: relative;">
					<div style="padding-bottom: 10px;">
										  <img src="/upload/iblock/40d/40def23510eebffce0b1b58fe9685387.jpg">
					  
					</div>
					<div style="position: relative; min-height: 60px">
					  <hr style="position: relative; top: 25px">
					  <img src="/upload/main/656/656e8e9ec6ad2360795e8a82d20adc10.jpeg" style="border: 2px solid #fafafa; border-radius: 25px; height: 47px; width: 47px; z-index:9px; position: inherit; box-shadow: 0 0 0 1px rgba(51, 67, 75, 0.22);">

					</div>
					<div>
					  <span style="color: #7c7b7b; font: 300 13px/20px 'Fira Sans',sans-serif;">Alina Mishenina</span>
					</div>
					<div style="height: 70px;">
					  <a href="/design/demo-proekt-v-stile-loft-1-72-83-kv-m/" style="font-size: 15px; font-weight: bold; font-family: 'RobotoSlabRegular'; padding-top: 10px; min-height: 74px;">Демо-проект в стиле лофт #1 (72,83 кв.м)</a>
					</div>
					<div style="padding: 15px 0">
					  <hr style="position: relative; top: 2px">
					  <div style="background-color: #3fb1da; width: 47px; height: 3px; margin: auto; position: relative"></div>
					</div>
					<div class="row">
					  <div class="col-md-6">
						<p style="color: #7c7b7b; font: 300 13px/20px 'Fira Sans',sans-serif; text-align: left; padding-top: 6px">
						  <span style="background: rgba(0, 0, 0, 0) url('/local/images/sprite_r.png') repeat scroll -55px -349px; display: inline-block; height: 18px; margin-right: 0; vertical-align: middle; width: 20px;"></span> 0                    </p>
					  </div>
					  <div class="col-md-6">
						<p class="price" style="bottom: 15px; color: #3fb1da; font: 400 2.1em 'RobotoRegular',sans-serif; text-align: right;">0 <span style="background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -30px -398px; display: inline-block; height: 16px; width: 10px;"></span></p>
					  </div>
					</div>
				  </div>
				</div>
						  <div class="out-feature-item col-xs-4 col-sm-4 col-md-4">
				  <div class="" style="border: 1px solid rgba(1, 1, 1, 0.05); box-sizing: border-box; margin-bottom: 18px; overflow: hidden; padding: 20px 20px; position: relative;">
					<div style="padding-bottom: 10px;">
										  <img src="/upload/iblock/787/787f3be645ea0ffb1a2df737744fdbd9.jpg">
					  
					</div>
					<div style="position: relative; min-height: 60px">
					  <hr style="position: relative; top: 25px">
					  <img src="/upload/main/4f9/4f984a4fec40d1619eac470e477321ef.png" style="border: 2px solid #fafafa; border-radius: 25px; height: 47px; width: 47px; z-index:9px; position: inherit; box-shadow: 0 0 0 1px rgba(51, 67, 75, 0.22);">

					</div>
					<div>
					  <span style="color: #7c7b7b; font: 300 13px/20px 'Fira Sans',sans-serif;">Proomer </span>
					</div>
					<div style="height: 70px;">
					  <a href="/design/demo-proekt-v-stile-loft-2-41-1-kv-m/" style="font-size: 15px; font-weight: bold; font-family: 'RobotoSlabRegular'; padding-top: 10px; min-height: 74px;">Демо-проект в стиле лофт #2 (41,1 кв.м)</a>
					</div>
					<div style="padding: 15px 0">
					  <hr style="position: relative; top: 2px">
					  <div style="background-color: #3fb1da; width: 47px; height: 3px; margin: auto; position: relative"></div>
					</div>
					<div class="row">
					  <div class="col-md-6">
						<p style="color: #7c7b7b; font: 300 13px/20px 'Fira Sans',sans-serif; text-align: left; padding-top: 6px">
						  <span style="background: rgba(0, 0, 0, 0) url('/local/images/sprite_r.png') repeat scroll -55px -349px; display: inline-block; height: 18px; margin-right: 0; vertical-align: middle; width: 20px;"></span> 0                    </p>
					  </div>
					  <div class="col-md-6">
						<p class="price" style="bottom: 15px; color: #3fb1da; font: 400 2.1em 'RobotoRegular',sans-serif; text-align: right;">0 <span style="background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -30px -398px; display: inline-block; height: 16px; width: 10px;"></span></p>
					  </div>
					</div>
				  </div>
				</div>
							</div>
				</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
<?endforeach; ?>
</div>
<div style="clear:both;"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>