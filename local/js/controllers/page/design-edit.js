(function($, APP) {
    'use strict';

    /**
     * Добавление дизайна
     **/
    APP.Controls.Page.DesignEdit = can.Control.extend({
        defaults: {
        },
        pluginName: 'designEdit',
        listenTo: ['designEdit.saveStep1Complete', 'designEdit.saveStep2Complete']
    }, {

        init: function() {
			var self = this;
            //this.runStepController(this.element.find(".js-first-step"), "DesignEditStep1");
            new APP.Controls.ProfileSidebar(this.element.find('.js-sidebar'));
			$("#accordion form.step2").each(function (i){
				new APP.Controls.ProfileSettingsForm(this);
			});
			new APP.Controls.ProfileSettingsForm($("form.step2-2"));
			new APP.Controls.ProfileSettingsForm($("form.step3"));
            if (this.element.find("#designId").val().length == 0){
                this.element.find(".js-second-step, .js-third-step, .js-fourth-step").addClass("disabled");
            }

			this.element.find("#accordion").accordion({
				heightStyle: "content"
			});

			$("#accordion .js-example-theme-multiple").select2({
				theme: "classic",
				placeholder: ""
			});
			
			$("#accordion .js-select-sinle-type-room").select2({
				theme: "classic",
				placeholder: "",
				maximumSelectionLength: 1
			});
			
			$(function() {$( "#accordion" ).accordion();});
			
			//Загрузчик документов
			new APP.Controls.DropzoneArea(this.element.find(".js-docs-plan-dropzone"), {
				url: '/plan/uploadplan',
				maxFilesize: 20,
				acceptedFiles: '.jpg, .png, .jpeg, .gif, .bmp, .doc, .docx, .xls, .xlsx, .pdf, .rar, .tar, .zip',
				files: true,
				paramsQuery: {
					designId: this.element.find('#planId').val(),
					fileType: "PROPERTY_DOCUMENTS"
				}
			});
			
			//Загрузчик документов
			new APP.Controls.DropzoneArea(this.element.find(".js-docs-design-main-dropzone"), {
				url: '/profile/design/upload-filec',
				maxFilesize: 20,
				acceptedFiles: '.doc, .docx, .xls, .xlsx, .pdf, .rar, .tar, .zip',
				files: true,
				paramsQuery: {
					designId: this.element.find('#designId').val(),
					fileType: "PROPERTY_DOCUMENTS"
				}
			});
			
			new APP.Controls.DropzoneArea(this.element.find(".js-docs-plan-option-dropzone"), {
				url: '/plan/planoptionuploadfile',
				maxFilesize: 20,
				acceptedFiles: '.jpg, .png, .jpeg, .gif, .bmp, .doc, .docx, .xls, .xlsx, .pdf, .rar, .tar, .zip',
				files: true,
				paramsQuery: {
					designId: this.element.find('#optionId').val(),
					fileType: "PROPERTY_DOCUMENTS"
				}
			});
										
			this.element.find('.js-docs-design-dropzone').each(function (i){
				var self_each = this;
				new APP.Controls.DropzoneArea($(this), {
					url: '/profile/design/upload-fileb',
					maxFilesize: 20,
					acceptedFiles: '.doc, .docx, .xls, .xlsx, .pdf, .rar, .tar, .zip',
					files: true,
					paramsQuery: {
						designId: function(el){return $(el).attr('data-id-design');}(self_each),
						fileType: "PROPERTY_DOCUMENTS"
					}
				});
			});

			//Загрузчик фото
			new APP.Controls.DropzoneArea(this.element.find('#accordion .js-imgs-dropzone'), {
				url: APP.urls.designEdit.uploadFile,
				maxFilesize: 5,
				files: false,
				multiple: true,
				acceptedFiles: '.jpg, .png, .jpeg, .gif, .bmp',
				paramsQuery: {
					designId: this.element.find('#designId').val(),
					fileType: "PROPERTY_IMAGES",
					resizeType: "PROMO_PHOTO"
				}
			});
			
			//Загрузчик фото
			new APP.Controls.DropzoneArea(this.element.find('.js-add-main-image'), {
				url: '/profile/design/upload-filec',
				maxFilesize: 5,
				files: false,
				multiple: false,
				acceptedFiles: '.jpg, .png, .jpeg, .gif, .bmp',
				paramsQuery: {
					designId: this.element.find('#designId').val(),
					fileType: "DETAIL_PICTURE",
					resizeType: "DROPZONE_MAIN_PHOTO"
				}
			});
			
			var active_tabs = this.element.find("#tabs").data('active');
			var disabled_tabs = this.element.find("#tabs").data('disabled');
			var tabs = this.element.find("#tabs");
	
			tabs.tabs({"active": active_tabs, "disabled": disabled_tabs});
			this.activePointMenu();
			
		    var preloader = new APP.Controls.PreloaderController(tabs);
			preloader.addPreloader(); 
			
			function fadeOutnojquery(el){
				preloader.removePreloader();
				$("#wrap-design-edit").css({'opacity' : '1'});
			}
			
			window.onload = function(){setTimeout(function(){fadeOutnojquery(tabs);},1000);};
        },
		
		'.js-up-menu click': function (el, e){
			e.preventDefault();
			var target = e.target;
			
			while (target != el) {
				if (target.tagName == 'LI') {
					if(!$(target).hasClass('disabled')){
						var point = $(target);
				
						if(point.attr('aria-controls') == 'documentation'){
							this.element.trigger("designEdit.getStep3Result");
						}
						else if(point.attr('aria-controls') == 'main-project'){
							this.element.trigger("designEdit.getStep2Result2");
						}
						else if(point.attr('aria-controls') == 'upload-project'){
							this.element.trigger("designEdit.getStep2Result");
						}
								
						el.find('li').each(function (i){
							if($(this).hasClass('active')){
								$(this).removeClass('active');
							}	
						});
						
						$(target).addClass('active');
						this.element.find('#top-menu > .nav').each(function (i) {	
							$(this).removeClass('active');				
						});
						var sidebar = this.element.find('#top-menu > .nav'+$('a', target).attr('href').replace('#', '.'));
						if(sidebar.length){
							$('.profile-nav-view-n2').show();
							sidebar.addClass('active');
						}
						else{
							
							$('.profile-nav-view-n2').hide();
						}
						this.element.find('#save-progress').attr('data-save-step', $(target).attr('data-step'));
						this.activePointMenu();
					};
					return;
				}
				target = target.parentNode;
			}		
		},
		
		'.js-select-address click': function(el, e) {
			$ajaxContent.ajaxl({
				topPreloader: false,
				url:'/search-service/step-plan-name/search',
				data: {'address': address},
				dataType: 'HTML',
				type: 'POST',
				success: this.proxy(function (data) {
					$ajaxContent.html(data);
				})
			});
		},
		
			
		'step4.getById': function(el, e){
	
			/*var target = e.target;		
			while (target != el) {
				if($(target).hasClass('js-docs-design-dropzone')){
					console.log(target)
					new APP.Controls.DropzoneArea($(target), {
						url: '/plan-option/upload-file',
						maxFilesize: 20,
						acceptedFiles: '.doc, .docx, .xls, .xlsx, .pdf, .rar, .tar, .zip',
						files: true,
						paramsQuery: {
							designId: this.element.find('#optionId').val(),
							fileType: "PROPERTY_DOCUMENTS"
						}
					});
				}
				target = target.parentNode;
			}
			
			*/
		/*	new APP.Controls.DropzoneArea(this.element.find(".js-docs-design-dropzone"), {
				url: '/plan-option/upload-file',
				maxFilesize: 20,
				acceptedFiles: '.doc, .docx, .xls, .xlsx, .pdf, .rar, .tar, .zip',
				files: true,
				paramsQuery: {
					designId: this.element.find('#optionId').val(),
					fileType: "PROPERTY_DOCUMENTS"
				}
			});*/
		},
		
		'.js-select-address click': function(el, e){	
			window.location.href = window.location.pathname+'?street='+el.attr('data-street')+'&number='+el.attr('data-number');
		},
		
		'.js-search-form click': function(el, e){
			window.location.href = window.location.pathname+'?street='+el.attr('data-street')+'&number='+el.attr('data-number');
		},
		
		'#address keyup': function(el, e){
            var address = el.val();
			var $ajaxContent = this.element.find('.result');
			var result = this.element.find('.search-form-design .result li');
			if((e.keyCode == 38 || e.keyCode == 40) && result.length > 0){
				var flag = 1;
				result.each(function( index ) {
					if($(this).hasClass('selected') && flag == 1 && e.keyCode == 40){
						$(this).removeClass('selected');
						$(this).next().addClass('selected');
						flag = 0
					}
					else if($(this).hasClass('selected') && flag == 1 && e.keyCode == 38){
						$(this).removeClass('selected');
						$(this).prev().addClass('selected');
						flag = 0
					}
				});
				if(flag == 1 && e.keyCode == 40){
					result.first().addClass('selected');
				}
				else if(flag == 1 && e.keyCode == 38){
					result.last().addClass('selected');
				}
			}
			else if(e.keyCode == 13){
				
					//result.last().addClass('selected');
					var number = '';
					var street = '';
					result.each(function(index){
						if($(this).hasClass('selected')){
							number = $(this).attr('data-number');
							street = $(this).attr('data-street');
							window.location.href = '/project/?street='+street+'&number='+number;
						}
					});	
			}
			else{
				$ajaxContent.ajaxl({
					topPreloader: false,
					url:'/search-service/step-plan-name/search',
					data: {'address': address},
					dataType: 'HTML',
					type: 'POST',
					success: this.proxy(function (data) {
						$ajaxContent.html(data);
					})
				});
			}
        },
		//событие на списке планировок
		'.js-list-plan click': function(el, e){
			var target = e.target;
			while (target.className != 'row') {
				//если клик на планировке(квартире)
				if($(target).hasClass('option') && $(target).hasClass('plan') && !$(target).hasClass('alien')){
					var id = target.getAttribute('data-id');
					var optionId = this.element.find("#optionId");
					
					optionId.attr('value', id);
					this.element.trigger("list.contentUpdatePlanList", {type:'planoption'});
					
					var filterParams = $('.js-filter-form').serializeArray();
				
					var validFilterParams = $.map(filterParams, function (el) {
						return el.value.length > 0 ? el : null;
					});

					var getStr = $.map(validFilterParams, function (el) {
						return el.name + "=" + el.value;
					}).join("&");
				
					var newUrl = location.origin + location.pathname + "?" + getStr;
				
					history.pushState({}, '', newUrl);
					return;
				}
				else if ($(target).hasClass('plan') && !$(target).hasClass('alien')) {
					$(target).addClass('selected');
					var planId = this.element.find("#designsPlan");
					planId.attr('value', '');
					if(target.getAttribute('data-id')){
						var id = target.getAttribute('data-id');
					}
					else if(target.getAttribute('data-id-plan')){
						var id = target.getAttribute('data-id-plan');
					}
					var planId = this.element.find("#planId");
					planId.attr('value', id);

					this.element.trigger("list.contentUpdatePlanList", {type:'plan'});
					
					var filterParams = $('.js-filter-form').serializeArray();
			
					var validFilterParams = $.map(filterParams, function (el) {
						return el.value.length > 0 ? el : null;
					});

					var getStr = $.map(validFilterParams, function (el) {
						return el.name + "=" + el.value;
					}).join("&");

					var newUrl = location.origin + location.pathname + "?" + getStr;
					history.pushState({}, '', newUrl);
					return;
				}
				target = target.parentNode;
			}
		},
		
		'.js-upload-plan click': function(el, e){
			e.preventDefault();
			this.element.find("#upload-plan").show();
			this.element.find("#upload-plan-option").hide();
			$.scrollTo($("#upload-plan"), 500);
		},
		
		'.js-upload-plan-option click': function(el, e){
			e.preventDefault();
			this.element.find("#upload-plan-option").show();
			this.element.find("#upload-plan").hide();
			$.scrollTo($("#upload-plan-option"), 500);
		},
		
		'.js-select-room click': function(el, e){
			e.preventDefault();
			if(e.target.tagName == 'A' && e.target.classList.contains('room')){
				var target = e.target;
				if(e.target.classList.contains('selected')){e.target.classList.remove('selected');
					if($("#countRoom").attr('value')){
						var arr_count_room = JSON.parse($("#countRoom").attr('value'));
					}
					else{
						var arr_count_room = [];
					}
					var i = 0;
				
					for(i; i <= arr_count_room.length; i++){
						if(arr_count_room[i] == target.getAttribute('data-value')){
							arr_count_room.splice(i, 1);
							break;
						}
					};
				
					$("#countRoom").attr('value',JSON.stringify(arr_count_room));
					this.element.trigger("list.contentUpdate", {});
				}
				else{
					e.target.classList.add('selected');
					if($("#countRoom").attr('value')){
						var arr_count_room = JSON.parse($("#countRoom").attr('value'));
					}
					else{
						var arr_count_room = [];
					}
					var i = 0;
					var flag = 1;
					for(i; i <= arr_count_room.length; i++){
						if(arr_count_room[i] == target.getAttribute('data-value')){
							flag = 0;
							break;
						}
					};
					if(flag == 1){
						arr_count_room.push(+target.getAttribute('data-value'));
					}
					$("#countRoom").attr('value',JSON.stringify(arr_count_room));
					this.element.trigger("list.contentUpdate", {});
				}
				var filterParams = $('.js-filter-form').serializeArray();

                var validFilterParams = $.map(filterParams, function (el) {
                    return el.value.length > 0 ? el : null;
                });

                var getStr = $.map(validFilterParams, function (el) {
                    return el.name + "=" + el.value;
                }).join("&");

                var newUrl = location.origin + location.pathname + "?" + getStr;
                history.pushState({}, '', newUrl);
			}
		},
		
		changeTypeRoom: function (el){
			var type_room = el.find('option:selected').attr('data-media-id');
			if(!el.find('.list-like-design').hasClass('flag')){
				el.find('.list-like-design').addClass('flag');
				el.find('.list-like-design ul').empty();
			}
			var $ajaxContent = el.find('.list-design');
			$ajaxContent.ajaxl({
				topPreloader: false,
				url:'/project/changetyperoom/',
				data: 'type_room='+type_room,
				dataType: 'HTML',
				type: 'POST',
				success: this.proxy(function (data){
					$ajaxContent.html(data);
					$(".knob").knob({
						//other parameters-----
						inputColor : '#34EB40'
					});
					el.find('#show-design').removeClass('desibled');
				})
			});
			
			var target = event.target;
			var type = +el.find('.type_room').serializeArray()[0].value;
	
			if(type == 1 				|| 
			   type == 2	   			||
			   type == 6				||
			   type == 4				||
			   type == 5
			){
				$('.used_room', el).addClass('noactive');
				$('.live_in_room', el).removeClass('noactive');
			}
			else if(type == 3 			|| 
					type == 7	   		||
					type == 8	   		
			){
				$('.live_in_room', el).addClass('noactive');
				$('.used_room', el).removeClass('noactive');
			}
			
		},
		
		'list.contentUpdate': function (el, e, param) {
			var $ajaxContent = this.element.find('.js-ajax-list-content');
			var data = [];

			if (param.page > 0) {
				data.push({
					name: "page",
					value: param.page
				})
			} else {
				data.push({
					name: "page",
					value: $("#page").val()
				})
			}

			if (param.viewCounter > 0) {
				data.push({
					name: "viewCounter",
					value: param.viewCounter
				})
			}

			this.element.find(".js-sort a").each(function(){
				data.push({
					name: "sort[" + $(this).data("type") + "]",
					value: $(this).data("method")
				})
			});

			$.merge(data, this.element.find('.js-filter-form').serializeArray());

			var self = this;
			$ajaxContent.ajaxl({
				topPreloader: true,
				url: location.pathname,
				data: data,
				dataType: 'HTML',
				type: 'POST',
				success: this.proxy(function (data) {
					$ajaxContent.html(data);
					if ($ajaxContent.find('.not-found').length > 0) {
					
					} else {
						
					}
					this.element.find('.preview360-gallery').preview360({});
					new APP.Controls.Pagination(this.element.find(".js-pagination"));
					$('.list-plan .plan-item').removeClass('hide').addClass('show');
					APP.Controls.FancyboxLink.initList(this.element.find(".js-fancybox"));
				})
			});
		},
		
		'list.contentUpdatePlanList': function (el, e, param) {
			var $ajaxContent = this.element.find('.js-ajax-list-content');
			var data = [];

			if (param.page > 0) {
				data.push({
					name: "page",
					value: param.page
				})
			} else {
				data.push({
					name: "page",
					value: $("#page").val()
				})
			}

			if (param.viewCounter > 0) {
				data.push({
					name: "viewCounter",
					value: param.viewCounter
				})
			}

			this.element.find(".js-sort a").each(function(){
				data.push({
					name: "sort[" + $(this).data("type") + "]",
					value: $(this).data("method")
				})
			});

			$.merge(data, this.element.find('.js-filter-form').serializeArray());

			var self = this;
			$ajaxContent.ajaxl({
				url: location.pathname,
				data: data,
				dataType: 'HTML',
				type: 'POST',
				success: this.proxy(function (data) {
					$ajaxContent.html(data);
	
					if(param.type == 'plan'){
						$.scrollTo($(".list-plan-options").offset().top, 500);
					}
					else if(param.type == 'planoption'){
					//	$.scrollTo($(".list-plan-design").offset().top, 500);
						var btn_save_progress = self.element.find("#save-progress");
						if(btn_save_progress.hasClass('noLink')){
							btn_save_progress.removeClass('noLink');
						}
						$('html,body').animate({scrollTop: $('body').offset().top},500);
					}
					if ($ajaxContent.find('.not-found').length > 0) {
					
					} else {
						
					}
					this.element.find('.preview360-gallery').preview360({});
					new APP.Controls.Pagination(this.element.find(".js-pagination"));
					$('.list-plan .plan-item').removeClass('hide').addClass('show');
					APP.Controls.FancyboxLink.initList(this.element.find(".js-fancybox"));				
				})
			});
		},
		
		'.js-show-hidden-filter click': function (el) {
			if(el.hasClass('open')){
				this.element.find(".btn_for_filter").removeClass('open').addClass('close');
				this.element.find("#filter_plan .filter_project").hide();
			}
			else{
				this.element.find(".btn_for_filter").removeClass('close').addClass('open');
				this.element.find("#filter_plan .filter_project").show();
			}
		},
		
		'.js-add-room click': function (el) {
			var template_room = this.element.find("#template_room");
		
			var html = template_room.html();
			var accordion = this.element.find("#accordion");
			//accordion.accordion('destroy');
			accordion.append(html);
			accordion.accordion("refresh");

			var new_room = $('#accordion .step2').last();
			var index = $('#accordion').index('#accordion .step2:last');
			accordion.accordion({active:index});
			new_room.find(".js-example-theme-multiple").select2({
				theme: "classic",
				placeholder: "",
			});
			new_room.find(".js-select-sinle-type-room").select2({
				theme: "classic",
				placeholder: "",
				maximumSelectionLength: 1
			});
			this.activePointMenu();
			new APP.Controls.DropzoneArea($('#accordion .step2 .js-imgs-dropzone').last(), {
				url: APP.urls.designEdit.uploadFile,
				maxFilesize: 5,
				files: false,
				multiple: true,
				acceptedFiles: '.jpg, .png, .jpeg, .gif, .bmp',
				paramsQuery: {
					designId: this.element.find('#designId').val(),
					fileType: "PROPERTY_IMAGES",
					resizeType: "PROMO_PHOTO"
				}
			});
			
			var step = $("#step").val();
			if(step >= 3){
				var count_project = accordion.find('form.step2').length;
				if(count_project > 1){
					$('.js-up-menu li[data-step = "step2-2"]').removeClass('disabled');
					
					var active_tabs = this.element.find("#tabs").data('active');
					var disabled_tabs = this.element.find("#tabs").data('disabled');
					var tabs = this.element.find("#tabs");
		
					for(var i = 0; i < disabled_tabs.length; i++){
						if(disabled_tabs[i] == 2){
							disabled_tabs.splice(i, 1);
						}
					}
					
					this.element.find("#tabs").attr('data-disabled', disabled_tabs);

					tabs.tabs({"disabled": disabled_tabs});
					
						
				}
			}
			new APP.Controls.ProfileSettingsForm($("#accordion form.step2").last());
		},
		
		'.js-save-progress click': function (el, e){
			e.preventDefault();

			var step = el.attr('data-save-step');
				
			switch (step) {
                case 'step1':
                    this.element.trigger("designEdit.saveStep1Complete");
					break;
                case 'step2':
                    this.element.trigger("designEdit.saveStep2Complete");
                    break;
				case 'step2-2':
                    this.element.trigger("designEdit.saveStep2Complete2");
                    break;
                case 'step3':
                    this.element.trigger("designEdit.saveStep3Complete");
                    break;

            }
			
		},
		
		'.js-file-delete click': function (el) {
			this.element.ajaxl({
				url: APP.urls.designEdit.deleteFile,
				data: {designId:this.element.find("#designId").val(), fileType: "PROPERTY_DOCUMENTS"},
				dataType: 'JSON',
				type: 'POST',
				success: this.proxy(function (data) {
					el.closest(".upload-doc").remove();
					$(window).trigger("dropzone.checkItems");
				})
			});
        },
		
		'.js-file-design-room-delete click': function (el) {
			this.element.ajaxl({
				url: APP.urls.designRoomEdit.deleteFile,
				data: {designId:el.parent().data('value-id'), fileType: "PROPERTY_DOCUMENTS"},
				dataType: 'JSON',
				type: 'POST',
				success: this.proxy(function (data) {
					el.closest(".upload-doc").remove();
					$(window).trigger("dropzone.checkItems");
				})
			});
        },
		
		'.js-delete-room click': function (el) {
			APP.helpers.showFancyboxDelete('Вы уверены, что хотите удалить этот проект?', this.removeDesign, el, this);
		},
		
		'designEdit.saveStep1Complete': function () {
           /* var $nextStepBlock = this.element.find("body");
            this.runStepController($nextStepBlock, "DesignEditStep1");
			
           // $nextStepBlock.removeClass("success").addClass("active");
           // $nextStepBlock.find(".js-step-body").slideDown(500);*/
			var self = this;
			var $nextStepBlock = this.element.find(".js-second-step");
            this.runStepController($nextStepBlock, "DesignEditStep2");
		   
			var dataParams = $('form.step1').serializeArray();
			
			var validFilterParams = $.map(dataParams, function (el) {
				return el.value.length > 0 ? el : null;
			});

			var getStr = $.map(validFilterParams, function (el) {
				return el.name + "=" + el.value;
			}).join("&");
			
			var designId = this.element.find("#designId").val();
			dataParams.push({name:'designId', value:designId});
			
			this.element.ajaxl({
				url: '/profile/design/save/first',
				data: dataParams,
				dataType: 'JSON',
				type: 'POST',
				success: this.proxy(function (data) {
					if(data.result){
						$("#designId").val(data.newId);
						$('.js-up-menu li').each(function (i){
							if($(this).hasClass('active')){
								$(this).removeClass('active').attr('aria-selected', false).attr('aria-expanded', false);
							}
							if($('.js-up-menu li[data-step = "step2"]')){
								$('.js-up-menu li[data-step = "step2"]').addClass('active').removeClass('disabled').attr('aria-selected', true).attr('aria-expanded', true);
								//$('#select-object').attr('aria-hidden', true).hide();
								//$('#upload-project').attr('aria-hidden', false).show();
			
								var disabled_tabs = self.element.find("#tabs").data('disabled');
								
								for(var i = 0; i < disabled_tabs.length; i++){
									if(disabled_tabs[i] == 1){
										disabled_tabs.splice(i, 1);
									}
								}
								self.element.find("#tabs").attr('data-disabled', '['+disabled_tabs+']');
								self.element.find("#tabs").tabs({"active": 1, "disabled": disabled_tabs});
								
								self.element.find("#tabs").tabs({active: 1});
							}
							$('#save-progress').attr('data-save-step', 'step2');
						});
						var step = $("#step").val();
						if(step < 2){
							$("#step").attr('value', 2);
						}
						//this.element.find("#tabs").tabs({"active": 1, "disabled": [2,3]});
						self.element.trigger("designEdit.getStep2Result");
						history.pushState({}, 'Редактирование проекта', location.origin + "/profile/design/edit/" + data.newId + "/?" + getStr);
					}
				})
            });	   
        },
		
		'designEdit.saveStep2Complete': function () {			
			var dataParams = $('form.step2').serializeArray();
			var designId = $("#designId").val();
			dataParams.push({name:'designId', value:designId});
			var count_project = $('#accordion form.step2').length;
			var q = 0;
			var self = this;
			var designIds = [];
			var errorForm = [];
			
			//чек формы
			$('#accordion form.step2').each(function (index) {
				$("#accordion").accordion({
					active: index
				});
				$('a.check', this).click();
				var flag = 1;
				var input_row = $('.input-row', this);
				input_row.each(function (){
					if($(this).hasClass('error')){		
						if(flag == 1){
							errorForm.push(index);
						}
						flag = 0;
					};
				})
			})
			
			if(errorForm.length == 0){
				$('#accordion form.step2').each(function (index) {
					var form_step2 = $(this);
					var dataParams = $(this).serializeArray();
				
					setTimeout(self.element.ajaxl({
						url: '/profile/design/save/second?mainDesignId='+designId,
						data: dataParams,
						dataType: 'JSON',
						type: 'POST',
						success: self.proxy(function (data) {	
							q+=1;
							if(data.newId){
								designIds.push(data.newId);
								form_step2.find('.designId').val(data.newId);
								form_step2.find('.roomId').val(data.roomId);
							}
							if(q == count_project && q > 1){							
								$('.js-up-menu li').each(function (i){
									if($(this).hasClass('active')){
										$(this).removeClass('active').attr('aria-selected', false).attr('aria-expanded', false);
									}
									if($('.js-up-menu li[data-step = "step2-2"]')){							
										$('.js-up-menu li[data-step = "step2-2"]').addClass('active').removeClass('disabled').attr('aria-selected', true).attr('aria-expanded', true);
										//$('#upload-project').attr('aria-hidden', true).hide();
										//$('#main-project').attr('aria-hidden', false).show();
										self.element.find("#tabs").tabs({active: 2});
										
										var disabled_tabs = self.element.find("#tabs").data('disabled');

										for(var i = 0; i < disabled_tabs.length; i++){
											if(disabled_tabs[i] == 2){
												disabled_tabs.splice(i, 1);
											}
										}
										self.element.find("#tabs").attr('data-disabled', '['+disabled_tabs+']');
										self.element.find("#tabs").tabs({"active": 2, "disabled": disabled_tabs});
									
									}
								});
								$('#save-progress').attr('data-save-step', 'step2-2');
												
								var step = $("#step").val();
								if(step < 3){
									$("#step").attr('value', 3);
								}
								self.element.trigger("designEdit.getStep2Result2")


								/*self.element.ajaxl({
									url: '/profile/design/editb/?designIds='+JSON.stringify(designIds)+'&designId='+this.element.find("#designId").val(),
									dataType: 'JSON',
									type: 'POST',
									success: self.proxy(function (data) {
										$('#save-progress').attr('data-save-step', 'step2-2');
														
										function explode(){
											self.element.trigger("designEdit.getStep2Result2")
										}
										setTimeout(explode, 1000);
									})
								})*/
									
							}
							else if(q == count_project && q == 1){
								$('.js-up-menu li').each(function (i){
									if($(this).hasClass('active')){
										$(this).removeClass('active').attr('aria-selected', false).attr('aria-expanded', false);
									}
									if($('.js-up-menu li[data-step = "step3"]')){							
										$('.js-up-menu li[data-step = "step3"]').addClass('active').removeClass('disabled').attr('aria-selected', true).attr('aria-expanded', true);
										self.element.find("#tabs").tabs({active: 3});
										
										var disabled_tabs = self.element.find("#tabs").data('disabled');

										for(var i = 0; i < disabled_tabs.length; i++){
											if(disabled_tabs[i] == 3){
												disabled_tabs.splice(i, 1);
											}
										}
										self.element.find("#tabs").attr('data-disabled', '['+disabled_tabs+']');
										self.element.find("#tabs").tabs({"active": 3, "disabled": disabled_tabs});
									
										
									}
								});
								
								self.element.ajaxl({
									url: '/profile/design/editb/?designIds='+JSON.stringify(designIds)+'&designId='+this.element.find("#designId").val(),
									dataType: 'JSON',
									type: 'POST',
									success: self.proxy(function (data) {
										$('#save-progress').attr('data-save-step', 'step3');
										self.element.trigger("designEdit.getStep3Result")

									})
								})
								
								var step = $("#step").val();
								if(step < 4){
									$("#step").attr('value', 4);
								}
								
							}
							//history.pushState({}, 'Редактирование проекта', location.origin + "/profile/design/edit/" + data.newId + "/");
						})
					}), 1000);
				});
			}
			else{
				$( "#accordion" ).accordion("refresh");
				$( "#accordion" ).accordion({
					active: errorForm.shift()
				});
			}
        },
		
		'designEdit.saveStep2Complete2': function () {
			var self = this;
			var dataParams = $('form.step2-2').serializeArray();
			var design = this.element.find("#designId").val();
			dataParams.push({name:'designId', value:design});
			
			var errorForm = [];
			//чек формы
			$('form.step2-2').each(function (index) {
				$('a.check', this).click();
				var flag = 1;
				var input_row = $('.input-row', this);
				input_row.each(function (){
					if($(this).hasClass('error')){		
						if(flag == 1){
							errorForm.push(index);
						}
						flag = 0;
					};
				})
			})
			
			if(errorForm.length == 0){
				this.element.ajaxl({
					url: '/profile/design/save/second/second',
					data: dataParams,
					type: 'POST',
					success: this.proxy(function (data) {
						$('.js-up-menu li').each(function (i){
							if($(this).hasClass('active')){
								$(this).removeClass('active').attr('aria-selected', false).attr('aria-expanded', false);
							}
							if($('.js-up-menu li[data-step = "step3"]')){							
								$('.js-up-menu li[data-step = "step3"]').addClass('active').removeClass('disabled').attr('aria-selected', true).attr('aria-expanded', true);
								//$('#upload-project').attr('aria-hidden', true).hide();
								//$('#main-project').attr('aria-hidden', false).show();
								self.element.find("#tabs").tabs({active: 3});
								
								var disabled_tabs = self.element.find("#tabs").data('disabled');

								for(var i = 0; i < disabled_tabs.length; i++){
									if(disabled_tabs[i] == 3){
										disabled_tabs.splice(i, 1);
									}
								}
								
								self.element.find("#tabs").attr('data-disabled', '['+disabled_tabs+']');
								self.element.find("#tabs").tabs({"active": 3, "disabled": disabled_tabs});
							}
						});
						$('#save-progress').attr('data-save-step', 'step3');
						
						var step = $("#step").val();
						if(step < 4){
							$("#step").attr('value', 4);
						}
						
						self.element.trigger("designEdit.getStep3Result")

					})
				});
			}
		},
		
		'designEdit.saveStep3Complete': function () {
			var dataParams = [];
			var design = this.element.find("#designId").val();
			dataParams.push({name:'designId', value:design});
			
			var errorForm = [];
			//чек формы
			$('form.step3').each(function (index) {
				$('a.check', this).click();
				var flag = 1;
				var input_row = $('.input-row', this);
				input_row.each(function (){
					if($(this).hasClass('error')){		
						if(flag == 1){
							errorForm.push(index);
						}
						flag = 0;
					};
				})
			})
			
			if(errorForm.length == 0){
				this.element.ajaxl({
					url: '/profile/design/save/third',
					data: dataParams,
					type: 'POST',
					dataType: 'JSON',
					success: this.proxy(function (data) {
						if(data.result){
							document.location.href = location.origin + "/profile/design/";
						}
					})
				})
			}
        },
		
		//Подгружает данные для второго шага
		'designEdit.getStep2Result': function () {
			var $ajaxContent = this.element.find('#ajax-get-step-second');
			var design = this.element.find("#designId").val();
			var dataParams = [{name:'designId', value:design}];
			var self = this;
			$ajaxContent.ajaxl({
				url: '/profile/design/get/second/',
				data: dataParams,
				type: 'POST',
				success: this.proxy(function (data) {
					//var result = JSON.stringify(data.designDesigns);
					//var result = JSON.parse(data);
					$ajaxContent.html(data);
					this.element.find("#accordion").accordion({
						heightStyle: "content"
					});
					
					$("#accordion .js-example-theme-multiple").select2({
						theme: "classic",
						placeholder: "",
					});
					
					$("#accordion .js-select-sinle-type-room").select2({
						theme: "classic",
						placeholder: "",
						maximumSelectionLength: 1
					});
					$("#accordion form.step2").each(function (i){
						new APP.Controls.ProfileSettingsForm(this);
					});
					new APP.Controls.DropzoneArea(self.element.find('#accordion .js-imgs-dropzone'), {
						url: APP.urls.designEdit.uploadFile,
						maxFilesize: 5,
						files: false,
						multiple: true,
						acceptedFiles: '.jpg, .png, .jpeg, .gif, .bmp',
						paramsQuery: {
							designId: self.element.find('#designId').val(),
							fileType: "PROPERTY_IMAGES",
							resizeType: "PROMO_PHOTO"
						}
					});			
				})
            });
			new APP.Controls.ProfileSettingsForm($("#accordion form.step2"));
		},
		
		//Подгружает данные для второго(доп) шага
		'designEdit.getStep2Result2': function () {
			var $ajaxContent = this.element.find('#ajax-get-step-second-second');
			var designId = $("#designId").val();
			var dataParams = [{name:'designId', value:designId}];
			var self = this;
			$ajaxContent.ajaxl({
				url: '/profile/design/get/second/second',
				data: dataParams,
				type: 'POST',
				success: this.proxy(function (data) {
					$ajaxContent.html(data);
					//Загрузчик фото
					new APP.Controls.ProfileSettingsForm($("form.step2-2"));
					new APP.Controls.DropzoneArea(self.element.find('.js-add-main-image'), {
						url: '/profile/design/upload-filec',
						maxFilesize: 5,
						files: false,
						multiple: false,
						acceptedFiles: '.jpg, .png, .jpeg, .gif, .bmp',
						paramsQuery: {
							designId: self.element.find('#designId').val(),
							fileType: "DETAIL_PICTURE",
							resizeType: "DROPZONE_MAIN_PHOTO"
						}
					});
				})
            });
		},
		
		//Подгружает данные для третьего шага
		'designEdit.getStep3Result': function () {
			var $ajaxContent = this.element.find('#ajax-get-step-third');
			var designId = $("#designId").val();
			var dataParams = [{name:'designId', value:designId}];
			var self = this;

			$ajaxContent.ajaxl({
				url: '/profile/design/get/third',
				data: dataParams,
				type: 'POST',
				success: this.proxy(function (data) {
					$ajaxContent.html(data);
					new APP.Controls.ProfileSettingsForm($("form.step3"));
					//Загрузчик документов
					new APP.Controls.DropzoneArea(self.element.find(".js-docs-design-main-dropzone"), {
						url: '/profile/design/upload-filec',
						maxFilesize: 20,
						acceptedFiles: '.doc, .docx, .xls, .xlsx, .pdf, .rar, .tar, .zip',
						files: true,
						paramsQuery: {
							designId: self.element.find('#designId').val(),
							fileType: "PROPERTY_DOCUMENTS"
						}
					});
					
					self.element.find('.js-docs-design-dropzone').each(function (i){
						var self_each = this;
						new APP.Controls.DropzoneArea($(self_each), {
							url: '/profile/design/upload-fileb',
							maxFilesize: 20,
							acceptedFiles: '.doc, .docx, .xls, .xlsx, .pdf, .rar, .tar, .zip',
							files: true,
							paramsQuery: {
								designId: function(el){return $(el).attr('data-id-design');}(self_each),
								fileType: "PROPERTY_DOCUMENTS"
							}
						});
					});		
				})
            });
		},
		
		'.js-room-project click': function (el, e) {
			e.preventDefault();
			var target = e.target;
			while(target != el){
				if($(target).hasClass('js-edit-project')){
					var target = $(target);
					target.removeClass('js-edit-project').addClass('js-save-project');
					target.removeClass('edit-project').addClass('save-project');
					target.html('<a href="#">Сохранить</a>');
					/*$('.js-room-project').each(function(){
						if($(this).hasClass('active')){
							$(this).removeClass('active');
						}
					});*/		
					el.addClass('active');
					break;
				}
				else if($(target).hasClass('js-save-project')){
					var target = $(target);
					var roomId = target.closest('.edit-block').find('.roomId').val();
					var mainDesignId = $('#designId').val();
					var designId = target.closest('.edit-block').find('.designId').val();
					var data = target.closest('.edit-block').find('form').serializeArray();
					this.savePriceProject(el, target, mainDesignId, designId, data);
				}
				target = target.parentNode;
			};
		},
		
		'.js-select-sinle-type-room change': function (el, e){	
			var type_room = '';
			el.find('option:selected').each(function() {
				type_room = $(this).data('name-case-1');
			});
			if(type_room.length > 0){$(el).parents("div.ui-accordion-content").prev().html('Дизайн-проект '+type_room+'<span title="Удалить комнату" class="delete_room js-delete-room">x</span>')};
		},
		
		'.ui-accordion-header click': function (el, e){
			this.activePointMenu();
		},
		
		'.js-input-price-square input': function (el, e){
			var price_square = el.val();
			var area = el.closest('.edit-block').find('.area').val();
			var total_price_design_room = price_square * area;
			el.closest('.edit-block').find('.price-design-room input').val(total_price_design_room)
		},
		
        '.js-step-title click': function (el, e) {
            if (!(el.closest(".js-step").hasClass("disabled"))) {
                this.runStep(el.closest(".js-step").data("step"));
            }
        },
	
        runStepController: function ($element, controller) {
            if (!$element.data(controller)) {
				
                $element.data(controller, true);
				
                this[controller] = new APP.Controls[controller]($element, {
                  //  parent: this.element,
                 //   designIdInput: this.element.find('#designId')
                });
            }

            if (this.element.find("#designId").val().length > 0) {
                this.element.find(".js-second-step, .js-third-step, .js-fourth-step").removeClass("disabled");
            }
        },
		
		removeDesign: function ($element, controller) {
			var index = $($element).index('.js-delete-room');
			var id_room = $element.data('id-room');
			var $ajaxContent = $('#upload-project');
			var designId = $("#designId").val();
			var dataParams = [{name:'designId', value:designId}];
			if(id_room){
				$ajaxContent.ajaxl({
					url: '/profile/design/delete-room/'+id_room,
					dataType: 'JSON',
					data: dataParams,
					type: 'POST',
					success: function (data) {
						if(data.success){
							$($element).parent('h3').next('div').andSelf().remove();
							removeDesignCount();
						}
					}
				});	 
			}
			else{
				$($element).parent('h3').next('div').andSelf().remove();
				removeDesignCount();
			};
			
			function removeDesignCount(){
				var count_project = $('#accordion form.step2').length;
				var step = $("#step").val();
				if(step >= 3){
					if(count_project <= 1){
						$('.js-up-menu li[data-step = "step2-2"]').addClass('disabled');
						
						var tabs = $("#tabs");
						
						var active_tabs = tabs.data('active');
						var disabled_tabs = tabs.data('disabled');
					
						var flag = 1;
						for(var i = 0; i < disabled_tabs.length; i++){
							if(disabled_tabs[i] == 2){
								flag = 0;
							}
						}
						if(flag == 1){
							disabled_tabs.push(2);
						}
						tabs.attr('data-disabled', disabled_tabs);
						tabs.tabs({"disabled": disabled_tabs});		
					}
				}
			};		
        },
				
        runStep: function (stepNumber) {
            var $activeStep = this.element.find(".js-step").filter(".active");
            if ($activeStep.data("step") == stepNumber) return;

            $activeStep.find(".js-step-body").slideUp(500, function () {
                $activeStep.removeClass("active");
                $activeStep.addClass("success");
                $activeStep.removeAttr("style");
            });

            switch (stepNumber) {
                case 1:
                    this.element.find(".js-first-step").removeClass("success").addClass("active");
                    this.element.find(".js-first-step").find(".js-step-body").slideDown(500);
                    break;
                case 2:
                    this.element.trigger("designEdit.saveStep1Complete");
                    break;
                case 3:
                    this.element.trigger("designEdit.saveStep2Complete");
                    break;
                case 4:
                    this.element.trigger("designEdit.saveStep3Complete");
                    break;
            }
        },
		
		activePointMenu: function(){
			var lastId,
				topMenu = $("#top-menu"),
				topMenuHeight = topMenu.outerHeight()+15,
				// All list items
				menuItems = topMenu.find("a"),
				// Anchors corresponding to menu items
				scrollItems = menuItems.map(function(){

				  var item = $($(this).attr("href"));
					if ($(item).is(':visible')) { 
						if (item.length) { return item; }
					}
				});
				
  
			// Bind click handler to menu items
			// so we can get a fancy scroll animation
			menuItems.click(function(e){
			  var href = $(this).attr("href"),
				  offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
			  $('html, body').stop().animate({ 
				  scrollTop: offsetTop
			  }, 300);
			  e.preventDefault();
			});

			// Bind to scroll
			$(window).scroll(function(){
			   // Get container scroll position
			   var fromTop = $(this).scrollTop()+topMenuHeight;
			   
			   // Get id of current scroll item
			   var cur = scrollItems.map(function(){
				 if ($(this).offset().top < fromTop)
				   return this;
			   });

			   // Get the id of the current element
			   cur = cur[cur.length-1];

			   var id = cur && cur.length ? cur[0].id : "";
	
			   if (lastId !== id) {
				   lastId = id;
				   // Set/remove active class
				   menuItems
					 .parent().removeClass("active")
					 .end().filter("[href='#"+id+"']").parent().addClass("active");
			   }                   
			});
		},
		
		savePriceProject: function(el, target, mainDesignId, id, data){
			if(id){
				this.element.ajaxl({
					url: '/profile/design/save/second?designId='+id+'&mainDesignId='+mainDesignId,
					dataType: 'JSON',
					data: data,
					type: 'POST',
					success: this.proxy(function(data){
						target.removeClass('js-save-project').addClass('js-edit-project');
						target.removeClass('save-project').addClass('edit-project');
						target.html('<a href="#">Редактировать</a>');						
						$('.js-room-project').each(function(){
							if($(this).hasClass('active')){
								$(this).removeClass('active');
							}
						});	
						el.removeClass('active');
					})
				});	 
			}
		},
		
		'.js-save-your-life click': function(el, e){
			e.preventDefault();
			var target = e.target;
		}
/*=======================================================================*/
    });

})(jQuery, window.APP);