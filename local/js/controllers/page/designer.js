(function($, APP) {
    'use strict';

    /**
     * Контроллер страницы дизайнера
     **/
    APP.Controls.Page.Designer = can.Control.extend({

        init: function() {
            new APP.Controls.Pagination(this.element.find(".js-pagination"));
            new APP.Controls.Likes.initList(this.element.find('.js-like'));
            APP.Controls.AddBasket.initList(this.element.find(".js-add-basket"));
			new APP.Controls.DesignerFeedbackForm(this.element.find(".js-designer-feedback-form"));
			new APP.Controls.Sorting(this.element.find(".js-sort"));
			this.element.find('.preview360-gallery').preview360({});
        },
		
		'.js-search-form click': function(e){
			var $ajaxContent = this.element.find('.js-ajax-list-content');
            var q = this.element.find('#form_search_q').val();		
			if (q != undefined) {
				this.element.trigger("list.contentUpdate", {q: q});
			}
		},
		
        'list.contentUpdate': function (el, e, param) {
            var $ajaxContent = this.element.find('.js-ajax-list-content');
            var data = [];

            if (param.page > 0) {
                data.push({
                    name: "page",
                    value: param.page
                })
            } else {
                data.push({
                    name: "page",
                    value: 1
                })
            }
			if (param.q && param.q.length > 0) {
				data.push({
					name: "q",
					value: param.q
				})
			}
			
			
			this.element.find(".js-sort a").each(function(){
                data.push({
                    name: "sort[" + $(this).data("type") + "]",
                    value: $(this).data("method")
                })
            });

            $ajaxContent.ajaxl({
                url: location.pathname,
                data: data,
				topPreloader: true,
                dataType: 'HTML',
                type: 'POST',
                success: this.proxy(function (data) {
                    $ajaxContent.html(data);
                    new APP.Controls.Pagination(this.element.find(".js-pagination"));
                    //$.scrollTo(this.element.find('.profile-content'), 500);
                })
            });
        }
    });

})(jQuery, window.APP);