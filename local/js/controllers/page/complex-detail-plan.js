(function($, APP) {
    'use strict';
    /**
     * 
     **/
    APP.Controls.Page.ComplexDetailPlan = can.Control.extend({
		
        init: function() {
			var self = this;
			this.element.find('.preview360-gallery').preview360({});
        },
		
		'.js-list-plan click': function (el, e){
			var target = e.target;
			while (target != el){
				if ($(target).is('.list-plan .plan')){
					var id = $(target).attr('data-id');
					var optionsPlan = this.element.find("#optionsPlan");
					optionsPlan.attr('value', id);
					this.element.trigger("list.contentUpdate", {});
					var filterParams = $('.js-filter-form').serializeArray();
					console.log(filterParams)
					var validFilterParams = $.map(filterParams, function (el) {
					return el.value.length > 0 ? el : null;
					});

					var getStr = $.map(validFilterParams, function (el) {
					return el.name + "=" + el.value;
					}).join("&");

					var newUrl = location.origin + location.pathname + "?" + getStr;
					history.pushState({}, '', newUrl);
					return;
					return;
				}
				target = target.parentNode;
			}
		},
		
		'.js-list-design click': function (el, e){
			var target = e.target;
			while (target != el){
				console.log(target)
				if ($(target).is('.design.hint')){	
					$(target).removeClass('hint');
					return;
				}
				target = target.parentNode;
			}
		},
		
		'list.contentUpdate': function (el, e, param) {
            var $ajaxContent = this.element.find('.js-ajax-list-content');
            var data = [];

            if (param.page > 0) {
                data.push({
                    name: "page",
                    value: param.page
                })
            } else {
                data.push({
                    name: "page",
                    value: 1
                })
            }

            if (param.viewCounter > 0) {
                data.push({
                    name: "viewCounter",
                    value: param.viewCounter
                })
            }

            this.element.find(".js-sort a").each(function(){
                data.push({
                    name: "sort[" + $(this).data("type") + "]",
                    value: $(this).data("method")
                })
            });

            $.merge(data, this.element.find('.js-filter-form').serializeArray());

            var self = this;
			var filterParams = $('.js-filter-form').serializeArray();

                var validFilterParams = $.map(filterParams, function (el) {
                    return el.value.length > 0 ? el : null;
                });

                var getStr = $.map(validFilterParams, function (el) {
                    return el.name + "=" + el.value;
                }).join("&");

                var newUrl = location.origin + location.pathname + "?" + getStr;
                history.pushState({}, '', newUrl);
				$ajaxContent.ajaxl({
					url: location.pathname,
					data: data,
					dataType: 'HTML',
					type: 'POST',
					success: this.proxy(function (data) {
						$ajaxContent.html(data);
						$('.content-plan-block').removeClass('disabled');
						$.scrollTo($(".list-plan"), 500);
						this.element.find('.preview360-gallery').preview360({});
					})
				});
        }
    });

})(jQuery, window.APP);