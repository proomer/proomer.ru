(function($, APP) {
    'use strict';
    /**
     * Контроллер детальной страницы жилого коплекса
     **/
    APP.Controls.Page.CompetitionList = can.Control.extend({

        init: function() {
			var self = this;
			new APP.Controls.FeedbackForm(this.element.find(".js-feedbackbuilder-form"));
			new APP.Controls.FilterForm(this.element.find(".js-filter-form"));
        },

        '.js-q input': function(el) {
            var q = el.val();
			var $ajaxContent = this.element.find('.js-ajax-list-content');
			$ajaxContent.ajaxl({
				topPreloader: false,
				url: location.pathname,
				data: {'q': q},
				dataType: 'HTML',
				type: 'POST',
				success: this.proxy(function (data) {
					$ajaxContent.html(data);
				})
			});
        },
		
		'.js-complex-detail-nav click': function(el, e){
			e.preventDefault();
			var target = e.target;
			// цикл двигается вверх от target к родителям до this
			
			while (target != el) {
				if ($(target).is('a.linkblock')) {	
			
					// нашли элемент, который нас интересует!
					el.find('a.linkblock').each(function (i) {
						if($(this).hasClass('selected')){
							$(this).removeClass('selected');
						}
					});
					$(target).addClass('selected');
	
					var classname = target.getAttribute('data-for');
					this.element.find('.block').each(function (i) {
						if(!$(this).hasClass('disabled')){
							$(this).addClass('disabled');
						}
					});
				
					this.element.find('.block.'+classname).removeClass('disabled');
					return;
				}
				else if($(target).is('li.house')){
					el.find('li.house').each(function (i) {	
						$(this).removeClass('active');				
					});
					
					this.element.find('#address_flat').html(target.getAttribute('data-location'))
					if(!$(target).hasClass('active')){
						$(target).addClass('active');
						var id_house = target.getAttribute('data-id');
						this.element.find('#id_house').attr('value', id_house);				
						this.element.trigger("list.contentUpdate", {});
					}
					return;
				}
				target = target.parentNode;
			}
		},
		
		'.js-select-design click': function(el, e){
			e.preventDefault();
			var target = e.target;
			alert(el.data('id'));
		},
		
		'list.contentUpdate': function (el, e, param) {
			var $ajaxContent = this.element.find('.js-ajax-list-content');
			var data = [];

			if (param.page > 0) {
				data.push({
					name: "page",
					value: param.page
				})
			} else {
				data.push({
					name: "page",
					value: $("#page").val()
				})
			}

			if (param.viewCounter > 0) {
				data.push({
					name: "viewCounter",
					value: param.viewCounter
				})
			}

			this.element.find(".js-sort a").each(function(){
				data.push({
					name: "sort[" + $(this).data("type") + "]",
					value: $(this).data("method")
				})
			});

			$.merge(data, this.element.find('.js-filter-form').serializeArray());

			var self = this;
			$ajaxContent.ajaxl({
				topPreloader: true,
				url: location.pathname,
				data: data,
				dataType: 'HTML',
				type: 'POST',
				success: this.proxy(function (data) {
					$ajaxContent.html(data);
					if ($ajaxContent.find('.not-found').length > 0) {
					
					} else {
						
					}
					new APP.Controls.Pagination(this.element.find(".js-pagination"));
					$('.list-plan .plan-item').removeClass('hide').addClass('show');
					APP.Controls.FancyboxLink.initList(this.element.find(".js-fancybox"));
				})
			});
		},
		
    });

})(jQuery, window.APP);