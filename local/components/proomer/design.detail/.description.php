<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
  'NAME' => 'Детальная страница дизайна',
  'DESCRIPTION' => 'Выводит блок детальной страницы дизайна',
  "PATH" => array(
    "ID" => "content",
    //'NAME' => 'Детальная страница дизайна 3',
    "CHILD" => array(
      "ID" => "design_detail",
      "NAME" => 'Cтраницы дизайна'
    )
  ),
  "ICON" => "/images/icon.gif",
);
?>
