<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = Array(
   'PARAMETERS' => array(
      'IBLOCK_ID' => array(
        'PARENT' => 'BASE',
        'NAME' => 'ID блока',
        'TYPE' => 'STRING',
      ),
      'STYLE_HIGHLOAD_ID' => array(
        'PARENT' => 'BASE',
        'NAME' => 'ID Highload стилей',
        'TYPE' => 'STRING',
      ),
      'ROOMS_IBLOCK_ID' => array(
        'PARENT' => 'BASE',
        'NAME' => 'ID блока комнат',
        'TYPE' => 'STRING',
      ),
	  'DESIGN_ROOM_IBLOCK_ID' => array(
        'PARENT' => 'BASE',
        'NAME' => 'ID блока комнат',
        'TYPE' => 'STRING',
      ),
      'ELEMENT_CODE' => array(
        'PARENT' => 'BASE',
        'NAME' => 'ID проекта',
        'TYPE' => 'STRING'
      ),
      'CACHE_TIME'  =>  array('DEFAULT' => 3600),
   )
);

?>
