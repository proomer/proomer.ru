<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (CModule::IncludeModule("catalog"))
	global $USER;

?>

<?
foreach ($arResult['ROOMS'] as $key => $arItem)
{
	$arCatalog = array();
	foreach ($arItem['CATALOG'] as $arCatalogId)
	{
		$res = CIBlockElement::GetByID($arCatalogId);
		if($ar_res = $res->GetNext())
		{
			$dbPrice = CPrice::GetList(
				array("QUANTITY_FROM" => "ASC", "QUANTITY_TO" => "ASC", "SORT" => "ASC"),
				array("PRODUCT_ID" => $ar_res['ID']),
      	false,
      	false,
      	array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
    	);

			$Price=array();

			while ($arPrice = $dbPrice->Fetch())
			{
    		$arDiscounts = CCatalogDiscount::GetDiscountByPrice (
        	$arPrice["ID"],
          $USER->GetUserGroupArray(),
          "N",
          "s1"
        );

    		$discountPrice = CCatalogProduct::CountPriceWithDiscount (
          $arPrice["PRICE"],
          $arPrice["CURRENCY"],
          $arDiscounts
        );

    		$arPrice["DISCOUNT_PRICE"] = $discountPrice;
				$Price[] = $arPrice;
			}

			$preview_picture = CFile::ResizeImageGet(
        $ar_res['PREVIEW_PICTURE'],
        array(
          'width' => 320,
          'height' => 320
        ),
        BX_RESIZE_IMAGE_EXACT
      );

			$arCatalog[] = array (
				'ID' => $ar_res['ID'],
				'NAME' => $ar_res['NAME'],
				'PREVIEW_PICTURE' => $preview_picture['src'],
				'PRICE' => $Price,
			);
		}
	}

	$arResult['ROOMS'][$key]['CATALOG'] = $arCatalog;
}
?>
