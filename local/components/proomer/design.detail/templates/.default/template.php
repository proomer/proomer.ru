<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
//$this->setFrameMode(true);
 ?>
<!-- TODO: убрать инлайн стили -->

<?php $imagesDir =  $this->GetFolder() . "/images/"; ?>

<!-- header -->
<div class="row">
  <div class="col-md-12">
    <div style="height: 550px; width: 100%; position: absolute; background-size: cover; background-repeat: no-repeat; background-position: 50% 50%; opacity: 0.2; background-color: #FFFFFF; background-image: url('<?php echo $arResult['DETAIL_PICTURE']['src'] ?>');"></div>
    <div class="content-container">
      <div class="row" style="position: relative; height: 550px; padding-top: 150px;">
        <div class="col-md-12">
          <?php foreach ($arResult['STYLES'] as $style): ?>
            <div class="design-styles" style="font-family: 'PT Sans';">
              <span>Стиль: </span><strong><?php echo $style['UF_NAME'] ?></strong>
            </div>
          <?php endforeach; ?>
        </div>
        <div class="col-md-12">
          <h1 style="color: #252525; font: 400 36px 'Roboto Slab',sans-serif; margin-bottom: 15px; text-align: left;"><? echo $arResult['NAME'] ?></h1>
        </div>
        <div class="col-md-9">
          <p style="color: #252525; font: 400 15px 'PT Sans',sans-serif; height: 150px;"><? echo $arResult['PREVIEW_TEXT']; ?></p>
        </div>
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-2">
              <a class="btn btn-block blue waves-effect js-add-basket" href="javascript:void(0)" data-element-id="<? echo $arResult['ID']; ?>" >Купить проект</a>
			  <div id="result"></div>

            </div>
            <div class="col-md-2" style="padding-left: 30px;">
              <span style="color: #252525; font: 400 14px 'PT Sans', sans-serif; text-align: left;">Стоимость проекта</span>
              <h1 style="color: #252525; font: 400 35px 'RobotoRegular', sans-serif; text-align: left; margin-top: -8px;">
			    <?php echo $arResult['COST']; ?>
			    <span style="display: inline-block; background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -386px -246px; height: 25px;  width: 17px;"></span>
			  </h1>
            </div>
            <div class="col-md-2" style="padding-left: 30px; margin-top: 10px;">
              <a class="js-fancybox fancyboxLink" href="#info-popup" style="color: #252525; font-family: 'PTSansItalic'; font-size: 14px; line-height: 1.0; text-decoration: none; border-bottom: 1px dotted #252525;">Что я получу после покупки дизайна?</a>
            </div>
            <div class="col-md-6" style="display: table; padding-top: 12px;">
              <div class="pull-right" style="display: table-cell; vertical-align: middle;">
                <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
                <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
                <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end header -->
<!-- author and price --->
<div class="row" style="padding: 60px 0 60px 0; background-color: #fafafa;">
  <div class="col-md-12">
    <div class="content-container">
      <div class="row">
        <div class="col-md-2">
          <img class="img-responsive img-circle" src="<?php echo $arResult['PROPERTY_CREATED_BY']['PERSONAL_PHOTO']['src'] ?>">
        </div>
        <div class="col-md-3" style="padding-left: 30px; height: 200px; display: table;">
          <div style="display: table-cell; vertical-align: middle;">
            <span style="font-size: 14px; color: #878787;">Автор проекта</span><br/>
            <span style="color: #252525; font: 400 25px 'Roboto Slab',sans-serif; text-align: left;"><?php echo $arResult['PROPERTY_CREATED_BY']['NAME'] ?></span>
          </div>
        </div>
        <div class="col-md-7">
          <table class="table">
            <thead>
              <tr>
                <th style="border-bottom: 1px solid #dddddd; font: 700 13px 'Roboto Slab',sans-serif; padding: 0 0 10px;">Площадь (общая <?php echo $arResult['SUM_AREA']; ?> м<sup>2</sup>)</th>
                <th style="border-bottom: 1px solid #dddddd; font: 700 13px 'Roboto Slab',sans-serif; padding: 0 10px 10px 0; text-align: right;">Мебель</th>
                <th style="border-bottom: 1px solid #dddddd; font: 700 13px 'Roboto Slab',sans-serif; padding: 0 10px 10px 0; text-align: right;">Свет</th>
                <th style="border-bottom: 1px solid #dddddd; font: 700 13px 'Roboto Slab',sans-serif; padding: 0 10px 10px 0; text-align: right;">Сантехника</th>
                <th style="border-bottom: 1px solid #dddddd; font: 700 13px 'Roboto Slab',sans-serif; padding: 0 10px 10px 0; text-align: right;">Электроника</th>
                <th style="border-bottom: 1px solid #dddddd; font: 700 13px 'Roboto Slab',sans-serif; padding: 0 10px 10px 0; text-align: right;">Материалы</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($arResult['ROOMS'] as $room): ?>
                <tr>
                  <th scope="row" style="border-top: 0; font: 400 13px 'PT Sans',sans-serif; padding: 12px 40px 0 0;">
                    <a style="text-decoration: underline; color: #077fed; text-decoration: none; border-bottom: 1px dotted #077fed;" href="#room-<?php echo $room['ID'] ?>"><?php echo $room['SECOND_NAME'] ?></a> <span style="float: right;"><?php echo $room['AREA'] ?> м<sup>2</sup></span>
                  </th>
                  <td  style="border-top: 0; font: 400 13px 'PT Sans',sans-serif; padding: 12px 10px 0 0; text-align: right;">
				    <?php echo number_format($room['PRICE_FURNITURE'], 0, '.', ' '); ?>
					<span style="display: inline-block; background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -433px -246px;  height: 10px; width: 8px;"></span>
				  </td>
                  <td  style="border-top: 0; font: 400 13px 'PT Sans',sans-serif; padding: 12px 10px 0 0; text-align: right;">
				    <?php echo number_format($room['PRICE_LIGHT'], 0, '.', ' '); ?>
					<span style="display: inline-block; background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -433px -246px;  height: 10px; width: 8px;"></span>
				  </td>
                  <td  style="border-top: 0; font: 400 13px 'PT Sans',sans-serif; padding: 12px 10px 0 0; text-align: right;">
				    <?php echo number_format($room['PRICE_PLUMBING'], 0, '.', ' '); ?>
					<span style="display: inline-block; background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -433px -246px;  height: 10px; width: 8px;"></span>
				  </td>
                  <td  style="border-top: 0; font: 400 13px 'PT Sans',sans-serif; padding: 12px 10px 0 0; text-align: right;">
				    <?php echo number_format($room['PRICE_ELECTRONICS'], 0, '.', ' '); ?>
					<span style="display: inline-block; background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -433px -246px;  height: 10px; width: 8px;"></span>
				  </td>
                  <td  style="border-top: 0; font: 400 13px 'PT Sans',sans-serif; padding: 12px 10px 0 0; text-align: right;">
				    <?php echo number_format($room['PRICE_MATERIALS'], 0, '.', ' '); ?>
					<span style="display: inline-block; background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -433px -246px;  height: 10px; width: 8px;"></span>
				  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end author and price -->
<?php if (count($arResult['PREVIEW_360']) >= 32): ?>
<!-- hr -->
<div class="row" style="background-color: #fafafa;">
  <div class="col-md-12">
    <div class="content-container">
      <hr/>
    </div>
  </div>
</div>
<!-- end hr -->
<!-- 3d preview 360 -->
<div class="row" style="background-color: #fafafa; padding-top: 60px; padding-bottom: 60px;">
  <div class="col-md-12">
    <div class="content-container">
      <?php
        $preview_360_one = $arResult['PREVIEW_360'][0];
        unset($arResult['PREVIEW_360'][0]);
        $preview_360_json = json_encode($arResult['PREVIEW_360'], JSON_HEX_QUOT);
      ?>
      <div style="width: 800px; margin: 0 auto 0; overflow: visible">
        <div class="preview360-gallery-d" data-json='<?php echo $preview_360_json;?>' style="margin: auto">
          <div class="preview360-background-d"></div>
          <img src="<?php echo $preview_360_one; ?>" class="preview360-d" style="z-index: 11;">
        </div>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
<!-- end 3d preview 360 -->
<!-- rooms -->
<?php foreach ($arResult['ROOMS'] as $room): ?>
  <div class="row" style="background-color: #fafafa;">
    <div class="col-md-12">
      <div class="content-container">
        <hr/>
      </div>
    </div>
  </div>
  <div class="row" id="room-<?php echo $room['ID'] ?>" style="background-color: #fafafa;">
    <div class="col-md-12">
      <div class="content-container">
        <div class="row" style="padding-top: 60px;">
          <div class="col-md-5">
            <span class="pull-right" style="color: #252525; font: 400 25px 'Roboto Slab',sans-serif;"><?php echo $room['NAME']; ?></span>
          </div>
          <div class="col-md-offset-1 col-md-6">
            <p class="pull-left" style="color: #252525; font: 400 14px 'PT Sans', sans-serif; text-align: left;"><?php echo $room['DETAIL_TEXT']; ?></p>
          </div>
          <!-- room images slider -->
          <div class="col-md-12" style="padding-top: 30px;">
            <?php if (count($room['IMAGES']) > 1): ?>
            <div class="row" style="padding: 20px 0 20px 0;">
              <div class="col-md-12">
                <div style="margin: auto; width: 142px;">
                  <div class="bxslider-room-control bxslider-room-control-prev" id="bxslider-room-<?php echo $room['ID'] ?>-prev" style="float: left;"></div>
                  <span id="bxslider-room-<?php echo $room['ID'] ?>-pager" style="float: left; font: 500 15px 'Fira Sans',sans-serif; line-height: 18px; margin: 0 20px 0 20px;"></span>
                  <div class="bxslider-room-control bxslider-room-control-next" id="bxslider-room-<?php echo $room['ID'] ?>-next" style="float: left;"></div>
                </div>
              </div>
            </div>
            <?php endif; ?>
            <div class="row">
              <div class="col-md-12">
                <ul id="bxslider-room-<?php echo $room['ID'] ?>">
                <?php foreach ($room['IMAGES'] as $roomImage): ?>
                  <li><img src="<?php echo $roomImage['src'] ?>"></li>
                <?php endforeach; ?>
                </ul>
              </div>
            </div>
          </div>
          <!-- end room images slider  -->
          <!-- products -->
          <?php if (count($room['CATALOG']) >= 1): $room['CATALOG'] = array_slice($room['CATALOG'], 0, 4); ?>
              <div class="row">
                <?php foreach ($room['CATALOG'] as $roomProductKey => $roomProduct): ?>
                  <div class="col-md-3" style="padding-top: 30px;">
                    <div style="<?php if ($roomProductKey == 0) { ?>margin-right: 10px;<?php } elseif ($roomProductKey == 3) { ?>margin-left: 10px;<?php } else { ?>margin: 0 10px 0 10px;<?php } ?>">
                      <img class="img-responsive" src="<?php echo $roomProduct['PREVIEW_PICTURE']; ?>" style="height: 320px;">
                      <div style="height: 110px; padding: 11px 12px; position: relative; background-color: #FFFFFF; border-left: 1px solid rgba(1, 1, 1, 0.05); border-right: 1px solid rgba(1, 1, 1, 0.05); border-bottom: 1px solid rgba(1, 1, 1, 0.05);">
                        <a style="color: #252525; font: 400 1.5em 'Roboto Slab', sans-serif; text-align: left;" href="#"><?php echo $roomProduct['NAME']; ?></a>
                        <div class="bottom">
    					  <span style="bottom: 15px; color: #252525; font: 400 2.1em 'RobotoRegular', sans-serif; position: absolute; text-align: left;">
                            <?php echo number_format($roomProduct['PRICE'][0]['PRICE'], 0, '.', ' '); ?>
							<span style="display: inline-block; background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -419px -246px;  height: 15px; width: 14px;"></span>
                          </span>
    											<a class="js-add-basket product-btn-basket" href="#">
                            <span class="basket-detail"></span>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endforeach; ?>
              </div>
          <?php endif; ?>
          <!-- end products -->
          <!-- room prices -->
          <div class="col-md-12" style="padding-top: 60px; padding-bottom: 60px;">
            <div class="row" style="">
              <div class="col-md-3" style="display: table;">
                <div style="display: table-cell; vertical-align: middle; text-align: right;">
                  <span class="pull-right" style="color: #252525; font: 400 25px 'Roboto Slab',sans-serif;">Стоимость мебели и материалов</span>
                  <span style="color: #077fed; font: 500 30px 'Fira Sans',sans-serif;">
				    <?php echo number_format($room['COST_ALL'], 0, '.', ' '); ?>
				    <span style="display: inline-block; background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -403px -246px;  height: 21px; width: 15px;"></span>
				  </span>
                </div>
              </div>
              <div class="col-md-offset-1 col-md-3">
                <table class="table">
                  <tbody>
                      <tr>
                        <td style="border-top: 0; font-family: 'PT Sans'; font-size: 14px; color: #878787; padding-top: 0;">Мебель</td>
                        <td style="border-top: 0; font: 400 13px 'PT Sans',sans-serif; padding-top: 0; text-align: right;">
						  <?php echo number_format($room['PRICE_FURNITURE'], 0, '.', ' '); ?>
						  <span style="display: inline-block; background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -433px -246px;  height: 10px; width: 8px;"></span>
						</td>
                      </tr>
                      <tr>
                        <td style="border-top: 0; font-family: 'PT Sans'; font-size: 14px; color: #878787; padding-top: 0;">Свет</td>
                        <td style="border-top: 0; font: 400 13px 'PT Sans',sans-serif; padding-top: 0; text-align: right;">
						  <?php echo number_format($room['PRICE_LIGHT'], 0, '.', ' '); ?>
						  <span style="display: inline-block; background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -433px -246px;  height: 10px; width: 8px;"></span>
						</td>
                      </tr>
                      <tr>
                        <td style="border-top: 0; font-family: 'PT Sans'; font-size: 14px; color: #878787; padding-top: 0;">Сантехника</td>
                        <td style="border-top: 0; font: 400 13px 'PT Sans',sans-serif; padding-top: 0; text-align: right;">
						  <?php echo number_format($room['PRICE_PLUMBING'], 0, '.', ' '); ?>
						  <span style="display: inline-block; background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -433px -246px;  height: 10px; width: 8px;"></span>
						</td>
                      </tr>
                      <tr>
                        <td style="border-top: 0; font-family: 'PT Sans'; font-size: 14px; color: #878787; padding-top: 0; padding-top: 0;">Электроника</td>
                        <td style="border-top: 0; font: 400 13px 'PT Sans',sans-serif; padding-top: 0; text-align: right;">
						  <?php echo number_format($room['PRICE_ELECTRONICS'], 0, '.', ' '); ?>
						  <span style="display: inline-block; background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -433px -246px;  height: 10px; width: 8px;"></span>
						</td>
                      </tr>
                      <tr>
                        <td style="border-top: 0; font-family: 'PT Sans'; font-size: 14px; color: #878787; padding-top: 0;">Материалы</td>
                        <td style="border-top: 0; font: 400 13px 'PT Sans',sans-serif; padding-top: 0; text-align: right;">
						  <?php echo number_format($room['PRICE_MATERIALS'], 0, '.', ' '); ?>
						  <span style="display: inline-block; background: rgba(0, 0, 0, 0) url('/local/images/sprite.png') no-repeat scroll -433px -246px;  height: 10px; width: 8px;"></span>
						</td>
                      </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-md-offset-1 col-md-2" style="display: table; height: 150px">
                <div style="display: table-cell; vertical-align: middle;">
                  <a class="btn btn-block blue waves-effect js-add-basket" href="javascript:void(0)" data-element-id="<?=$room['ID']?>">Купить проект</a> 
                </div>
              </div>
              <div class="col-md-2">
              </div>
            </div>
          </div>
          <!-- end room prices -->
        </div>
      </div>
    </div>
  </div>
<?php endforeach; ?>
<!-- end rooms -->

<!-- scripts -->
<script type="text/javascript">
  jQuery(function(){
    <?php foreach ($arResult['ROOMS'] as $room): ?>

      jQuery('#bxslider-room-<?php echo $room['ID'] ?>').bxSlider({
        pager: true,
        pagerType: 'short',
        pagerSelector: '#bxslider-room-<?php echo $room['ID'] ?>-pager',
        controls: true,
        prevSelector: '#bxslider-room-<?php echo $room['ID'] ?>-prev',
        nextSelector: '#bxslider-room-<?php echo $room['ID'] ?>-next',
        nextText: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
        prevText: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
        mode: 'fade'
      });

    <?php endforeach; ?>
  });
</script>
