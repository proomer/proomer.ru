<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?

$Sum = $paySysParams["SHOULD_PAY"]["VALUE"];
$ShopID = $paySysParams["SHOP_ID"]["VALUE"];
$scid = $paySysParams["SCID"]["VALUE"];
$customerNumber = $paySysParams["ORDER_ID"]["VALUE"];
$orderDate = $paySysParams["ORDER_DATE"]["VALUE"];
$orderNumber = $paySysParams["ORDER_ID"]["VALUE"];
$paymentType = $paySysParams["PAYMENT_VALUE"]["VALUE"];

$Sum = number_format($Sum, 2, ',', '');
?>

<?if(strlen($paySysParams["IS_TEST"]["VALUE"]) > 0):
	?>
	<form name="ShopForm" action="https://demomoney.yandex.ru/eshop.xml" method="post" target="_blank">
<?else:
	?>
	<form name="ShopForm" action="https://money.yandex.ru/eshop.xml" method="post">
<?endif;?>
<font class="tablebodytext">
<input name="shopID" value="<?=$ShopID?>" type="hidden">
<input name="scid" value="<?=$scid?>" type="hidden">
<input name="customerNumber" value="<?=$customerNumber?>" type="hidden">
<input name="orderNumber" value="<?=$this->ID?>" type="hidden">
<input name="sum" value="<?=$Sum?>" type="hidden">
<input name="paymentType" value="<?=$paymentType?>" type="hidden">
<input name="cms_name" value="1C-Bitrix" type="hidden">

<!-- <br /> -->
<!-- Детали заказа:<br /> -->
<!-- <input name="OrderDetails" value="заказ №<?=$orderNumber?> (<?=$orderDate?>)" type="hidden"> -->
<br />
<div class="btn-wrapper">
    <a class="btn blue js-submit-btn waves-effect" href="javascript:void(0);">Оплатить заказ</a>
</div>
</form>
