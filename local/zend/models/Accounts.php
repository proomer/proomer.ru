<?

/**
 * Class Sibirix_Model_Complex
 *
 */
class Sibirix_Model_Accounts extends Sibirix_Model_Bitrix {

    protected $_iblockId = IB_ACCOUNTS;
    protected $_selectFields = array(
        'ID',
        'NAME',
		'PROPERTY_CARD',
		'PROPERTY_NUMBER',
		'PROPERTY_OWNER'
    );
	
	/*=================================================================================*/
	//	����� �� id
	/*=================================================================================*/	
	public function editAccount($fields) {
		$bxElement           = new CIBlockElement();
		if(CModule::IncludeModule("iblock"))
        $fields["IBLOCK_ID"] = IB_ACCOUNTS;

        if ($fields["ID"] > 0) {
            $designId    = $fields["ID"];
            $designProps = $fields["PROPERTY_VALUES"];
            unset($fields["ID"], $fields["PROPERTY_VALUES"]);

            if (!empty($fields["NAME"])) {
                $fields["CODE"] = CUtil::translit($fields["NAME"], "ru");
            }

            $updateResult = $bxElement->Update($designId, $fields);

            if ($updateResult && !empty($designProps)) {

                $bxElement->SetPropertyValuesEx($designId, IB_ACCOUNTS, $designProps);

            }

            $result = $designId;
        } else {
            if (empty($fields["NAME"])) {
                $fields["NAME"] = 'Pattern_Card' . time();
                $fields["CODE"] = CUtil::translit($fields["NAME"], "ru");
            }
            $newId = $bxElement->Add($fields);
			
            $result = $newId;
        }

        return $result;
    }
	
}








