<?

/**
 * Class Sibirix_Model_Complex
 *
 */
class Sibirix_Model_Goods extends Sibirix_Model_Bitrix {

    protected $_iblockId = IB_GOODS;
    protected $_selectFields = array(
        'ID',
        'NAME',
		'CODE',
		'PROPERTY_LENGTH',
		'PROPERTY_WIDTH',
		'PROPERTY_HEIGHT',
		'PROPERTY_MATERIAL',
		'PROPERTY_MADEIN',
		'PROPERTY_PRICE',
		'PROPERTY_ARTICLE',
		'PROPERTY_STATUS',
		'PROPERTY_ID_USER',
		'PROPERTY_IMG',
		'PROPERTY_COLOR',
		'PROPERTY_STYLE',
		'PROPERTY_PREVIEW',
		'PROPERTY_USED_DESIGN',
		'PROPERTY_PLAN_FLAT',
		'IBLOCK_SECTION_ID',
		'PREVIEW_PICTURE',
		'DETAIL_PICTURE',
		'PREVIEW_TEXT',
		'DETAIL_TEXT',
		'DISCOUNT_VALUE',
		'PRINT_DISCOUNT_VALUE',
		'PRINT_VALUE',
		'SHOW_COUNTER'
    );
	
	protected $_selectListFields = [
        'ID',
        'CODE',
        'CREATED_BY',
        'NAME',
        'DETAIL_PICTURE',
        'PREVIEW_TEXT',
        'PROPERTY_BUDGET',
        'PROPERTY_STATUS',
        'PROPERTY_LIKE_CNT'
    ];
	
	/**
     * Перезаписывание актуального количества элементов на странице
    */
    public function reinitViewCounter() {
        $this->_pageSize = Sibirix_Model_ViewCounter::getViewCounter();
    }
	/**
     * Добавление/изменение товара
     * @param $fields
     * @return bool|int
    */
    public function editGoods($fields) {
        $bxElement           = new CIBlockElement();
        $fields["IBLOCK_ID"] = IB_GOODS;
		
        if ($fields["ID"] > 0) {
            $designId    = $fields["ID"];
            $designProps = $fields["PROPERTY_VALUES"];
            unset($fields["ID"], $fields["PROPERTY_VALUES"]);

            if (!empty($fields["NAME"])) {
                $fields["CODE"] = CUtil::translit($fields["NAME"], "ru");
            }

            $updateResult = $bxElement->Update($designId, $fields);

            if ($updateResult && !empty($designProps)) {
                $bxElement->SetPropertyValuesEx($designId, IB_GOODS, $designProps);

                //оповещение администратору
                if (array_key_exists('STATUS', $designProps) && $designProps['STATUS'] == GOODS_STATUS_MODERATION) {
                   // $notificationModel = new Sibirix_Model_Notification();

                    //$design = $this->getElement($designId);
                   // $designer = Sibirix_Model_User::getCurrent();
                    //$notificationModel->statusModeraion($design, $designer);
                }
            }
			$newId = $fields["ID"];
            $result = $updateResult;
        } else {
            $patternEnumId = EnumUtils::getListIdByXmlId($this->_iblockId, "STATUS", "draft");
            if (empty($fields["NAME"])) {
                $fields["NAME"] = PATTERN_PRODUCT_NAME . time();
				$fields["CODE"] = CUtil::translit($fields["NAME"], "ru").time();
                $fields["PROPERTY_VALUES"]["STATUS"] = $patternEnumId;
            }
			else{
				$fields["CODE"] = CUtil::translit($fields["NAME"], "ru");
			}

            $newId = $bxElement->Add($fields);
            $result = $newId;
        }

        if (empty($fields["PRICE_VALUE"])) {
            return $result;
        }
		if (empty($fields["SIZE_VALUE"])) {
			return $result;
		}
		
		$fields["PRICE_VALUE"]["PRODUCT_ID"] = $result;
		
		CCatalogProduct::Add(
			array(
				"ID"     => $fields["PRICE_VALUE"]["PRODUCT_ID"],
				"HEIGHT" => $fields["SIZE_VALUE"]["HEIGHT"],
				"WIDTH" => $fields["SIZE_VALUE"]["WIDTH"],
				"LENGTH" => $fields["SIZE_VALUE"]["LENGTH"],
			),
			true
		);
		
        $bxPrice = new CPrice();
        $res = $bxPrice->GetList(array(), array(
                "PRODUCT_ID"       => $fields["PRICE_VALUE"]["PRODUCT_ID"],
                "CATALOG_GROUP_ID" => $fields["PRICE_VALUE"]["CATALOG_GROUP_ID"],
                "CURRENCY"         => $fields["PRICE_VALUE"]["CURRENCY"]
            ));

        if ($arr = $res->Fetch()) {
            $bxPrice->Update($arr["ID"], $fields["PRICE_VALUE"]);
        } else {
            $bxCatalogProduct = new CCatalogProduct();
            $bxCatalogProduct->Add(["ID" => $fields["PRICE_VALUE"]["PRODUCT_ID"]]);
            $bxPrice->Add($fields["PRICE_VALUE"]);
        }
        return $result;
    }
/*=================================================================================*/
//	Категории
/*=================================================================================*/
	public function getCat($params = []) {
		$categories = $this->getSections($params);
		$categories = $this->getImgItems($categories);
		return $categories;
    }
	
	public function getCatChild($where) {
		$params['where'] = ["=SECTION_ID" => $where];
		return $this->getSections($params);
     
    }
	public function getImgItems($arr_row) {
    	foreach($arr_row as $row){
			if(isset($row->PICTURE)){
				$row->PICTURE = Resizer::resizeImage($row->PICTURE, "SHOP_CATEGORY_ICON");
			}
    		if(isset($row->PREVIEW_PICTURE)){$row->PREVIEW_PICTURE = CFile::GetPath($row->PREVIEW_PICTURE);};
    		if(isset($row->DETAIL_PICTURE)){$row->DETAIL_PICTURE = CFile::GetPath($row->DETAIL_PICTURE);};
    		if(isset($row->PROPERTY_IMG_VALUE)){$row->PROPERTY_IMG_VALUE = CFile::GetPath($row->PROPERTY_IMG_VALUE);};
    	};
		
		return $arr_row;
    }
/*=================================================================================*/
//	Добавляет один товар
/*=================================================================================*/
	public function addGoods($data) {	
		return $this->add($data);
    }	
/*=================================================================================*/
//	Возвращает список товаров
/*=================================================================================*/
	public function getProductList($filter = array(), $sort, $page) {
        $filter['PROPERTY_STATUS'] = GOODS_STATUS_PUBLISHED;
        $itemList = $this->select($this->_selectFields, true)->where($filter)->orderBy($sort, true)->getPage($page);
		$this->_getGoodsInfo($itemList->items);
        return $itemList;
    }
	public function getProductbList($selected, $filter = array(), $sort) {
		if(!$selected){
			$selected = $this->_selectListFields;
		}
        $filter['PROPERTY_STATUS'] = GOODS_STATUS_PUBLISHED;
        $itemList->items = $this->select($selected, true)->where($filter)->orderBy($sort, true)->getElements();
		$this->_getGoodsInfo($itemList->items);
        return $itemList;
    }
/*=================================================================================*/
//	Удаляет товары
/*=================================================================================*/	
	public function delGoods($id, $where) {
		$this->remove($id);
    }
/*=================================================================================*/
/*=================================================================================*/		
	 public function addPlan($id, $imageFile) {
        $imageExist = $this->select(["PROPERTY_PREVIEW"], true)->getElement($id);
        $alreadyImages = array();
		
        foreach ($imageExist->PROPERTY_PREVIEW_VALUE as $key => $image) {
            $alreadyImages[$imageExist->PROPERTY_PREVIEW_PROPERTY_VALUE_ID[$key]] = CIBlock::makeFilePropArray($image);
        }
        $fields["ID"] = $id;
        $fields["PROPERTY_VALUES"] = array(
            "PREVIEW" => $alreadyImages + array("n0" => array("VALUE" => $imageFile))
        );
	
        $element = new CIBlockElement();
		
        $element->SetPropertyValuesEx($id, IB_GOODS, $fields["PROPERTY_VALUES"]);

        $newImage = $this->select(["PROPERTY_PREVIEW"], true)->getElement($id);
		//echo print_r($newImage);
		//exit;
        $resultImages = array();
        foreach ($newImage->PROPERTY_PREVIEW_VALUE as $key => $imageId) {
            $resultImages[] = array(
                "valueId" => $newImage->PROPERTY_PREVIEW_PROPERTY_VALUE_ID[$key],
                "imgSrc" => Resizer::resizeImage($imageId, "DROPZONE_PREVIEW_PHOTO")
            );
        }

        return $resultImages;
    }

    /**
     * Удаляет файл из множетсвенного свойства
     * возвращает список новых файлов
     * @param $imageItemId
     * @return array
     * @throws Exception
     */
    public function deletePlan($designId, $imageItemId) {
        $arFile["MODULE_ID"] = "iblock";
        $arFile["del"] = "Y";

        $element = new CIBlockElement();
        $element->SetPropertyValueCode($designId, "PREVIEW", Array($imageItemId => Array("VALUE" => $arFile)));

        $newImage = $this->select(["PROPERTY_PREVIEW"], true)->getElement($designId);
        $resultImages = array();
        foreach ($newImage->PROPERTY_PREVIEW_VALUE as $key => $imageId) {
            $resultImages[] = array(
                "valueId" => $newImage->PROPERTY_PREVIEW_PROPERTY_VALUE_ID[$key],
                "imgSrc" => Resizer::resizeImage($imageId, "DROPZONE_PREVIEW_PHOTO")
            );
        }

        return $resultImages;
    }
		
	public function _getGoodsInfo($items) {

		$designPriceList = $this->getPrice($items);
		$designSizeList = $this->getSize($items);

        //$this->getImageData($designs);
        $list = [];
        //активные лайки пользователя
        if (Sibirix_Model_User::isAuthorized()) {
            $hh = Highload::instance(HL_LIKES)->cache(0);
            $list = $hh->where(['UF_USER_ID' => Sibirix_Model_User::getId()])->fetch();

            $list = array_map(function ($obj) {
                return $obj['UF_DESIGN'];
            }, $list);
        }

        //корзина
        $basketModel = new Sibirix_Model_Basket();
        $basketItems = $basketModel->getBasketProductsId();
		$favouriteItems = $basketModel->getFavouriteProductsId();
		//
        foreach ($designerGetList as $ind => $designer) {
            $designerList[$designer->ID] = $designer;
        }

        foreach ($items as $key => $designItem) {
            $designItem->PRICES = $designPriceList[$designItem->ID];
			$designItem->SIZE = $designSizeList[$designItem->ID];
            //$designItem->DESIGNER = $designerList[$designItem->CREATED_BY];
            //$designItem->IS_LIKED = (!empty($list)) ? in_array($designItem->ID, $list) : false;
            $designItem->IS_IN_BASKET = (!empty($basketItems))? in_array($designItem->ID, $basketItems) : false;
			$designItem->IS_IN_FAVOURITE = (!empty($favouriteItems))? in_array($designItem->ID, $favouriteItems) : false;
			
			if(CCatalogSKU::IsExistOffers($designItem->ID, IB_GOODS)){
				$designItem->OFFERS = 1;
			}
			else{$designItem->OFFERS = 0;};
			$discount = CCatalogDiscount::GetDiscountByProduct($designItem->ID);

			if(!empty($discount)){
				$designItem->DISCOUNT = $discount[0];
				
				$discount[0]['NEW_PRICE_VALUE'] = $designItem->PRICES['PRICE'] - round($designItem->PRICES['PRICE'] * $designItem->DISCOUNT['VALUE']/100);
				
				$designItem->DISCOUNT = $discount[0];
			}
        }
    }
	
	/**
     * Получает прайс-лист товаров
     * @param $product
     * @return array
     */
    public function getPrice($product) {
        if (empty($product)) return array();
        $priceList = array();

        if (!is_array($product)) {
            $product = [$product];
        }

        $designIds = array();
        foreach ($product as $designItem) {
            if (!is_object($designItem)) continue;
            $designIds[] = $designItem->ID;
        }

        if (empty($designIds)) {
            $designIds = $product;
        }

        $bxPrice = new CPrice();
        $getList = $bxPrice->GetList(array(),array("PRODUCT_ID" => $designIds));

        while ($price = $getList->Fetch()) {
            $priceList[$price["PRODUCT_ID"]] = $price;
        }

        return $priceList;
    }
	
	/**
     * Получает габариты товаров
     * @param $product
     * @return array
    */
    public function getSize($product) {
		if (empty($product)) return array();
        $sizeList = array();

        if (!is_array($product)) {
            $product = [$product];
        }

        $productIds = array();
        foreach ($product as $productItem) {
            if (!is_object($productItem)) continue;
            $productIds[] = $productItem->ID;
        }

        if (empty($productIds)) {
            $productIds = $product;
        }

        $bxPrice = new CPrice();
        $getList = CCatalogProduct::GetList(array(), array("ID" => $productIds), false, false, array('ID', 'PURCHASING_PRICE', 'WIDTH', 'LENGTH', 'HEIGHT'));

        while ($price = $getList->Fetch()) {
            $sizeList[$price["ID"]] = $price;
        }

        return $sizeList;
    }
	
	/**
     * Формирование фильтрующего массива
     * @param $getParams
     * @return array
    */
    public function prepareFilter($getParams) {
        if (empty($getParams)) return array();

        $filterKeys = array(
            "price",
            "primaryColor",
            "style",
			"madeIn",
            "status",
			"categoryId"
        );

        foreach ($getParams as $paramKey => $param) {
            if (!in_array($paramKey, $filterKeys)) {
                unset($getParams[$paramKey]);
            }
        }

        if (empty($getParams)) return array();

        $filterArray = array();

        //По цене дизайна
        if (!empty($getParams["price"]) && $getParams["price"] != '0:0') {
            $avgPriceArray = explode(":", $getParams["price"]);
            $filterArray[">=CATALOG_PRICE_" . BASE_PRICE] = $avgPriceArray[0];
            $filterArray["<=CATALOG_PRICE_" . BASE_PRICE] = $avgPriceArray[1];

        }

        //По средней бюджета
        /*if (!empty($getParams["budget"])) {
            $avgPriceArray = explode(":", $getParams["budget"]);
            if ($avgPriceArray[0] == 0) {
                $filterArray[] = array(
                    "LOGIC" => "OR",
                    [">=PROPERTY_BUDGET" => $avgPriceArray[0], "<=PROPERTY_BUDGET" => $avgPriceArray[1]],
                    ["=PROPERTY_BUDGET"  => false],
                );
            } else {
                $filterArray[">=PROPERTY_BUDGET"] = $avgPriceArray[0];
                $filterArray["<=PROPERTY_BUDGET"] = $avgPriceArray[1];
            }
        }*/
		
		 //По категории
        if (!empty($getParams["categoryId"])) {	
			if(is_numeric($getParams["categoryId"])){
				$filterArray["=SECTION_ID"] = $getParams["categoryId"];
			} 
			else{
				$filterArray["=SECTION_CODE"] = $getParams["categoryId"];
			}
        }
	
		
        //По цвету
        if (!empty($getParams["primaryColor"])) {
            $filterArray["=PROPERTY_COLOR"] = $getParams["primaryColor"];
        }
		
		//По производителю
        if (!empty($getParams["madeIn"])) {
            $filterArray["=PROPERTY_MADEIN"] = $getParams["madeIn"];
        }

        //По стилю
        if (!empty($getParams["style"])) {
            $filterArray["=PROPERTY_STYLE"] = $getParams["style"];
        }

        //статус
		$filterArray["=PROPERTY_STATUS"] = GOODS_STATUS_PUBLISHED;
        /*if (!empty($getParams["status"])) {
            $status = $getParams["status"];
            $key = array_search(0, $status);

            if ($key !== false) {
                $filterArray["CATALOG_PRICE_" . BASE_PRICE] = 0;
                unset($status[$key]);
            }

            if (!empty($status)) {
                $filterArray["=PROPERTY_STATUS"] = $getParams["status"];
            }
        }*/

        //По дополнительным параметрам из сервиса поиска дизайна
        //Дизайны привязаны к объектам через планировки квартир. Всё сводится к выбору нужных планировок квартир
        $houseModel    = new Sibirix_Model_House();
        $entranceModel = new Sibirix_Model_Entrance();
        $floorModel    = new Sibirix_Model_Floor();
        $flatModel     = new Sibirix_Model_Flat();

        $setSearchServiceFilter = false;
        if (!empty($getParams["flat"])) {
            //Если задана квартира
            $flatList = $flatModel->select(["PROPERTY_PLAN"], true)->where(["ID" => $getParams["flat"]])->getElements();
            $setSearchServiceFilter = true;
        } elseif(!empty($getParams["floor"])) {
            //Если задан этаж
            $flatList = $flatModel->select(["PROPERTY_PLAN"], true)->where(["PROPERTY_FLOOR" => $getParams["floor"]])->getElements();
            $setSearchServiceFilter = true;
        } elseif(!empty($getParams["entrance"])) {
            //Если задан подъезд
            $floorList = $floorModel->orderBy([], true)->select(["ID"], true)->where(["PROPERTY_ENTRANCE" => $getParams["entrance"]])->getElements();
            $idList    = array_map(function($obj){return $obj->ID;}, $floorList);

            if (!empty($idList)) {
                $flatList = $flatModel->select(["PROPERTY_PLAN"], true)->where(["PROPERTY_FLOOR" => $idList])->getElements();
            }
            $setSearchServiceFilter = true;
        } elseif(!empty($getParams["house"])) {
            //Если задан дом
            $entranceList = $entranceModel->orderBy([], true)->select(["ID"], true)->where(["PROPERTY_HOUSE" => $getParams["house"]])->getElements();
            $idList    = array_map(function($obj){return $obj->ID;}, $entranceList);

            if (!empty($idList)) {
                $floorList = $floorModel->orderBy([], true)->select(["ID"], true)->where(["PROPERTY_ENTRANCE" => $idList])->getElements();
                $idList    = array_map(function($obj){return $obj->ID;}, $floorList);

                if (!empty($idList)) {
                    $flatList = $flatModel->select(["PROPERTY_PLAN"], true)->where(["PROPERTY_FLOOR" => $idList])->getElements();
                }
            }
            $setSearchServiceFilter = true;
        } elseif(!empty($getParams["complexId"])) {
            //Если задан комплекс
            $houseList = $houseModel->orderBy([], true)->select(["ID"], true)->where(["PROPERTY_COMPLEX" => $getParams["complexId"]])->getElements();
            $idList    = array_map(function($obj){return $obj->ID;}, $houseList);

            if (!empty($idList)) {
                $entranceList = $entranceModel->orderBy([], true)->select(["ID"], true)->where(["PROPERTY_HOUSE" => $idList])->getElements();
                $idList    = array_map(function($obj){return $obj->ID;}, $entranceList);

                if (!empty($idList)) {
                    $floorList = $floorModel->orderBy([], true)->select(["ID"], true)->where(["PROPERTY_ENTRANCE" => $idList])->getElements();
                    $idList    = array_map(function($obj){return $obj->ID;}, $floorList);
                    if (!empty($idList)) {
                        $flatList = $flatModel->select(["PROPERTY_PLAN"], true)->where(["PROPERTY_FLOOR" => $idList])->getElements();
                    }
                }
            }
            $setSearchServiceFilter = true;
        }

        if (!empty($flatList)) {
            $filterArray["PROPERTY_PLAN"] = array_map(function ($obj) {return $obj->PROPERTY_PLAN_VALUE;}, $flatList);
        } elseif($setSearchServiceFilter) {
            $filterArray["PROPERTY_PLAN"] = 0;
        }

        return $filterArray;
    }
	
}








