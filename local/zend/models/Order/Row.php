<?

/**
 * Class Sibirix_Model_Order_Row
 *
 */
class Sibirix_Model_Order_Row extends Sibirix_Model_Bitrix_Row {

    protected static $paySystemRef;

    /**
     *
     */

    public function includePaySystem() {
        $bxPaySysAction = new CSalePaySystemAction();

        $cSalePaymentAction = $bxPaySysAction->GetById(PAY_SYSTEM_ID);
        $paySysParams = unserialize($cSalePaymentAction['PARAMS']);

        $paySystemActionData = [
			"SHOP_ID" => $paySysParams["YANDEX_SHOP_ID"]["VALUE"],
			"SCID" => $paySysParams["YANDEX_SCID"]["VALUE"],
			"CUSTOMER_NUMBER" => $paySysParams["ORDER_ID"]["VALUE"],
			"ORDER_DATE" => $paySysParams["ORDER_DATE"]["VALUE"],
			"ORDER_NUMBER" => $this->ID,
            "SHOULD_PAY" => $this->PRICE,
			"PAYMENT_TYPE" => $paySysParams["PAYMENT_VALUE"]["VALUE"],
            "OrderDescr" => $this->ID,
            "CURRENCY" => $this->CURRENCY,
            "DATE_INSERT" => $this->DATE_INSERT,
            "EMAIL_USER" => $this->PROPS['EMAIL'],
			"IS_TEST" => $paySysParams["PS_IS_TEST"]["VALUE"],
			"YANDEX_SHOP_KEY" => $paySysParams["YANDEX_SHOP_KEY"]["VALUE"]
        ];

        include(P_DR . '/bitrix/php_interface/include/sale_payment/yandex_3x' . '/payment.php');
		
		
        return $this;
    }
}