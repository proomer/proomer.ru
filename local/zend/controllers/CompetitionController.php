<?

/**
 * Class DesignController
 */
class CompetitionController extends Sibirix_Controller {
    /**
     * @var Sibirix_Model_Design
     */
    protected $_model;

    public function init() {
        /**
         * @var $ajaxContext Sibirix_Controller_Action_Helper_SibirixAjaxContext
         */
      //  $ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
      //  $ajaxContext->addActionContext('design-list', 'html')
       //     ->initContext();

       // $this->_model = new Sibirix_Model_Design();
    }

    public function listAction(){
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'competition-list');
		Zend_Registry::get('BX_APPLICATION')->SetTitle(("Все проекты"));
		
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
        $ajaxContext->addActionContext('list', 'html')
            ->initContext();
		
		$projectModel 		= new Sibirix_Model_Project();
		$competitionModel 	= new Sibirix_Model_Competition();
		$userModel			= new Sibirix_Model_User();
		$apartModel    		 = new Sibirix_Model_ProjectApart();
        $roomModel     		 = new Sibirix_Model_ProjectRoom();
		$planoptionModel     = new Sibirix_Model_PlanOption();
		$planModel     		 = new Sibirix_Model_Plan();
		$flatModel     		 = new Sibirix_Model_Flat();
		$floorModel     	 = new Sibirix_Model_Floor();
		$entranceModel     	 = new Sibirix_Model_Entrance();
		$houseModel     	 = new Sibirix_Model_House();
		$complexModel     	 = new Sibirix_Model_Complex();
		
		$filterParams = $this->getAllParams();
		
		$filter = [
			">=CATALOG_PRICE_1" => $filterParams["priceMin"],
			"<=CATALOG_PRICE_1" => $filterParams["priceMax"]    
        ];
		
		$competitionList = $apartModel->getProjectApartbList(['ID', 'NAME', 'PREVIEW_TEXT', 'SHOW_COUNTER', 'PROPERTY_TIME', 'PROPERTY_ID_ORDER',
		'PROPERTY_DESIGN_LIKED', 'PROPERTY_OFFERS'],$filter);
		$competitionList2 = $roomModel->getProjectRoombList(['ID', 'NAME', 'PREVIEW_TEXT', 'SHOW_COUNTER', 'PROPERTY_TIME', 'PROPERTY_ID_ORDER',
		'PROPERTY_DESIGN_LIKED', 'PROPERTY_OFFERS'],$filter);

		$result->items = array_merge($competitionList->items, $competitionList2->items);
		
		foreach($result->items as $item){
			$item->PRICE_VALUE = CPrice::GetBasePrice($item->ID);
	
			$project = $projectModel->select(['ID', 'PROPERTY_ID_OPTION_PLAN'], true)->getElement($item->PROPERTY_ID_ORDER);
			$planOption = $planoptionModel->select(['ID', 'PROPERTY_PLAN_FLAT', 'PROPERTY_IMAGES'], true)->getElement($project->PROPERTY_ID_OPTION_PLAN);
			$item->IMG = $planOption->PROPERTY_IMAGES;
			if (!empty($item->IMG)) {
				$image = Resizer::resizeImage($item->IMG, 'PLAN_LIST');
			} else {
				$image = '/local/images/proomer2.png';
			}
			$item->IMG = $image;
		}

		$this->view->assign([
			"result"	=> $result,
			//"itemList"  => $result->items,
		//	'paginator' => EHelper::getPaginator($result->pageData->totalItemsCount, $result->pageData->current, $result->pageData->size),
		]);
	}
	
	public function editCompetition($id) {
		$designId = $this->getParam('designId');
		$competitionId = $this->getParam('competition');
	//	$offers = $this->select(['PROPERTY_OFFERS'], true)->getElement()
	}
	
	public function removeOfferAction() {
	}
	
	public function detailAction() {

		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('detail', 'html')
		->initContext();
		
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'competition-detail');
        Zend_Registry::get('BX_APPLICATION')->SetTitle("Готовые дизайны для новостроек");
		
		$element = (int) $this->getParam('element');
		$_SESSION['competition'] = $element;
		$designId = $this->getParam('designId');
		$type_query = $this->getParam('type_query');
		
		if(!$element){
			Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
		}

		$projectModel 		= new Sibirix_Model_Project();
		$competitionModel 	= new Sibirix_Model_Competition();
		$userModel			= new Sibirix_Model_User();
				
		$apartModel    		 = new Sibirix_Model_ProjectApart();
        $roomModel     		 = new Sibirix_Model_ProjectRoom();
		$planoptionModel     = new Sibirix_Model_PlanOption();
		$planModel     		 = new Sibirix_Model_Plan();
		$flatModel     		 = new Sibirix_Model_Flat();
		$floorModel     	 = new Sibirix_Model_Floor();
		$designModel         = new Sibirix_Model_Design();
		$entranceModel     	 = new Sibirix_Model_Entrance();
		$houseModel     	 = new Sibirix_Model_House();
		$complexModel     	 = new Sibirix_Model_Complex();
		$familyModel         = new Sibirix_Model_ProjectFamily();

		$myDesignList = EHelper::getMyDesignList($element);

		if($type_query == 'delete'){
					$fields = array();
					$iblock = IB_ORDER_PROJECT_APARTMENT;
					$model = $apartModel;
					$competition = $apartModel->select(['PROPERTY_OFFERS'],true)->getElement($element);
					if(!$competition){
						$iblock = IB_ORDER_PROJECT_ROOM;
						$model = $roomModel;
						$competition = $roomModel->select(['PROPERTY_OFFERS'],true)->getElement($element);
					}
					$offers = $competition->PROPERTY_OFFERS_VALUE;
					
					if(in_array($designId, $offers)){			
						$key = array_search($designId, $offers);
						$fields = array();
						$fields["ID"] = $element;
						if ($key !== false){
							unset($offers[$key]);
						}
				
						if(count($offers) > 0){
							$fields["PROPERTY_VALUES"]["OFFERS"] = $offers;
						}
						else{
							$fields["PROPERTY_VALUES"]["OFFERS"] = '';
						};
						
						foreach($myDesignList->items as $design){
							if($design->ID == $designId){									
								if($iblock == IB_ORDER_PROJECT_ROOM){
									$result = $roomModel->editRoomProject($fields);
								}
								else if($iblock == IB_ORDER_PROJECT_APARTMENT){
									$result = $apartModel->editApartProject($fields);
								}
							}
						}	

						if($offers){
							$designList = $designModel->getDesignList(['=ID' => $offers]);
				
							foreach($designList->items as $design){
								if (!$design->PROPERTY_IMG_3_VALUE) {
									$design->MAIN_IMAGE = Resizer::resizeImage($design->DETAIL_PICTURE, 'PREVIEW_360_SMALL');
								} else {
									$period = 32/8;
									$gallery3d = array();
									for ($i=1; $i < 8+1; $i++) {
										$cur_index = $i * $period;
										$gallery3d[] = Resizer::resizeImage($design->PROPERTY_IMG_3_VALUE[$cur_index], 'PREVIEW_360_SMALL');
									}
									$design->GALLERY3D = $gallery3d;
								}
								$personal_photo_resize = CFile::ResizeImageGet($design->DESIGNER->PERSONAL_PHOTO['ID'], array('width'=>47, 'height'=>47), BX_RESIZE_IMAGE_EXACT, true);
									
								$personal_photo = $personal_photo_resize ? $personal_photo_resize['src'] : '/local/images/no-avatar-small.png'; // фото дизайнера

								$design->PERSONAL_PHOTO = $personal_photo;										
							};
							
						}
						else{
							$designList = array();
						}
						$this->view->assign([
							"designList"	=> $designList,
							"myDesignList"  => $myDesignList
						]);
					}
		}
		else if($type_query == 'update'){

			$fields = array();
			$iblock = IB_ORDER_PROJECT_APARTMENT;
			$model = $apartModel;
			$competition = $apartModel->select(['PROPERTY_OFFERS'],true)->getElement($element);
			if(!$competition){
				$iblock = IB_ORDER_PROJECT_ROOM;
				$model = $roomModel;
				$competition = $roomModel->select(['PROPERTY_OFFERS'],true)->getElement($element);
			}
			$offers = $competition->PROPERTY_OFFERS_VALUE;
	
			if(!in_array($designId, $offers)){
				array_push($offers, $designId);
				$fields = array();
				$fields["ID"] = $element;
				$fields["PROPERTY_VALUES"]["OFFERS"] = $offers;
				if($iblock == IB_ORDER_PROJECT_ROOM){
					$result = $roomModel->editRoomProject($fields);
				}
				else if($iblock == IB_ORDER_PROJECT_APARTMENT){
					$result = $apartModel->editApartProject($fields);
				}
			}; 
			
			$designList = $designModel->getDesignList(['=ID' => $offers]);
			foreach($designList->items as $design){
				if (!$design->PROPERTY_IMG_3_VALUE) {
					$design->MAIN_IMAGE = Resizer::resizeImage($design->DETAIL_PICTURE, 'PREVIEW_360_SMALL');
				} else {
					$period = 32/8;
					$gallery3d = array();
					for ($i=1; $i < 8+1; $i++) {
						$cur_index = $i * $period;
						$gallery3d[] = Resizer::resizeImage($design->PROPERTY_IMG_3_VALUE[$cur_index], 'PREVIEW_360_SMALL');
					}
					$design->GALLERY3D = $gallery3d;
				}
				$personal_photo_resize = CFile::ResizeImageGet($design->DESIGNER->PERSONAL_PHOTO['ID'], array('width'=>47, 'height'=>47), BX_RESIZE_IMAGE_EXACT, true);
					
				$personal_photo = $personal_photo_resize ? $personal_photo_resize['src'] : '/local/images/no-avatar-small.png'; // фото дизайнера

				$design->PERSONAL_PHOTO = $personal_photo;				
			};	

			$this->view->assign([
				"designList"	=> $designList,
				"myDesignList"  => $myDesignList
			]);
		}
		else{
			$hlanimalReference 	= Highload::instance(HL_ANIMAL);
			$animalHighloadList  = $hlanimalReference->select(array_merge(["ID"], array("UF_NAME", "UF_XML_ID")), true)->fetch();
			
			$hlageReference 	= Highload::instance(HL_AGEPEOPLE);
			$ageHighloadList  = $hlageReference->select(array_merge(["ID"], array("UF_NAME", "UF_XML_ID")), true)->fetch();
			
			$filter = [
				"=ID" => $element
			];
							
			//Конкурс			
			$competition = $apartModel->select(['ID', 'NAME', 'IBLOCK_ID', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'CREATED_BY', 'PROPERTY_SUGGEST', 'PROPERTY_TIME', 'PROPERTY_ID_ORDER', 'PROPERTY_DESIGN_LIKED', 'PROPERTY_PEOPLE', 'PROPERTY_PEOPLE_CHILDREN', 'PROPERTY_ANIMAL', 'PROPERTY_OFFERS'], true)->getElement($element);
	
			if(!$competition){
				$competition = $roomModel->select(['ID', 'NAME', 'IBLOCK_ID', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'CREATED_BY', 'PROPERTY_SUGGEST', 'PROPERTY_TIME', 'PROPERTY_ID_ORDER', 'PROPERTY_DESIGN_LIKED', 'PROPERTY_PEOPLE', 'PROPERTY_PEOPLE_CHILDREN', 'PROPERTY_ANIMAL', 'PROPERTY_OFFERS'], true)->getElement($element);
			};
			if(!$competition){
				Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
				throw new Zend_Exception('Not found', 404);
			}
			
			Zend_Registry::get('BX_APPLICATION')->SetTitle($competition->NAME);	
			
			$peopleIds = array_map(function ($obj) {
				return $obj;
				}, $competition->PROPERTY_PEOPLE_VALUE);
			$peopleIds = array_unique($peopleIds);
			
			$peopleChildIds = array_map(function ($obj) {
				return $obj;
				}, $competition->PROPERTY_PEOPLE_CHILDREN_VALUE);
			$peopleChildIds = array_unique($peopleChildIds);
			
			$animalIds = array_map(function ($obj) {
				return $obj;
				}, $competition->PROPERTY_ANIMAL_VALUE);
			$animalIds = array_unique($animalIds);
			
			if($peopleIds){$competition->PEOPLE = $familyModel->select([
			'ID',
			'CODE',
			'NAME',
			'PROPERTY_PEOPLE_AGE',
			'PROPERTY_PEOPLE_GENDER',
			'PROPERTY_ID_ORDER',
			'PROPERTY_TYPE',
			'PROPERTY_ANIMAL',
			'PROPERTY_PEOPLE_AGE'
		],true)->where(['=ID' => $peopleIds])->getElements();}else{$competition->PEOPLE_VALUE = array();};
			if($peopleChildIds){$competition->PEOPLE_CHILDREN = $familyModel->select([
			'ID',
			'CODE',
			'NAME',
			'PROPERTY_PEOPLE_AGE',
			'PROPERTY_PEOPLE_GENDER',
			'PROPERTY_ID_ORDER',
			'PROPERTY_TYPE',
			'PROPERTY_ANIMAL',
			'PROPERTY_PEOPLE_AGE'
		],true)->where(['=ID' => $peopleChildIds])->getElements();}else{$competition->PEOPLE_CHILDREN = array();};
			if($animalIds){$competition->ANIMAL = $familyModel->select([
			'ID',
			'CODE',
			'NAME',
			'PROPERTY_PEOPLE_AGE',
			'PROPERTY_PEOPLE_GENDER',
			'PROPERTY_ID_ORDER',
			'PROPERTY_TYPE',
			'PROPERTY_ANIMAL',
			'PROPERTY_PEOPLE_AGE'
		],true)->where(['=ID' => $animalIds])->getElements();}else{$competition->ANIMAL = array();};
				
			for($n = 0; $n < count($competition->ANIMAL); $n++){
				for($i = 0; $i < count($animalHighloadList); $i++){
					if($animalHighloadList[$i]['UF_XML_ID'] == $competition->ANIMAL[$n]->PROPERTY_ANIMAL_VALUE){
						//array_push($new_item_animal, $animalHighloadList[$i]['UF_NAME']);
						$competition->ANIMAL[$n]->PROPERTY_ANIMAL_VALUE = $animalHighloadList[$i]['UF_NAME'];
					};
				};
			};
			
			for($n = 0; $n < count($competition->PEOPLE); $n++){
				for($i = 0; $i < count($ageHighloadList); $i++){
					if($ageHighloadList[$i]['UF_XML_ID'] == $competition->PEOPLE[$n]->PROPERTY_PEOPLE_AGE_VALUE){
						//array_push($new_item_animal, $animalHighloadList[$i]['UF_NAME']);
						$competition->PEOPLE[$n]->PROPERTY_PEOPLE_AGE_VALUE = $ageHighloadList[$i]['UF_NAME'];
					};
				};
			};
			
			for($n = 0; $n < count($competition->PEOPLE_CHILDREN); $n++){
				for($i = 0; $i < count($ageHighloadList); $i++){
					if($ageHighloadList[$i]['UF_XML_ID'] == $competition->PEOPLE_CHILDREN[$n]->PROPERTY_PEOPLE_AGE_VALUE){
						//array_push($new_item_animal, $animalHighloadList[$i]['UF_NAME']);
						$competition->PEOPLE_CHILDREN[$n]->PROPERTY_PEOPLE_AGE_VALUE = $ageHighloadList[$i]['UF_NAME'];
					};
				};
			};
			
			$user = $userModel->findById($competition->CREATED_BY);
		
			$competition->CREATED_BY = $user;
			//Получим базовую цену
			$competition->PRICE_VALUE = CPrice::GetBasePrice($competition->ID);

			$project = $projectModel->select(['ID', 'PROPERTY_ID_OPTION_PLAN'], true)->getElement($competition->PROPERTY_ID_ORDER);
			$planOption = $planoptionModel->select(['ID', 'PROPERTY_PLAN_FLAT', 'PROPERTY_IMAGES'], true)->getElement($project->PROPERTY_ID_OPTION_PLAN);

			if (!empty($planOption->PROPERTY_IMAGES_VALUE)) {
				$image = Resizer::resizeImage($planOption->PROPERTY_IMAGES_VALUE, 'PLAN_LIST');
			} else {
				$image = '/local/images/proomer2.png';
			}
			$planOption->PROPERTY_IMAGES_VALUE = $image;

			$plan = $planModel->select(['ID', 'PROPERTY_AREA', 'PROPERTY_ROOM', 'PROPERTY_IMAGES', 'PROPERTY_DOCUMENTS'], true)->getElement($planOption->PROPERTY_PLAN_FLAT);

			if (!empty($plan->PROPERTY_IMAGES_VALUE)) {
				$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'PLAN_LIST');
			} else {
				$image = '/local/images/proomer2.png';
			}
			$plan->PROPERTY_IMAGES_VALUE = $image;

			$plan->PROPERTY_DOCUMENTS_VALUE = CFile::GetPath($plan->PROPERTY_DOCUMENTS_VALUE[0]);

			$flat = $flatModel->select(['ID', 'PROPERTY_FLOOR'], true)->where(['=PROPERTY_PLAN' => $plan->ID])->getElement();
			$floor = $floorModel->select(['ID', 'PROPERTY_ENTRANCE'], true)->where($flat->PROPERTY_FLOOR)->getElement();
			$entrance = $entranceModel->select(['ID', 'PROPERTY_HOUSE'], true)->getElement($floor->PROPERTY_ENTRANCE);
			$house = $houseModel->select(['ID', 'NAME', 'PROPERTY_COMPLEX', 'PROPERTY_STREET', 'PROPERTY_HOUSE_NUMBER'], true)->getElement($entrance->PROPERTY_HOUSE);
		
			$complex = $complexModel->select(['ID', 'NAME'], true)->getElement($house->PROPERTY_COMPLEX_VALUE);
		
			$designIds = array_map(function ($obj) {
					return $obj;
					}, $competition->PROPERTY_OFFERS_VALUE);
			$designIds = array_unique($designIds);
	
			if($designIds){
				$designList = $designModel->getDesignList(['=ID' => $designIds]);
			
				if(Sibirix_Model_User::isAuthorized()){
					//отсортируем по CREATED_BY		
					function cmp($a, $b) 
					{		
						if(!$a->CREATED_BY == Sibirix_Model_User::getId()){
							return $a;
						}
					}
					usort($designList->items, "cmp");
				};
				$i = 0;
							
				foreach($designList->items as $design){
					if (!$design->PROPERTY_IMG_3_VALUE){
						$design->MAIN_IMAGE = Resizer::resizeImage($design->DETAIL_PICTURE, 'PREVIEW_360_SMALL');
					} else {
						$period = 32/8;
						$gallery3d = array();
						for ($i=1; $i < 8+1; $i++) {
							$cur_index = $i * $period;
							$gallery3d[] = Resizer::resizeImage($design->PROPERTY_IMG_3_VALUE[$cur_index], 'PREVIEW_360_SMALL');
						}
						$design->GALLERY3D = $gallery3d;
					}
					
					$personal_photo_resize = CFile::ResizeImageGet($design->DESIGNER->PERSONAL_PHOTO['ID'], array('width'=>47, 'height'=>47), BX_RESIZE_IMAGE_EXACT, true);
						
					$personal_photo = $personal_photo_resize ? $personal_photo_resize['src'] : '/local/images/no-avatar-small.png'; // фото дизайнера

					$design->PERSONAL_PHOTO = $personal_photo;
						
				};
				unset($design);	
			}
			else{
				$design = '';
			};

			if(CModule::IncludeModule("iblock"))
			CIBlockElement::CounterInc($competition->ID);
			$this->view->assign([
				"competition"	=> $competition,
				"complex"		=> $complex,
				"house"			=> $house,
				"plan"			=> $plan,
				"designList"	=> $designList,
				"myDesignList"  => $myDesignList,
				"planOption"	=> $planOption,
				"element"		=> $element
			]);
		}
    }
}