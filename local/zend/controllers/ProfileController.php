<?

/**
 * Class ProfileController
 */
class ProfileController extends Sibirix_Controller {

    /**
     * @var $user Sibirix_Model_User_Row
     */
    protected $user;

    public function preDispatch() {
	
        /**
         * @var $ajaxContext Sibirix_Controller_Action_Helper_SibirixAjaxContext
         */
        $ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
        $ajaxContext->addActionContext('design', 'html')
            ->initContext();

        $this->user = Sibirix_Model_User::getCurrent();

        if (!$this->user) {
            $this->redirect('/');
        }
    }

    public function indexAction() {

        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'profile-inner');
        Zend_Registry::get('BX_APPLICATION')->SetTitle('Профиль');
		
        $this->view->user = $this->user;

    }

    public function designerUpdateAction() {
        //$form = new Sibirix_Form_ProfileDesigner();

        if (!(Sibirix_Model_User::isAuthorized())) {
            $this->_helper->json(['success' => false, 'errorFields' =>  ['NAME' => 'Пользователь не авторизован']]);
        }

       // if ($form->isValid($this->getAllParams())) {
            $model = new Sibirix_Model_User();
            $name = $this->getParam('name');
            $lastName = $this->getParam('last_name');
            $phone = $this->getParam('phone');
            $email = $this->getParam('email');
            $portfolio = $this->getParam('portfolio');
            $about = $this->getParam('about');
            $styles = $this->getParam('styles');
			$city = $this->getParam('city');
			$personal_country = $this->getParam('personal_country');
			$kompany = $this->getParam('kompany');
			$address = $this->getParam('address');

            $result = $model->updateDesignerProfile($this->user, $name, $lastName, $phone, $email, $portfolio, $about, $styles, $kompany, $address, $city, $personal_country);

            if ($result === true) {
                $this->_helper->json(['success' => true]);
            } else {
                foreach ($result as $ind => $error) {
                    $form->getElement($ind)->addErrors($result);
                }
                $form->markAsError();
                $form->getFieldsErrors();
                $this->_helper->json(['success' => false, 'errorFields' =>  $form->formErrors]);
            }
       // } else {
        //    $form->getFieldsErrors();
       // }

        $this->_helper->json(['success' => false, 'errorFields' =>  $form->formErrors]);
    }
	
	public function providerUpdateAction() {
        //$form = new Sibirix_Form_ProfileDesigner();

        if (!(Sibirix_Model_User::isAuthorized())) {
            $this->_helper->json(['success' => false, 'errorFields' =>  ['NAME' => 'Пользователь не авторизован']]);
        }

       // if ($form->isValid($this->getAllParams())) {
            $model = new Sibirix_Model_User();
            $name = $this->getParam('name');
            $lastName = $this->getParam('last_name');
            $phone = $this->getParam('phone');
			$www = $this->getParam('www');
            $email = $this->getParam('email');
            $portfolio = $this->getParam('portfolio');
            $about = $this->getParam('about');
            $sections = $this->getParam('sections');
			$city = $this->getParam('city');
			$country = $this->getParam('personal_country');
			$kompany = $this->getParam('kompany');
			$address = $this->getParam('address');

            $result = $model->updateProviderProfile($this->user, $phone, $www, $sections, $kompany, $address, $city, $country);

            if ($result === true) {
				
                $this->_helper->json(['success' => true]);
            } else {
                foreach ($result as $ind => $error) {
                    $form->getElement($ind)->addErrors($result);
                }
				
               // $form->markAsError();
               // $form->getFieldsErrors();
                $this->_helper->json(['success' => false, 'errorFields' =>  $form->formErrors]);
				
            }
       // } else {
        //    $form->getFieldsErrors();
       // }

        $this->_helper->json(['success' => false, 'errorFields' =>  $form->formErrors]);
    }
	
	public function clientUpdateAction() {
        $form = new Sibirix_Form_ProfileClient();

        $userId = $this->getParam('userId');
        if (!(Sibirix_Model_User::getId() == $userId)) {
            $this->_helper->json(['success' => false, 'errorFields' =>  ['NAME' => $userId]]);
        }

        if ($form->isValid($this->getAllParams())) {
            $model = new Sibirix_Model_User();
            $name = $form->getValue('name');
            $lastName = $form->getValue('lastName');
            $phone = $form->getValue('phone');
            $email = $form->getValue('email');

            $result = $model->updateClientProfile($this->user, $name, $lastName, $phone, $email);
            if ($result === true) {
                $this->_helper->json(['success' => true]);
            } else {
                foreach ($result as $ind => $error) {
                    $form->getElement($ind)->addErrors($result);
                }
                $form->markAsError();
                $form->getFieldsErrors();
                $this->_helper->json(['success' => false, 'errorFields' =>  $form->formErrors]);
            }
        } else {
            $form->getFieldsErrors();
        }

        $this->_helper->json(['success' => false, 'errorFields' =>  $form->formErrors]);
    }

    public function changePasswordAction() {
        $form = new Sibirix_Form_ProfileChangePassword();

        $userId = $this->getParam('userId');
        if (!Sibirix_Model_User::isAuthorized() || !(Sibirix_Model_User::getId() == $userId)) {
            $this->_helper->json(['success' => false, 'errorFields' =>  ['NAME' => $userId]]);
        }

        if ($form->isValid($this->getAllParams())) {
            $model = new Sibirix_Model_User();
            $password = $form->getValue('password');
            $newPassword = $form->getValue('newPassword');
            $newPasswordConfirm = $form->getValue('newPasswordConfirm');

            $result = $model->changeProfilePassword($this->user, $password, $newPassword, $newPasswordConfirm);
            if ($result === true) {
                $this->_helper->json(['success' => true]);
            } else {
                foreach ($result as $ind => $error) {
                    $form->getElement($ind)->addErrors($result);
                }
                $form->markAsError();
                $form->getFieldsErrors();
                $this->_helper->json(['success' => false, 'errorFields' =>  $form->formErrors]);
            }
        } else {
            $form->getFieldsErrors();
        }

        $this->_helper->json(['success' => false, 'errorFields' =>  $form->formErrors]);
    }

    public function uploadPhotoAction() {
        $upload = new Zend_File_Transfer();
        $fileInfo = $upload->getFileInfo()['avatar'];
        $fileModel = new Sibirix_Model_Files();

        if (!$fileModel->checkFile($upload->getFileInfo()["avatar"], "jpg,png,jpeg")) {
            $this->_helper->json(['success' => false, 'messageTitle' => Settings::getOption('formatErrorMessage'), 'messageText' => Settings::getOptionText('formatErrorMessage')]);
        }

        if ($fileInfo["size"] > MAX_FILE_SIZE) {
            $this->_helper->json(['success' => false, 'messageTitle' => Settings::getOption('sizeErrorMessage'), 'messageText' => Settings::getOptionText('sizeErrorMessage')]);
        }

        $userModel = new Sibirix_Model_User();
        $result = $userModel->updatePhoto($this->user, $fileInfo);

        $newPhoto = $userModel->select(['PERSONAL_PHOTO'], false)->getElement($this->user->ID);

        $avatar = Resizer::resizeImage($newPhoto->PERSONAL_PHOTO, "PROFILE_AVATAR");
        $headerAvatar = Resizer::resizeImage($newPhoto->PERSONAL_PHOTO, "HEADER_AVATAR");

        if ($result === true) {
            $this->_helper->json(['success' => true, 'avatar' => $avatar, 'headerAvatar' => $headerAvatar]);
        }

        $this->_helper->json(['success' => true, 'file' => $fileInfo]);
    }
	
	public function uploadBackgroundAction() {
        $upload = new Zend_File_Transfer();
        $fileInfo = $upload->getFileInfo()['avatar'];
        $fileModel = new Sibirix_Model_Files();

        if (!$fileModel->checkFile($upload->getFileInfo()["avatar"], "jpg,png,jpeg")) {
            $this->_helper->json(['success' => false, 'messageTitle' => Settings::getOption('formatErrorMessage'), 'messageText' => Settings::getOptionText('formatErrorMessage')]);
        }

        if ($fileInfo["size"] > MAX_FILE_SIZE) {
            $this->_helper->json(['success' => false, 'messageTitle' => Settings::getOption('sizeErrorMessage'), 'messageText' => Settings::getOptionText('sizeErrorMessage')]);
        }

        $userModel = new Sibirix_Model_User();
        $result = $userModel->updateBackground($this->user, $fileInfo);

        $newPhoto = $userModel->select(['UF_IMAGE_BACKGROUND'], false)->getElement($this->user->ID);

        $avatar = Resizer::resizeImage($newPhoto->UF_IMAGE_BACKGROUND, "PROFILE_BACKGROUND_USER");

        if ($result === true) {
            $this->_helper->json(['success' => true, 'avatar' => $avatar, 'headerAvatar' => $headerAvatar]);
        }

        $this->_helper->json(['success' => true, 'file' => $fileInfo]);
    }
	
	public function uploadLogoAction() {
        $upload = new Zend_File_Transfer();
        $fileInfo = $upload->getFileInfo()['avatar'];
        $fileModel = new Sibirix_Model_Files();

        if (!$fileModel->checkFile($upload->getFileInfo()["avatar"], "jpg,png,jpeg")) {
            $this->_helper->json(['success' => false, 'messageTitle' => Settings::getOption('formatErrorMessage'), 'messageText' => Settings::getOptionText('formatErrorMessage')]);
        }

        if ($fileInfo["size"] > MAX_FILE_SIZE) {
            $this->_helper->json(['success' => false, 'messageTitle' => Settings::getOption('sizeErrorMessage'), 'messageText' => Settings::getOptionText('sizeErrorMessage')]);
        }

        $userModel = new Sibirix_Model_User();
        $result = $userModel->updateLogo($this->user, $fileInfo);

        $newPhoto = $userModel->select(['UF_WORK_LOGO'], false)->getElement($this->user->ID);
	
        $avatar = Resizer::resizeImage($newPhoto->UF_WORK_LOGO, "PROFILE_WORK_LOGO");

        if ($result === true) {
            $this->_helper->json(['success' => true, 'avatar' => $avatar, 'headerAvatar' => $headerAvatar]);
        }

        $this->_helper->json(['success' => true, 'file' => $fileInfo]);
    }

    public function removePhotoAction() {
        if (!check_bitrix_sessid()) {
            $this->_helper->json(['success' => false]);
        }

        $userModel = new Sibirix_Model_User();
        $result = false;
        if (!empty($this->user->PERSONAL_PHOTO)) {
            $result = $userModel->updatePhoto($this->user, ['del' => 'Y']);
        }

        $avatar = Resizer::resizeImage(Settings::getOptionFile('defaultAvatar'), "PROFILE_AVATAR");
        $headerAvatar = Resizer::resizeImage(Settings::getOptionFile('defaultAvatar'), "HEADER_AVATAR");
        if ($result === true) {
            $this->_helper->json(['success' => true, 'avatar' => $avatar, 'headerAvatar' => $headerAvatar]);
        }

        $this->_helper->json(['success' => false, 'message' => 'Файл не был удален']);
    }
	
	public function providerAction() {
		
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'profile-inner-page');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'provider-edit');

        if (!Sibirix_Model_User::isAuthorized() || Sibirix_Model_User::getCurrent()->getType() != PROVIDER_TYPE_ID) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }

        $designId = $this->getParam("designId", 0);
        Zend_Registry::get('BX_APPLICATION')->SetTitle(($designId > 0?"Редактирование проекта" : "Добавление проекта"));

        $planName = "";
        if ($designId > 0) {
            $designData = $this->_model->getElement($designId);
            if ($designData->PROPERTY_STATUS_ENUM_ID ==  EnumUtils::getListIdByXmlId(IB_DESIGN, "STATUS", "moderation")) {
                LocalRedirect($this->view->url([],"profile-provider"));
            }

            $planModel = new Sibirix_Model_Plan();
            $planName = $planModel->getElement($designData->PROPERTY_PLAN_VALUE)->NAME;

            if (!$this->_model->checkDesignAccess($designId, $designData->CREATED_BY)) {
                Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
                throw new Zend_Exception('Not found', 404);
            }
        }

        /**
         * Шаг 1
         */
        //Выбор по планеровке
       // $planForm = new Sibirix_Form_SearchService_StepPlan();

        //Выбор по квартире
        //$complexForm = $this->view->action("add-index", "search-service");

        /**
         * Шаг 2
         */
        $step2Form = new Sibirix_Form_AddProviderStep2();

        $roomModel = new Sibirix_Model_Room();
        if (!empty($designData)) {

            //Получаем цену
            $price = $designData->getPrice();

            //Получаем комнаты
            $roomList = $roomModel->cache(0)->where(["PROPERTY_DESIGN" => $designId])->getElements();
            $roomValue = array();

            if (!empty($roomList)) {
                foreach ($roomList as $room) {
                    $roomValue[] = array(
                        "ID"      => $room->ID,
                        "NAME"    => $room->NAME,
                        "AREA"    => $room->PROPERTY_AREA_VALUE,
                        "HAS_IMG" => !empty($room->PROPERTY_IMAGES_VALUE),
                    );
                }
            }

            $step2FormData = array(
                "designName"        => $designData->getName(),
                "shortDescription"  => $designData->PREVIEW_TEXT,
                "designDescription" => $designData->DETAIL_TEXT,
				"anonsPhoto"         => !empty($designData->PREVIEW_PICTURE) ? Resizer::resizeImage($designData->PREVIEW_PICTURE, "DROPZONE_ANONS_PHOTO") : "",
                "mainPhoto"         => !empty($designData->DETAIL_PICTURE) ? Resizer::resizeImage($designData->DETAIL_PICTURE, "DROPZONE_MAIN_PHOTO") : "",
				"category"          => $designData->PROPERTY_CATEGORY_VALUE,
                "style"             => $designData->PROPERTY_STYLE_VALUE,
                "color"             => $designData->PROPERTY_PRIMARY_COLOR_VALUE,
                "designPrice"       => ($price > 0 ? $price : ""),
               // "designFree"        => ($price > 0 ? false : true),
                "square"            => $designData->PROPERTY_AREA_VALUE,
                "roomList"          => $roomValue
            );

            $step2Form->populate($step2FormData);
        }

        /**
         * Шаг 3
         */
       // $step3Form = $this->view->action("room-form", "room", false, ["designId" => $designId]);
		//$step3Form = new Sibirix_Form_AddProviderStep3();
        /**
         * Шаг 4
         */
        $step4Form = new Sibirix_Form_AddProviderStep4();
        if (!empty($designData) && !empty($designData->PROPERTY_DOCUMENTS_VALUE)) {
            $fileData = EHelper::getFileData($designData->PROPERTY_DOCUMENTS_VALUE);

            $step4FormData["docs"][$designData->PROPERTY_DOCUMENTS_VALUE] = array(
                "fileType" => GetFileExtension($fileData["FILE_NAME"]),
                "fileName" => $fileData["ORIGINAL_NAME"],
                "fileSize" => round($fileData["FILE_SIZE"]/1024) . " Кб",
            );

            $step4Form->populate($step4FormData);
        }

        if (!empty($designData)) {
            $this->view->designData = $designData;
        }
		$model = new Sibirix_Model_Goods;
		$row_goods = $model -> getGoods($this->user->ID);
		$row_goods = $model -> getImgItems($row_goods['ITEMS']);
		
        $this->view->planName    = "Дизайн для планировки: " . $planName;
      //  $this->view->planForm    = $planForm;
        $this->view->step2Form   = $step2Form;
        $this->view->step3Form   = $step3Form;
        $this->view->step4Form   = $step4Form;
		$this->view->row_goods = $row_goods;
        //$this->view->complexForm = $complexForm;
    }
	
	public function editAction() {
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'profile-inner-page');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'provider-edit');

        if (!Sibirix_Model_User::isAuthorized() || Sibirix_Model_User::getCurrent()->getType() != PROVIDER_TYPE_ID) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }

        $designId = $this->getParam("designId", 0);
        Zend_Registry::get('BX_APPLICATION')->SetTitle(($designId > 0?"Редактирование проекта" : "Добавление проекта"));

        $planName = "";
        if ($designId > 0) {
            $designData = $this->_model->getElement($designId);
            if ($designData->PROPERTY_STATUS_ENUM_ID ==  EnumUtils::getListIdByXmlId(IB_DESIGN, "STATUS", "moderation")) {
                LocalRedirect($this->view->url([],"profile-provider"));
            }

            $planModel = new Sibirix_Model_Plan();
            $planName = $planModel->getElement($designData->PROPERTY_PLAN_VALUE)->NAME;

            if (!$this->_model->checkDesignAccess($designId, $designData->CREATED_BY)) {
                Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
                throw new Zend_Exception('Not found', 404);
            }
        }

        /**
         * Шаг 1
         */
        //Выбор по планеровке
       // $planForm = new Sibirix_Form_SearchService_StepPlan();

        //Выбор по квартире
        //$complexForm = $this->view->action("add-index", "search-service");

        /**
         * Шаг 2
         */
        $step2Form = new Sibirix_Form_AddProviderStep2();

        $roomModel = new Sibirix_Model_Room();
        if (!empty($designData)) {
            //Получаем цену
            $price = $designData->getPrice();

            //Получаем комнаты
            $roomList = $roomModel->cache(0)->where(["PROPERTY_DESIGN" => $designId])->getElements();
            $roomValue = array();

            if (!empty($roomList)) {
                foreach ($roomList as $room) {
                    $roomValue[] = array(
                        "ID"      => $room->ID,
                        "NAME"    => $room->NAME,
                        "AREA"    => $room->PROPERTY_AREA_VALUE,
                        "HAS_IMG" => !empty($room->PROPERTY_IMAGES_VALUE),
                    );
                }
            }

            $step2FormData = array(
                "designName"        => $designData->getName(),
                "shortDescription"  => $designData->PREVIEW_TEXT,
                "designDescription" => $designData->DETAIL_TEXT,
				"anonsPhoto"         => !empty($designData->PREVIEW_PICTURE) ? Resizer::resizeImage($designData->PREVIEW_PICTURE, "DROPZONE_ANONS_PHOTO") : "",
                "mainPhoto"         => !empty($designData->DETAIL_PICTURE) ? Resizer::resizeImage($designData->DETAIL_PICTURE, "DROPZONE_MAIN_PHOTO") : "",
				"category"          => $designData->PROPERTY_CATEGORY_VALUE,
                "style"             => $designData->PROPERTY_STYLE_VALUE,
                "color"             => $designData->PROPERTY_PRIMARY_COLOR_VALUE,
                "designPrice"       => ($price > 0 ? $price : ""),
               // "designFree"        => ($price > 0 ? false : true),
                "square"            => $designData->PROPERTY_AREA_VALUE,
                "roomList"          => $roomValue
            );

            $step2Form->populate($step2FormData);
        }

        /**
         * Шаг 3
         */
       // $step3Form = $this->view->action("room-form", "room", false, ["designId" => $designId]);
		//$step3Form = new Sibirix_Form_AddProviderStep3();
        /**
         * Шаг 4
         */
        $step4Form = new Sibirix_Form_AddProviderStep4();
        if (!empty($designData) && !empty($designData->PROPERTY_DOCUMENTS_VALUE)) {
            $fileData = EHelper::getFileData($designData->PROPERTY_DOCUMENTS_VALUE);

            $step4FormData["docs"][$designData->PROPERTY_DOCUMENTS_VALUE] = array(
                "fileType" => GetFileExtension($fileData["FILE_NAME"]),
                "fileName" => $fileData["ORIGINAL_NAME"],
                "fileSize" => round($fileData["FILE_SIZE"]/1024) . " Кб",
            );

            $step4Form->populate($step4FormData);
        }

        if (!empty($designData)) {
            $this->view->designData = $designData;
        }
		$model = new Sibirix_Model_Goods;
		$row_goods = $model -> getGoods($this->user->ID);
		$row_goods = $model -> getImgItems($row_goods['ITEMS']);
		
        $this->view->planName    = "Дизайн для планировки: " . $planName;
      //  $this->view->planForm    = $planForm;
        $this->view->step2Form   = $step2Form;
        $this->view->step3Form   = $step3Form;
        $this->view->step4Form   = $step4Form;
		$this->view->row_goods = $row_goods;
		
        //$this->view->complexForm = $complexForm;
    }
	
	public function unfinishedAction() {
		if (!$this->user->getType() == DESIGNER_TYPE_ID) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }
		
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'profile-inner-page');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'profile-design');
        Zend_Registry::get('BX_APPLICATION')->SetTitle('Мои проекты');
		
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('unfinished', 'html')
		->initContext();
		
		
        $designModel = new Sibirix_Model_Design();

        if ($this->getParam("viewCounter") > 0) {
            Sibirix_Model_ViewCounter::setProfileViewCounter($this->getParam("viewCounter"));
            $designModel->reinitProfileViewCounter();
        } else {
            $designModel->reinitProfileViewCounter();
        }

        $sortData = $this->getParam("sort");

        if (!empty($sortData)) {
            if (!empty($sortData["popular"])) {
                $catalogSort["PROPERTY_LIKE_CNT"] = $sortData["popular"];
            }
            if (!empty($sortData["date"])) {
                $catalogSort["DATE_CREATE"] = $sortData["date"];
            }
            if (!empty($sortData["price"])) {
                $catalogSort["catalog_PRICE_" . BASE_PRICE] = $sortData["price"];
            }
            if (!empty($sortData["budget"])) {
                $catalogSort["PROPERTY_BUDGET"] = $sortData["budget"];
            }
        }
        $catalogSort["SORT"] = "ASC";

        $filter = new Sibirix_Form_FilterProfileDesign();
        $filterParams = $this->getAllParams();

        $filter->populate($filterParams);
        $validFilterValues = $filter->getValues();
        $catalogFilter = $designModel->prepareFilter($validFilterValues);
        $catalogFilter["CREATED_BY"] = $this->user->ID;
        $catalogFilter["PROPERTY_STATUS"] = DESIGN_STATUS_DRAFT;

        $result = $designModel->getDesignList($catalogFilter, $catalogSort, $this->getParam("page", 1), true);
		//$result = $designModel->getDesignList([], $catalogSort, $this->getParam("page", 1), true);
        $status = EnumUtils::getPropertyValuesList(IB_DESIGN, 'STATUS');
		
		foreach($result->items as $design){
			if (!$design->PROPERTY_IMG_3_VALUE) {
				$design->MAIN_IMAGE = Resizer::resizeImage($design->DETAIL_PICTURE, 'PREVIEW_360_SMALL');
			} else {
				$period = 32/8;
				$gallery3d = array();
				for ($i=1; $i < 8+1; $i++) {
					$cur_index = $i * $period;
					$gallery3d[] = Resizer::resizeImage($design->PROPERTY_IMG_3_VALUE[$cur_index], 'PREVIEW_360_SMALL');
				}
				$design->GALLERY3D = $gallery3d;
			}						
		};
		
        $this->view->assign([
            "filter"    => $filter,
            "itemList"  => $result->items,
            'paginator' => EHelper::getPaginator($result->pageData->totalItemsCount, $result->pageData->current, $result->pageData->size),
            'status' => $status,
        ]);
	}
	
    public function designAction() {
        if ($this->user->getType() != DESIGNER_TYPE_ID) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }

        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'profile-inner-page');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'profile-design');
        Zend_Registry::get('BX_APPLICATION')->SetTitle('Мои проекты');
        $designModel = new Sibirix_Model_Design();

        if ($this->getParam("viewCounter") > 0) {
            Sibirix_Model_ViewCounter::setProfileViewCounter($this->getParam("viewCounter"));
            $designModel->reinitProfileViewCounter();
        } else {
            $designModel->reinitProfileViewCounter();
        }

        $sortData = $this->getParam("sort");

        if (!empty($sortData)) {
            if (!empty($sortData["popular"])) {
                $catalogSort["PROPERTY_LIKE_CNT"] = $sortData["popular"];
            }
            if (!empty($sortData["date"])) {
                $catalogSort["DATE_CREATE"] = $sortData["date"];
            }
            if (!empty($sortData["price"])) {
                $catalogSort["catalog_PRICE_" . BASE_PRICE] = $sortData["price"];
            }
            if (!empty($sortData["budget"])) {
                $catalogSort["PROPERTY_BUDGET"] = $sortData["budget"];
            }
        }
        $catalogSort["SORT"] = "ASC";

        $filter = new Sibirix_Form_FilterProfileDesign();
        $filterParams = $this->getAllParams();

        if ($filterParams["priceMin"] === null || $filterParams["priceMax"] === null) {
            $filterParams["price"] = $designModel->getExtremePrice($this->user->ID);
        } else {
            $filterParams["price"] = array(
                $filterParams["priceMin"],
                $filterParams["priceMax"]
            );
        }

        $filter->populate($filterParams);
        $validFilterValues = $filter->getValues();
        $catalogFilter = $designModel->prepareFilter($validFilterValues);
        $catalogFilter["PROPERTY_CREATED_BY"] = $this->user->ID;
        $catalogFilter["!PROPERTY_STATUS"] = DESIGN_STATUS_DELETED;
		$catalogFilter["!PROPERTY_STATUS"] = DESIGN_STATUS_DRAFT;

        $result = $designModel->getDesignList($catalogFilter, $catalogSort, $this->getParam("page", 1), true);
		//$result = $designModel->getDesignList([], $catalogSort, $this->getParam("page", 1), true);
        $status = EnumUtils::getPropertyValuesList(IB_DESIGN, 'STATUS');
		
		foreach($result->items as $design){
			if (!$design->PROPERTY_IMG_3_VALUE) {
				$design->MAIN_IMAGE = Resizer::resizeImage($design->DETAIL_PICTURE, 'PREVIEW_360_SMALL');
			} else {
				$period = 32/8;
				$gallery3d = array();
				for ($i=1; $i < 8+1; $i++) {
					$cur_index = $i * $period;
					$gallery3d[] = Resizer::resizeImage($design->PROPERTY_IMG_3_VALUE[$cur_index], 'PREVIEW_360_SMALL');
				}
				$design->GALLERY3D = $gallery3d;
			}						
		};
		
        $this->view->assign([
            "filter"    => $filter,
            "itemList"  => $result->items,
            'paginator' => EHelper::getPaginator($result->pageData->totalItemsCount, $result->pageData->current, $result->pageData->size),
            'status' => $status,
        ]);
    }
	
	public function marketplaceAction() {		
		//if (!$this->user->getType() == PROVIDER_TYPE_ID) {
        //    Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
        //    throw new Zend_Exception('Not found', 404);
        //}
		
		//$user = Sibirix_Model_User::getCurrent();
		
		$user = CUser::GetById($this->user->ID)->fetch();

		$hlSetionsReference 	= Highload::instance(HL_CATEGORY);
		
        $SetionsHighloadList  = $hlSetionsReference->select(array_merge(["ID"], array("UF_NAME", "UF_XML_ID")), true)->fetch();
		
		$arr_section = array();
		foreach($user['UF_SECTIONS_GOODS'] as $section){
			
			foreach($SetionsHighloadList as $hl_section){
				
				if($hl_section['ID'] == $section){
					array_push($arr_section, $hl_section['UF_NAME']);
				} 
			}
			
		}
		$user['UF_SECTIONS_GOODS'] = $arr_section;
	
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'profile-inner-page');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'profile-provider');
        Zend_Registry::get('BX_APPLICATION')->SetTitle('Маркетплейс');
		
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
        $ajaxContext->addActionContext('marketplace', 'html')
            ->initContext();
		
		$model_goods = new Sibirix_Model_Goods();
		
		/*==========================================================================
			//Категории
		==========================================================================*/	
		$params['select'] = ['ID', 'CODE', 'IBLOCK_SECTION_ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PICTURE', 'PICTURE'];
		$dbCatList = $model_goods->getCat($params);

		$categories = $dbCatList;
		
		/*==========================================================================
			//Сортировка, фильтры, лимиты
		==========================================================================*/
		//лимит
		if ($this->getParam("viewCounter") > 0) {
			Sibirix_Model_ViewCounter::setViewCounter($this->getParam("viewCounter"));
			$model_goods->reinitViewCounter();
		}
		else{
			Sibirix_Model_ViewCounter::setViewCounter(20);
			$model_goods->reinitViewCounter();		
		}
		
		//сортировка
		$sortData = $this->getParam("sort");
		
		if (!empty($sortData)) {
			if (!empty($sortData["popular"])) {
				$catalogSort["PROPERTY_LIKE_CNT"] = $sortData["popular"];
			}
			if (!empty($sortData["name"])) {
				$catalogSort["NAME"] = $sortData["name"];
			}
			if (!empty($sortData["date"])) {
				$catalogSort["DATE_CREATE"] = $sortData["date"];
			}
			if (!empty($sortData["price"])) {
				$catalogSort["catalog_PRICE_" . BASE_PRICE] = $sortData["price"];
			}
			if (!empty($sortData["budget"])) {
				$catalogSort["PROPERTY_BUDGET"] = $sortData["budget"];
			}		
		}
			
		$catalogSort["SHOW_COUNTER"] = "DESC";
				
		$user['UF_WORK_LOGO'] = Resizer::resizeImage($user['UF_WORK_LOGO'], 'PROFILE_WORK_LOGO');
		
		$filter = array(
			'CREATED_BY' => $user['ID']
		);
		$select = array('ID', 
						'NAME', 
						'CODE',
						'CREATED_BY',
						'IBLOCK_SECTION_ID',
						'DETAIL_PICTURE', 
						'PREVIEW_PICTURE', 
						'PROPERTY_PREVIEW', 'PROPERTY_STATUS', 'PROPERTY_COLOR', 'PROPERTY_MADEIN', 
						'PROPERTY_MATERIAL', 'PROPERTY_STYLE', 'PROPERTY_PREVIEW', 'PROPERTY_USED_DESIGN'
					);
		$productList = $model_goods->getProductbList($select, $filter, $catalogSort);
		
		foreach($productList->items as $item){
			if (!empty($item)) {
				$image = Resizer::resizeImage($item->PREVIEW_PICTURE, "SHOP_PREVIEW_GOODS_PICTURE");
			} else {
				$image = Resizer::resizeImage(Settings::getOptionFile('defaultDesignImage'), 'SHOP_PREVIEW_GOODS_PICTURE');
			}
			$item->PREVIEW_PICTURE = $image;
			
			$cat = CIBlockSection::GetByID($item->IBLOCK_SECTION_ID);
			$ar_res = $cat->GetNext();
			if($ar_res['CODE']){
				$catId = $ar_res['CODE'];
			}
			else{
				$catId = $ar_res['ID'];
			}
			
			$item->CATEGORY = $catId;
			
			//CATEGORY
			foreach($categories as $cat){
				if($item->CATEGORY == $cat->ID || $item->CATEGORY == $cat->CODE){
					$item->CATEGORY = $cat;
					break;
				}
			}
			
		};

		$this->view->assign([
            "user"	=> $user,
			"itemList" => $productList->items
        ]);
	}
	
	public function appAction(){
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'profile-inner-page');
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'profile-provider');
		Zend_Registry::get('BX_APPLICATION')->SetTitle('Заявки');
		
		if (!CModule::IncludeModule("sale")){return;};
		
		$user = CUser::GetById($this->user->ID)->fetch();

		$hlSetionsReference = Highload::instance(HL_CATEGORY);
		
        $SetionsHighloadList  = $hlSetionsReference->select(array_merge(["ID"], array("UF_NAME", "UF_XML_ID")), true)->fetch();
		
		$arr_section = array();
		foreach($user['UF_SECTIONS_GOODS'] as $section){
			foreach($SetionsHighloadList as $hl_section){				
				if($hl_section['ID'] == $section){
					array_push($arr_section, $hl_section['UF_NAME']);
				} 
			}
		}
		$user['UF_SECTIONS_GOODS'] = $arr_section;
	
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
        $ajaxContext->addActionContext('marketplace', 'html')
            ->initContext();
		
		$model_goods = new Sibirix_Model_Goods();
		
		/*==========================================================================
			//Категории
		==========================================================================*/	
		$params['select'] = ['ID', 'CODE', 'IBLOCK_SECTION_ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PICTURE', 'PICTURE'];
		$dbCatList = $model_goods->getCat($params);

		$categories = $dbCatList;
		
		/*==========================================================================
			//Сортировка, фильтры, лимиты
		==========================================================================*/
		//лимит
		if ($this->getParam("viewCounter") > 0) {
			Sibirix_Model_ViewCounter::setViewCounter($this->getParam("viewCounter"));
			$model_goods->reinitViewCounter();
		}
		else{
			Sibirix_Model_ViewCounter::setViewCounter(20);
			$model_goods->reinitViewCounter();		
		}
		
		$GoodsList = $model_goods->select(['ID'],true)->where(['=CREATED_BY' => 1],true)->getElements();
		
		$goodsIds = array_map(function ($obj) {
			return $obj->ID;
		}, $GoodsList);
		
		//получить заказ с определенным товаром
		$res = CSaleBasket::GetList(array(), array("PRODUCT_ID" => $goodsIds), false, false, 
																							array('
																									ID',
																									'NAME',
																									'ORDER_ID',
																									'PRODUCT_ID',
																									'USER_ID',
																									'PRICE',
																									'BASE_PRICE',
																									'DATE_INSERT',
																									'FUSER_ID'
																							)
																						);
																						
		$arrSale = array();
		$newArrSale = array();
		while ($arItem = $res->Fetch()) {
			$rsUser = CUser::GetByID($arItem['USER_ID']);
			$arUser = $rsUser->Fetch();
			$arItem['AUTHOR'] = $arUser;
			$arItem['AUTHOR']['AVATAR'] = Resizer::resizeImage($arItem['AUTHOR']['ID'], 'PROFILE_APP_AVATAR');
			array_push($arrSale, $arItem);			
		}

		//дуплет
		$arrSale99 = $arrSale;

		//форматируем массив
		foreach($arrSale99 as $sale){
			$newArrSale[$sale['ORDER_ID']]['ORDER_ID'] = $sale['ORDER_ID'];
			$newArrSale[$sale['ORDER_ID']]['USER_ID'] = $sale['USER_ID'];
			$newArrSale[$sale['ORDER_ID']]['FUSER_ID'] = $sale['FUSER_ID'];
			$newArrSale[$sale['ORDER_ID']]['AUTHOR'] = $sale['AUTHOR'];	
			$newArrSale[$sale['ORDER_ID']]['DATE_INSERT'] = $sale['DATE_INSERT']->format('d.m.Y');
			$newArrSale[$sale['ORDER_ID']]['PRODUCT'][$sale['PRODUCT_ID']]['ID'] = $sale['PRODUCT_ID'];
			$newArrSale[$sale['ORDER_ID']]['PRODUCT'][$sale['PRODUCT_ID']]['NAME'] = $sale['NAME'];
			$newArrSale[$sale['ORDER_ID']]['PRODUCT'][$sale['PRODUCT_ID']]['PRICE'] = $sale['PRICE'];
			$product = $model_goods->select(['ID', 'PREVIEW_PICTURE'], true)->getElement($newArrSale[$sale['ORDER_ID']]['PRODUCT'][$sale['PRODUCT_ID']]['ID']);	
			$image = '';
			if ($product->PREVIEW_PICTURE) {
				$image = Resizer::resizeImage($product->PREVIEW_PICTURE, 'SHOP_PRODUCT_ICON');
			} else {
				$image = '/local/images/img-not-found.jpg';
			}
			$newArrSale[$sale['ORDER_ID']]['PRODUCT'][$sale['PRODUCT_ID']]['ICON'] = $image;
						
			foreach($arrSale99 as $key_sale_t => $val_sale_t){
				if($sale_t['ORDER_ID'] == $sale['ORDER_ID']){
					$newArrSale[$val_sale_t['ORDER_ID']]['PRODUCT'][$val_sale_t['PRODUCT_ID']]['ID'] = $val_sale_t['PRODUCT_ID'];
					$newArrSale[$val_sale_t['ORDER_ID']]['PRODUCT'][$val_sale_t['PRODUCT_ID']]['NAME'] = $val_sale_t['NAME'];
					$newArrSale[$val_sale_t['ORDER_ID']]['PRODUCT'][$val_sale_t['PRODUCT_ID']]['PRICE'] = $val_sale_t['PRICE'];
					$product_t = $model_goods->select(['ID', 'PREVIEW_PICTURE'], true)->getElement($newArrSale[$val_sale_t['ORDER_ID']]['PRODUCT'][$val_sale_t['PRODUCT_ID']]['ID']);	
					if ($product_t->PREVIEW_PICTURE) {
						$image_t = Resizer::resizeImage($product_t->PREVIEW_PICTURE, 'SHOP_PRODUCT_ICON');
					} else {
						$image_t = '/local/images/img-not-found.jpg';
					}
					$newArrSale[$val_sale_t['ORDER_ID']]['PRODUCT'][$val_sale_t['PRODUCT_ID']]['ICON'] = $image_t;
					unset($arrSale99[$key_sale_t]);
				}
			}			
		}
	
		//сортировка
		$sortData = $this->getParam("sort");
		
		if (!empty($sortData)) {
			if (!empty($sortData["popular"])) {
				$catalogSort["PROPERTY_LIKE_CNT"] = $sortData["popular"];
			}
			if (!empty($sortData["name"])) {
				$catalogSort["NAME"] = $sortData["name"];
			}
			if (!empty($sortData["date"])) {
				$catalogSort["DATE_CREATE"] = $sortData["date"];
			}
			if (!empty($sortData["price"])) {
				$catalogSort["catalog_PRICE_" . BASE_PRICE] = $sortData["price"];
			}
			if (!empty($sortData["budget"])) {
				$catalogSort["PROPERTY_BUDGET"] = $sortData["budget"];
			}		
		}
			
		$catalogSort["SHOW_COUNTER"] = "DESC";
		
		$user['UF_WORK_LOGO'] = Resizer::resizeImage($user['UF_WORK_LOGO'], 'PROFILE_WORK_LOGO');

		$this->view->assign([
            "user"	=> $user,
			"arrSale" => $newArrSale
        ]);
	}
	
	public function editGoodsAction(){

		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'profile-inner-page');
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'profile-provider');
		
		$model_goods = new Sibirix_Model_Goods();
		$modelCatalog = new Sibirix_Model_Catalog();
		
		$elementCode = $this->getParam('elementCode');
		
		Zend_Registry::get('BX_APPLICATION')->SetTitle(($elementCode?"Редактирование товара" : "Добавление товара"));
		
		$params = $this->getAllParams();
		
		if($params['name']){
		
			$fields = array(
				"ID"              => $elementCode,
				"NAME"			  => $params['name'],
				"PREVIEW_TEXT"	  => $params['preview_text'],
				"DETAIL_TEXT"     => $params['detail_text'],
				"IBLOCK_SECTION_ID" => $params['section_id'],
				"PROPERTY_VALUES" => array(
					"STATUS"	=> GOODS_STATUS_MODERATION,
					"COLOR"		=> $params['color'],
					"STYLE"		=> $params['style'],
				),
				"PRICE_VALUE" => array(
					"PRODUCT_ID"       => $elementCode,
					"CATALOG_GROUP_ID" => BASE_PRICE,
					"PRICE"            => $params['price'],
					"CURRENCY"         => "RUB"
				),
				"SIZE_VALUE" => array(
					"WIDTH"			  => $params['width'],
					"HEIGHT"		  => $params['height'],
					"LENGTH"		  => $params['length']
				)
			);
			
			if($params['photoRoom']){
				$photoIds = explode(',', $params['photoRoom']);
				$photoData = array();

				foreach($photoIds as $photo){
					$rsPhoto = CFile::MakeFileArray(CFile::GetPath($photo));
					array_push($photoData, $rsPhoto);
				}
			};
			
			if($photoData){
				$fields['PROPERTY_VALUES']['PREVIEW'] = $photoData;
			}

			$result = $model_goods->editGoods($fields)->LAST_ERROR;
			$el2 = new CIBlockElement; 
		
			$this->_helper->json(['success' => $result?true:false]);
		};
	
		if(!empty($elementCode)){
			$filter_items = [
				[
					"LOGIC" => "OR",
					["ID" => $elementCode],
					["CODE" => $elementCode],
				],
			];
	
			$element = $model_goods->select(['ID','CODE', 'NAME', 'PREVIEW_TEXT', 'DETAIL_TEXT',
			'PROPERTY_COLOR', 'PROPERTY_STYLE', 'PROPERTY_PREVIEW'], true)->where($filter_items)->getElement();
			$element->PRICE_VALUE = array_shift($model_goods->getPrice($element));
			$element->SIZE = array_shift($model_goods->getSize($element));
		};

		/*==========================================================================
			//Категории
		==========================================================================*/	
		$params['select'] = ['ID', 'CODE', 'IBLOCK_SECTION_ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PICTURE', 'PICTURE'];
		$dbCatList = $model_goods->getCat($params);
		$categories = $dbCatList;
		
		$links = array();
		$tree = array();
		$childs = array();
		
		foreach($categories as $item){
			if($item->IBLOCK_SECTION_ID){
				$childs[$item->IBLOCK_SECTION_ID][] = $item;
				foreach($categories as $item){ 
					if(isset($childs[$item->ID]))
					$item->CHILD = $childs[$item->ID];
					$tree = $childs[$item->ID];
				}
			}
		}
		
		/*=============================================================================
	   /		Получает цепочку категорий от текущей до корневой					  /
	   *============================================================================*/
	  
		function getCategoryUp($where){
			static $chain_category = array();
			static $i = 0;
			$model = new Sibirix_Model_Goods;
			$categories = $model->getCat($where);

			array_push($chain_category, $categories[$i]);
			if(!empty($categories[$i]->IBLOCK_SECTION_ID)){
				$filter['where'] = ["=ID" => $categories[$i]->IBLOCK_SECTION_ID];
				getCategoryUp($filter);
				$i+=1;
			}
			return array_reverse($chain_category);
		};
		
		$filter = array();
		$filter['where'] = [
			array( 
				"LOGIC"=>"OR", 
				array("ID"=>255), 
				array("CODE"=>'catalog'), 
			) ];
			
		$ch_categories = getCategoryUp($filter);

		$user = $this->user;
		$catalog = $modelCatalog->select(["ID", "NAME", "CREATED_BY", "DATE_CREATE", "PROPERTY_FILE"], true)->where(["=CREATED_BY" => $user->ID], true)->getElements();
		
		
		/*if(count($catalog) == 0 ){
			
		}
		else{

		}*/

		$this->view->assign([
            "categories" => $dbCatList,
			"item"		 => $element,
			"tree"		 => $tree,
			"elementCode" => $elementCode,
			"catalog"	=> $catalog 
        ]);
	}
	
	public function uploadCatalogAction(){
		
		$upload = new Zend_File_Transfer();
		$modelCatalog = new Sibirix_Model_Catalog();
        $fileModel = new Sibirix_Model_Files();
		
		$user = $this->user;
		
        $fileType   = $this->getParam("fileType");
        $resizeType = $this->getParam("resizeType");

        $fields = array(
            $fileType => $upload->getFileInfo()["file"]
        );

        $regep = '/^PROPERTY_(.*)$/';
        if (preg_match($regep, $fileType, $matches)) {
            $fields["PROPERTY_VALUES"][$matches[1]] = $fields[$fileType];
            unset($fields[$fileType]);
        }

        $accessExtensions = "";
        switch ($fileType) {
            case "PROPERTY_FILE":
                $accessExtensions = "csv, excel";
                break;
            default:
                $this->_helper->json(['result' => false, "response" => "Invalid file type"]);
                break;
        }

        $editResult = $modelCatalog->editCatalog($fields);
        $newFile = false;
		
        if ($editResult) {
            $newFile = $modelCatalog->select([$fileType], true)->getElement($editResult);
        }

        if ($this->getParam("files") == "true") {
            $response = array();
            if ($editResult) {
                $fileData = EHelper::getFileData($newFile->$fileType);
                $response = array(
                    "valueId" => $editResult,
                    "fileType" => GetFileExtension($fileData["FILE_NAME"]),
                    "fileName" => $fileData["ORIGINAL_NAME"],
                    "fileSize" => round($fileData["FILE_SIZE"]/1024) . " Кб",
                );
            }
        } else {
            $response = array();
            if ($editResult) {
                if (empty($resizeType)) {
                    $resizeType = "DROPZONE_MAIN_PHOTO";
                }
                $response["imageSrc"] =  Resizer::resizeImage($newFile->DETAIL_PICTURE, $resizeType);
				$response["id"] = $designId;
            }		
        }

        $this->_helper->json(['result' => (bool)$editResult, "response" => $response]);
	}
	
	public function basketAction() {

		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'profile-inner');
        Zend_Registry::get('BX_APPLICATION')->SetTitle('Профиль');
		$type = $this->getParam('type');
		/*if($type == 'favourite'){
			$delay = "Y";
			$pageTitle = "Избранные товары";
		}
		else if($type == 'basket'){
			$delay = "N";
			$pageTitle = "Корзина";
		}
		else{
			Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
		}*/
		$delay = "N";
		$pageTitle = "Корзина";
		$basketModel = new Sibirix_Model_Basket();
		
		$basketData = $basketModel->getBasket($delay);

		$this->view->assign([
            "pageTitle" => $pageTitle,
            //"filter"    => $filter,
            "itemList"  => $basketData['basketItems'],
            //'paginator' => EHelper::getPaginator($result->pageData->totalItemsCount, $result->pageData->current, $result->pageData->size),
        ]);
    }
	
	public function favouriteAction() {
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'profile-inner');
        Zend_Registry::get('BX_APPLICATION')->SetTitle('Профиль');
		$type = $this->getParam('type');
		$basketModel = new Sibirix_Model_Basket();

		if($type == 'goods'){
			$delay = "Y";
			$pageTitle = "Избранные товары";
			$basketData = $basketModel->getBasket($delay);
			$favoutiteData = array();
			foreach($basketData['basketItems'] as $item){
				$result = CIBlockElement::GetByID($item['PRODUCT_ID'])->Fetch();
				if($result['IBLOCK_ID'] == IB_GOODS){
					array_push($favoutiteData, $item);
				}
			}
		}
		else if($type == 'design'){
			$delay = "Y";
			$pageTitle = "Избранные дизайны";
			$basketData = $basketModel->getBasket($delay);
			$favoutiteData = array();
			foreach($basketData['basketItems'] as $item){
				$result = CIBlockElement::GetByID($item['PRODUCT_ID'])->Fetch();
				if($result['IBLOCK_ID'] == IB_DESIGN){
					array_push($favoutiteData, $item);
				}
			}
		}
		else{
			Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
		}

		$this->view->assign([
            "pageTitle" => $pageTitle,
            //"filter"    => $filter,
            "itemList"  => $favoutiteData,
            //'paginator' => EHelper::getPaginator($result->pageData->totalItemsCount, $result->pageData->current, $result->pageData->size),
        ]);
    }
		
	public function ordersAction() {
		//ВХОД ТОЛЬКО ДЛЯ ДИЗАЙНЕРОВ
		if (!$this->user->getType() == DESIGNER_TYPE_ID) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }

        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'profile-inner-page');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'profile-inner');
        Zend_Registry::get('BX_APPLICATION')->SetTitle('Мои заказы');
		
		$designModel 		 = new Sibirix_Model_Design();
		$projectModel 		 = new Sibirix_Model_Project();
		$apartModel    		 = new Sibirix_Model_ProjectApart();
		$roomModel     		 = new Sibirix_Model_ProjectRoom();
		$planModel     		 = new Sibirix_Model_Plan();
		$planoptionModel     = new Sibirix_Model_PlanOption();
		
		
		/*Получим свои дизайн-проекты*/

		$designList = $designModel->select(['ID'],true)->where(['=CREATED_BY' => $this->user->ID, '!=PROPERTY_STATUS' => DESIGN_STATUS_DRAFT])->getElements();

		$designIds = array_map(function ($obj) {
					return $obj->ID;
					}, $designList);
		$designIds = array_unique($designIds);
				
		$filter = array('PROPERTY_OFFERS' => $designIds);
		
		//конкурсы для квартир
		$competitionList = $apartModel->getProjectApartbList(['ID', 'NAME', 'PREVIEW_TEXT', 'SHOW_COUNTER', 'PROPERTY_TIME', 'PROPERTY_ID_ORDER',
		'PROPERTY_DESIGN_LIKED', 'PROPERTY_STATUS', 'PROPERTY_PRICE_SQUARE'],$filter);
		//конкурсы для комнат
		$competitionList2 = $roomModel->getProjectRoombList(['ID', 'NAME', 'PREVIEW_TEXT', 'SHOW_COUNTER', 'PROPERTY_TIME', 'PROPERTY_ID_ORDER',
		'PROPERTY_DESIGN_LIKED', 'PROPERTY_STATUS', 'PROPERTY_PRICE_SQUARE'],$filter);
				
		//сливаем
		$result->items = array_merge($competitionList->items, $competitionList2->items);
		
		$result->totalOpen = 0;
		$result->totalFinished = 0;
			
		foreach($result->items as $item){
			if($item->PROPERTY_STATUS_ENUM_ID == PROJECT_ROOM_STATUS_OPEN || $item->PROPERTY_STATUS_ENUM_ID == PROJECT_APARTMENT_STATUS_OPEN){
				$result->totalOpen+=1;
			}
			else if($item->PROPERTY_STATUS_ENUM_ID == PROJECT_ROOM_STATUS_FINISHED || $item->PROPERTY_STATUS_ENUM_ID == PROJECT_APARTMENT_STATUS_FINISHED){
				$result->totalFinished+=1;
			}
			
			$project = $projectModel->select(['ID', 'PROPERTY_ID_OPTION_PLAN'], true)->getElement($item->PROPERTY_ID_ORDER);
			
			$planOption = $planoptionModel->select(['ID', 'PROPERTY_PLAN_FLAT', 'PROPERTY_IMAGES'], true)->getElement($project->PROPERTY_ID_OPTION_PLAN);
	
			if (!empty($planOption->PROPERTY_IMAGES_VALUE)) {
				$image = Resizer::resizeImage($planOption->PROPERTY_IMAGES_VALUE, 'PLAN_LIST');
			} else {
				$image = '/local/images/proomer2.png';
			}
			
			$planOption->PROPERTY_IMAGES_VALUE = $image;

			$plan = $planModel->select(['ID', 'PROPERTY_DOCUMENTS'], true)->getElement($planOption->PROPERTY_PLAN_FLAT);

			if (!empty($plan->PROPERTY_IMAGES_VALUE)) {
				$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'PLAN_LIST');
			} else {
				$image = '/local/images/proomer2.png';
			}

		//	$plan->PROPERTY_IMAGES_VALUE = $image;
			$item->PROPERTY_DOCUMENTS_VALUE = CFile::GetPath($plan->PROPERTY_DOCUMENTS_VALUE[0]);
										
		};

        $this->view->assign([
            "filter"    => $filter,
            "itemList"  => $result,
            'paginator' => EHelper::getPaginator($result->pageData->totalItemsCount, $result->pageData->current, $result->pageData->size),
            'status' => $status,
        ]);
    }
	
    public function typeAction() {
        if ($this->user->getType() !== UNDEFINED_TYPE_ID) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }

        $form = new Sibirix_Form_ProfileType();

        if (!Sibirix_Model_User::isAuthorized()) {
            $this->_helper->json(['success' => false, 'errorFields' =>  ['profileType' => 'Пользователь не авторизован']]);
        }

        if ($form->isValid($this->getAllParams())) {
            $model = new Sibirix_Model_User();
            $type = $form->getValue('profileType');
	
            $result = $model->updateType($this->user, $type);
			
            if ($result === true) {
                $this->_helper->json(['success' => true]);
            } else {
                foreach ($result as $ind => $error) {
                    $form->getElement($ind)->addErrors($result);
                }
                $form->markAsError();
                $form->getFieldsErrors();
                $this->_helper->json(['success' => false, 'errorFields' =>  $form->formErrors]);
            }
        } else {
            $form->getFieldsErrors();
        }

        $this->_helper->json(['success' => false, 'errorFields' =>  $form->formErrors]);
    }
	
	public function cancelPartAction() {

		$competitionId = (int) $this->getParam('competitionId');
		
		$designModel 		 = new Sibirix_Model_Design();
		$apartModel    		 = new Sibirix_Model_ProjectApart();
		$roomModel     		 = new Sibirix_Model_ProjectRoom();
		
		/*Получим свои дизайн-проекты*/
		$designList = $designModel->select(['ID'],true)->where(['=CREATED_BY' => $this->user->ID, '!=PROPERTY_STATUS' => DESIGN_STATUS_DRAFT])->getElements();
		
		$designIds = array_map(function ($obj) {
					return $obj->ID;
					}, $designList);
		$designIds = array_unique($designIds);

		$competition = $apartModel->select(['ID', 'PROPERTY_OFFERS'], true)->getElement($competitionId);

		if($competition){
			$offers = $competition->PROPERTY_OFFERS_VALUE;
			//$competitionPROPERTY_OFFERS_VALUE
			//$roomModel->editApartProject();

			foreach($designIds as $keyDesign => $designId){
				foreach($offers as $keyOffers => $offer){
					if($designId == $offer){
						unset($offers[$keyOffers]);
					}
				}
			}
			//костыль для битрикса
			if(count($offers) == 0){
				$offers = array(' ');
			}
			$fields = array(
				'ID' => $competitionId,
					'PROPERTY_VALUES' => array(
					'OFFERS' => $offers
				)
			);
			$result = $apartModel->editApartProject($fields);
		}
		else{
			$competition = $roomModel->select(['ID', 'PROPERTY_OFFERS'], true)->getElement($competitionId);
			if($competition){

				$offers = $competition->PROPERTY_OFFERS_VALUE;
				//$competitionPROPERTY_OFFERS_VALUE
				//$roomModel->editApartProject();
				
				foreach($designIds as $keyDesign => $designId){
					foreach($offers as $keyOffers => $offer){
						if($designId == $offer){
							unset($offers[$keyOffers]);
						}
					}
				};
				//костыль для битрикса
				if(count($offers) == 0){
					$offers = " ";
				}

				$fields = array(
					'ID' => $competitionId,
					'PROPERTY_VALUES' => array(
						'OFFERS' => $offers
					)
				);

				$result = $roomModel->editRoomProject($fields);
			}
		}
		
		$this->_helper->json(['success' => $result]);
    }
	
	public function financeAction() {
		
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'profile-inner');
		Zend_Registry::get('BX_APPLICATION')->SetTitle('Профиль');
				
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('finance', 'html')
		->initContext();
		
		$modelAccounts = new Sibirix_Model_Accounts();
		
		$data = $this->getAllParams();
		
		$user = $this->user;
		
		CModule::IncludeModule("sale");
		
		$filter = array();
		
		//$filter['USER_ID'] = $user->ID;
		if($data['period']){
			$filter['>=TRANSACT_DATE'] = $data['period'];
		};
		if($data['operation']){
			$filter['DEBIT'] = $data['operation'];
		};
		if((int) $data['page'] > 0){
			$page = $data['page'];
		}
		else{
			$page = 1;
		};
		
		$limit = 4 * $page;
		
		// Выберем все счета
		$dbAccountCurrency = CSaleUserAccount::GetList(
			array(),
			array('USER_ID' => $user->ID),
			false,
			array("CURRENT_BUDGET", "CURRENCY")
		);
		
		$arrCurrency = array();
		
		while ($arAccountCurrency = $dbAccountCurrency->Fetch())
		{
			array_push($arrCurrency, $arAccountCurrency);
		}
		
		if($arrCurrency){
			$currency = array_shift($arrCurrency);
		}
		else{
			$currency = 0;
		}
	
		// Выберем все транзакции пользователя
		$dbAccountCurrency = CSaleUserTransact::GetList(
			Array("ID" => "DESC"), $filter
		);
		$count = 0;
		$arrTransact = array();	
		while ($arAccountCurrency = $dbAccountCurrency->Fetch())
		{
			if($count < $limit){
				array_push($arrTransact, $arAccountCurrency);
				$count+=1;
			}
		}
		
		$listCard = $modelAccounts->select(['ID', 'PROPERTY_NUMBER', 'PROPERTY_CARD'],true)->where(['=PROPERTY_OWNER'])->limit(3)->getElements();

		$this->view->assign([
            "arrTransact"  => $arrTransact,
			"listCard"	   => $listCard,
			"currency"     => $currency,
			"filter"	   => $data
        ]);
	}
	
	public function financeSettingsAction() {
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'profile-inner');
        Zend_Registry::get('BX_APPLICATION')->SetTitle('Профиль');
		$modelUser = new Sibirix_Model_User();
		$modelAccounts = new Sibirix_Model_Accounts();
		/*CModule::IncludeModule("sale");
		$dbAccountCurrency = CSaleUserAccount::GetList(
			array(),
			array("USER_ID" => $userId),
				false,
				false,
				array("CURRENT_BUDGET", "CURRENCY")
		);*/
		//$arAccountCurrency = $dbAccountCurrency->Fetch();
		
		//while ($arAccountCurrency = $dbAccountCurrency->Fetch())
		//{
			//echo "На счете ".$arAccountCurrency["CURRENCY"].": ";
			//echo SaleFormatCurrency($arAccountCurrency["CURRENT_BUDGET"],
			//$arAccountCurrency["CURRENCY"])."<br>";
			//echo '123123123';
		//}
		
		
		$fields = array('ID',
						'NAME', 'SECOND_NAME', 'LAST_NAME',
						'PERSONAL_BIRTHDAY', 'PERSONAL_PHONE',
						'UF_PASSPORT_SERIES', 'UF_PASSPORT_NUMBER',
						'UF_DATE_ISSUE', 'UF_ISSUED_BY', 'UF_SUBDIVISION_CODE',
						'UF_REG_ADDRESS', 'UF_MAILING_ADDRES', 'UF_MAILING_ADDRESS',
						'UF_INN',
						'UF_RESIDENCY', 'UF_LEGAL_STATUS',
						'UF_SCAN'
        );
		
        $user = $modelUser->getUserData($this->user->ID, $fields);
		
		$arrayPhoto = array();
		foreach($user->UF_SCAN as $skan){
			array_push($arrayPhoto, CFile::getById($skan)->Fetch());
		}
				
		$UserField = CUserFieldEnum::GetList(array(), array("ID" => $user->UF_RESIDENCY));
		$user->UF_RESIDENCY = $UserFieldAr = $UserField->GetNext();
		
		$UserField = CUserFieldEnum::GetList(array(), array("ID" => $user->UF_LEGAL_STATUS));
		$user->UF_LEGAL_STATUS = $UserFieldAr = $UserField->GetNext();

		$user->UF_SCAN = $arrayPhoto;
		
		$listCard = $modelAccounts->select(['ID', 'PROPERTY_NUMBER', 'PROPERTY_CARD'],true)->where(['=PROPERTY_OWNER'])->limit(3)->getElements();
		
		$this->view->assign([
            "user"    => $user,
			"listCard" => $listCard
        ]);
	}
	
	public function editFinanceSettingsAction(){
		$modelUser = new Sibirix_Model_User();
		$modelAccounts = new Sibirix_Model_Accounts();
		$data = $this->getAllParams();
		$userId = (int) $this->user->ID;
		
		$errorFields = array();
		$fields = array(
			'PERSONAL_BIRTHDAY'   => $data['date_birth'],
			'UF_PASSPORT_SERIES'  => $data['series'],
			'UF_PASSPORT_NUMBER'  => $data['number'],
			'UF_DATT_ISSUE'		  => $data['date_issue'],
			'UF_ISSUED_BY'		  => $data['issued_by'],
			'UF_SUBDIVISION_CODE' => $data['subdivision_code'],
			'UF_SUBDIVISION_CODE' => $data['subdivision_code'],
			'UF_REG_ADDRESS'	  => $data['registration_address'],
			'UF_INN'			  => $data['inn'],
			'UF_MAILING_ADDRESS'  => $data['mailing_address'],
			'UF_LEGAL_STATUS'     => $data['legal_status'],
			'UF_RESIDENCY'     	  => $data['resident'],
			'PERSONAL_PHONE'	  => $data['phone_required']
		);
		
		$HL_CARD = Sibirix_Model_Reference::getReference(HL_CARD, array("UF_NAME", "UF_XML_ID"), "ID");
		
		$i = 0;
		$accounts = $modelAccounts->select(['ID'], true)->where(['=PROPERTY_OWNER' => $userId])->getElements();
		foreach($accounts as $acc){
			$modelAccounts->remove($acc->ID);
		};
		foreach($data['number_cart'] as $cart){
			if($i <= 2 && !empty($cart)){
				// проверим встречается ли подстрока в строке
				$find = strpos ($cart, '*');
				if ($find === FALSE){
					$cart = str_replace(" ", "", $cart);
					$cart = (int) $cart;
					$postdata = http_build_query(
						array(
							'skr_destinationCardNumber' => $cart,
							'skr_responseFormat' => 'json'
						)
					);

					$opts = array('http' =>
						array(
							'method'  => 'POST',
							'header'  => 'Content-type: application/x-www-form-urlencoded',
							'content' => $postdata
						)
					);
					
					$context = stream_context_create($opts);
					
					$json = json_decode(file_get_contents(MASK_YANDEX, false, $context));
					if(isset($json->storeCard->reason) && $json->storeCard->reason != 'cardinvalid'){
						$fields_card = array(); 
						$fields_card['ID'] = 0;
						$fields_card['PROPERTY_VALUES']['NUMBER'] = $json->storeCard->skr_destinationCardPanmask;
									
						$fields_card['PROPERTY_VALUES']['CARD'] = strtolower($json->storeCard->skr_destinationCardType);

						$fields_card['PROPERTY_VALUES']['OWNER'] = $userId;
						
						$result = $modelAccounts->editAccount($fields_card);
					}
					else if($json->storeCard->reason == 'cardinvalid'){
						$error = ["message" => 'Введите действующий номер карты', "fieldId" => 'number_cart', "eq" => $i];
						array_push($errorFields, $error);
					}
				};
			};
			$i+=1;
		}

		if(count($errorFields) == 0){
			$editResult = $modelUser->updateDesignerbProfile($userId, $fields);
		}
		else{
			$editResult = 0;
		}
		$this->_helper->json(['success' => (bool)$editResult, 'errorFields' => $errorFields]);
	}
	
	/**
	* Загрузка изображений с dropzone "налету"
	*/
	public function financeSettingsUploadScanAction() {
		
		$upload = new Zend_File_Transfer();
		$modelUser = new Sibirix_Model_User();
        $userId = (int) $this->user->ID;
        $fileModel = new Sibirix_Model_Files();

        $fileType   = $this->getParam("fileType");
        $resizeType = $this->getParam("resizeType");
		$arrayPhoto = array();
		foreach($modelUser->select([$fileType], true)->getElement($userId)->UF_SCAN as $skan){
			array_push($arrayPhoto, CFile::MakeFileArray(CFile::getPath($skan)));
		}
		array_push($arrayPhoto, $upload->getFileInfo()["file"]);
        $fields = array(
            $fileType => $arrayPhoto
        );
	
        $regep = '/^PROPERTY_(.*)$/';
        if (preg_match($regep, $fileType, $matches)) {
            $fields["PROPERTY_VALUES"][$matches[1]] = $fields[$fileType];
            unset($fields[$fileType]);
        }

        $accessExtensions = "";
       // switch ($fileType) {
       //     case "UF_SCAN":
       //     default:
       //         $this->_helper->json(['result' => false, "response" => "Invalid file type"]);
      //          break;
      //  }

        if (!$fileModel->checkFile($upload->getFileInfo()["file"], $accessExtensions)) {
            $this->_helper->json(['success' => false, "response" => "Invalid file type"]);
        }
	
		$editResult = $modelUser->updateDesignerbProfile($userId, $fields);

        $newFile = false;
        //if ($editResult) {
        $newFile = $modelUser->select([$fileType], true)->getElement($userId);

        //}
        if ($this->getParam("files") == "true") {
            $response = array();
          //  if ($editResult) {
                $fileData = EHelper::getFileData($newFile->$fileType);
                $response = array(
					"designId" => $userId,
                    "valueId" => $newFile->$fileType,
                    "fileType" => GetFileExtension($fileData["FILE_NAME"]),
                    "fileName" => $fileData["ORIGINAL_NAME"],
                    "fileSize" => round($fileData["FILE_SIZE"]/1024) . " Кб",
                );
         //   }
        } else {
            $response = array();
           // if ($editResult) {
                if (empty($resizeType)) {
                    $resizeType = "DROPZONE_MAIN_PHOTO";
                }
                $response["imageSrc"] =  Resizer::resizeImage($newFile->$fileType, $resizeType);
				$response["id"] = $designId;
           // }

        }
			
        $this->_helper->json(['success' => (bool)$editResult, "response" => $response]);
	}
	
	/**
     * Удаляет файл
    */
    public function financeSettingsRemoveScanAction() {
        $userId = (int) $this->user->ID;
		$modelUser = new Sibirix_Model_User();

        $fileType   = $this->getParam("fileType");
		$file_id   = $this->getParam("file_id");

        $fields = array(
           // $fileType => array("del" => "Y")
        );

        //  $regep = '/^PROPERTY_(.*)$/';
        //  if (preg_match($regep, $fileType, $matches)) {
           // $fields[$matches[1]] = $fields[$fileType];
           // unset($fields[$fileType]);
		//   }
		
		$arrayPhoto = array();
		
		foreach($modelUser->select([$fileType], true)->getElement($userId)->UF_SCAN as $skan){
			if($skan != $file_id){
				array_push($arrayPhoto, CFile::MakeFileArray(CFile::getPath($skan)));
			}
		}

		$fields['UF_SCAN'] = $arrayPhoto;
	
        $editResult = $modelUser->updateDesignerbProfile($userId, $fields);
		CFile::Delete($file_id);

        $this->_helper->json(['result' => (bool)$editResult]);
    }
	
	public function addCommunicationAction() {
		$params = $this->getAllParams();
		$model = new Sibirix_Model_User();
				
		$prefix = $params['social'];
		$login = $params['login'];
		
		$result = $model->updateDesignerProfileSocial($this->user, $login, $prefix);
		
		$this->_helper->json(['success' => $result, 'login' => $login]);
	}
	
		/**
     * Загрузка изображений с dropzone "налету"
    */
    public function uploadFileAction() {
		
		$upload   = new Zend_File_Transfer();
		
		$fileType 	= $this->getParam("fileType");
		$resizeType = $this->getParam('resizeType');
	
		//получим информацию о файле
		$file = $upload->getFileInfo()["file"];
		
		$arDataFile["name"]		 = $file['name'];
		$arDataFile["size"]		 = $file['size'];
		$arDataFile["tmp_name"]  = $file['tmp_name'];
		$arDataFile["MODULE_ID"] = 'iblock';
		$arDataFile['type']		 = $file['type'];
		
		//если файл загружен
		if (strlen($arDataFile["name"]) > 0){
			//регистрируем файл
			$fid = CFile::SaveFile($arDataFile, "iblock");
		}
		else{
			//посылаем
			Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
			throw new Zend_Exception('Not found', 404);
		}
		
        if ($this->getParam("files") == "true") {
            $response = array();
			$fileData = EHelper::getFileData($newFile->$fileType);
			$response = array(
				"valueId" => $fid,
				"fileType" => GetFileExtension($arDataFile["name"]),
				"fileName" => $arDataFile["name"],
				"fileSize" => round($arDataFile["size"]/1024) . " Кб",
			);
        }else {
			
            $response = array();
			if (empty($resizeType)) {
				$resizeType = "DROPZONE_MAIN_PHOTO";
			}

			$response = array(
				[
				"imageSrc" => Resizer::resizeImage($fid, $resizeType),
				"valueId" => $fid
				],
			);
        }

        $this->_helper->json(['result' => (bool)$fid, "response" => $response]);
    }
}