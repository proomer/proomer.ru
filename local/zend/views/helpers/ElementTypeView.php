<?

/**
 * Class Zend_View_Helper_ElementCntList
 */
class Zend_View_Helper_ElementTypeView extends Zend_View_Helper_Abstract {

    public function elementTypeView($profile = false) {
        return $this->view->partial('_partials/element-type-view.phtml', array(
            "elementCnt"   => Sibirix_Model_ViewCounter::getCounterList($profile),
            "currentValue" => Sibirix_Model_ViewCounter::getViewCounter($profile)
        ));
    }
}